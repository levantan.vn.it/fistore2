<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_book', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');

            $table->string('name')->comment('Tên người nhận hàng');
            $table->string('phone')->comment('Điện thoại liên hệ');
            $table->integer('province_id')->comment('Tỉnh thành');
            $table->integer('district_id')->comment('Quận huyện');
            $table->integer('ward_id')->comment('Phường/Xã');
            $table->string('address')->comment('Địa chỉ');
            $table->tinyInteger('address_type')->default(0)->comment('0: Nhà riêng | 1: Công ty');

            $table->boolean('defaulted')->default(false)->comment('Địa chỉ giao hàng mặc định');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_book');
    }
}
