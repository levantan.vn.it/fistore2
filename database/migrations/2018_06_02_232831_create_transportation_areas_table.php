<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportationAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportation_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transport_id');
            $table->foreign('transport_id')->references('id')->on('transportations')->onDelete('CASCADE');
            $table->unsignedInteger('province_id');
            $table->unsignedInteger('district_id');
            $table->double('min_order')->default(0)->comment('Trị giá tối thiểu hóa đơn áp dụng');
            $table->double('max_order')->default(null)->nullable()->comment('Trị giá tối đa hóa đơn áp dụng');
            $table->double('price')->default(0)->comment('Giá vận chuyển');
            $table->boolean('denied')->default(false)->comment('Từ chối vận chuyển');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportation_areas');
    }
}
