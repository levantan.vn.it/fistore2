<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Tên người dùng');
            $table->string('email')->unique()->comment('Địa chỉ email để quản lý, không được trùng');
            $table->string('phone')->nullable()->comment('Số điện thoại liên hệ');
            $table->string('password')->nullable()->comment('Mật khẩu đăng nhập, null nếu là tài khoản mạng xã hội');

            $table->tinyInteger('gender')->nullable()->comment('1: nam | 0: nữ');
            $table->datetime('date_of_birth')->nullable()->comment('Ngày sinh');

            $table->string('provider')->nullable()->comment('Loại tài khoản mạng xã hội');
            $table->string('social_id')->unique()->nullable()->comment('ID tài khoản mạng xã hội');

            $table->integer('extra_point')->default(0)->comment('Điểm extra tích lũy');

            $table->boolean('verified')->default(false)->comment('True Nếu đã xác thực Email');
            $table->boolean('disabled')->default(false)->comment('True Nếu bị khóa tài khoản');
            
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
