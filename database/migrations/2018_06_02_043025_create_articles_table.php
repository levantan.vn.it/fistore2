<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('content')->nullable();
            $table->string('slug')->unique();
            $table->string('thumbnail')->nullable()->comment('Ảnh bài viết'); // Khi null sẽ sử dụng helper để chuyển hướng sang file default.jpg
            $table->string('title')->nullable()->comment('Tiêu đề cho SEO');
            $table->text('description')->nullable()->comment('Mô tả cho SEO');
            $table->unsignedInteger('author_id')->nullable()->comment('Ánh xạ đến bảng admins');
            $table->text('tags')->nullable()->comment('Từ khóa');
            $table->integer('view')->default(0)->comment('Lượt xem');

            $table->boolean('hidden')->default(false)->comment('Hiện/Ẩn bài viết');
            $table->boolean('drafted')->default(false)->comment('Lưu nháp');
            $table->boolean('trashed')->default(false)->comment('Chuyển vào thùng rác');
            
            $table->boolean('featured')->default(false)->comment('Bài viết nổi bật');
            $table->datetime('reposted_at')->nullable()->comment('Đăng lên đầu tiên');
            
            $table->foreign('author_id')->references('id')->on('admins')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
