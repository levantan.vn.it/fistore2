<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('product_id')->nullable();
            $table->unsignedInteger('version_id')->nullable();
            
            $table->string('code');
            $table->string('name');
            $table->integer('quantity');
            $table->double('price');
            $table->double('original_price')->default(null)->nullable()->comment('Giá gốc');
            $table->double('subtotal');
            $table->boolean('destroyed')->default(false)->comment('Sản phẩm đã bị xóa khỏi database');

            $table->foreign('order_id')->references('id')->on('product_orders')->onDelete('CASCADE');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_order_details');
    }
}
