<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medias', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('target_id')->nullable();
            $table->string('target_type')->comment('product|article|page|banner');
            $table->string('target_url')->nullable()->comment('Đường dẫn trỏ đến khi click ảnh');
            $table->string('name')->nullable();
            $table->double('size')->default(null)->nullable()->comment('Đơn vị tính: Byte');
            $table->string('type')->nullable()->comment('Ex: image/jpeg');
            $table->string('path')->comment('Ex: timestamp/name.mine');
            $table->integer('index')->default(0)->comment('Thứ tự hình ảnh nếu thuộc cùng một đối tượng');
            $table->boolean('trashed')->default(false)->comment('Chuyển vào thùng rác');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medias');
    }
}
