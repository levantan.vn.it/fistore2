<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductGroupRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_group_relationships', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('group_id');

            $table->primary(['product_id', 'group_id']);
            $table->foreign('product_id')->references('id')->on('products')->onDelete('CASCADE');
            $table->foreign('group_id')->references('id')->on('product_groups')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_group_relationships');
    }
}
