<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewThankedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_thanked', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('review_id');

            $table->foreign('user_id')->on('users')->references('id')->onDelete('CASCADE');
            $table->foreign('review_id')->on('reviews')->references('id')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_thanked');
    }
}
