<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_configs', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('key')->comment('Keycode dùng cho Lập trình viên phát triển');
            $table->text('value')->nullable();
            $table->text('default')->nullable()->comment('Giá trị mặc định'); // Dùng cho chức năng "Khôi phục mặc định"
            $table->text('comment')->nullable();
            $table->boolean('activated')->default(true);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_configs');
    }
}
