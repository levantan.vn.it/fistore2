<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('parent_id')->nullable()->comment('Trả lời nhận xét của...');
            $table->integer('rating')->nullable()->comment('Xếp loại');
            $table->string('title')->nullable()->comment('Tiêu đề nhận xét');
            $table->longText('content')->nullable()->comment('Bình luận');
            $table->integer('thanked')->default(0)->comment('Được cảm ơn');
            $table->boolean('bought')->default(false)->comment('Đã mua hàng');

            $table->boolean('trashed')->default(false);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
