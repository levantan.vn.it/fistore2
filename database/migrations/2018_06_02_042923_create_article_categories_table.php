<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->unsignedInteger('parent_id')->nullable()->comment('Ánh xạ đến chuyên mục cha');
            $table->string('title')->nullable()->comment('Tiêu đề cho SEO');
            $table->text('description')->nullable()->comment('Mô tả cho SEO');
            $table->boolean('pinned')->default(false)->comment('Ghim lên trang chủ');

            $table->foreign('parent_id')->references('id')->on('article_categories')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_categories');
    }
}
