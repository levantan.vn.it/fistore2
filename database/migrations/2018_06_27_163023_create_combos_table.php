<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCombosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Tên gói combo');
            $table->string('slug');
            $table->double('price')->comment('Giá bán');
            $table->string('thumbnail')->nullable()->comment('Ảnh sản phẩm');
            $table->longText('content')->nullable();
            $table->text('tags')->nullable();
            $table->integer('quantity')->nullable()->comment('Null nếu không giới hạn số lượng');

            $table->string('title')->nullable()->comment('Tiêu đề cho SEO');
            $table->text('description')->nullable()->comment('Mô tả cho SEO');

            $table->boolean('hidden')->default(false)->comment('Hiện/Ẩn bài viết');
            
            $table->datetime('reposted_at')->nullable()->comment('Đăng lên đầu tiên');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('combos');
    }
}
