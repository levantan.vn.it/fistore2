<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hot_deals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->double('price')->comment('Giá bán khuyến mãi');
            $table->double('original_price')->nullable()->comment('Giá gốc');
            $table->integer('quantity')->nullable()->comment('Số lượng');
            $table->datetime('started_at')->comment('Ngày bắt đầu');
            $table->datetime('expired_at')->comment('Ngày hết hạn');
            $table->boolean('disabled')->default(false);

            $table->foreign('product_id')->references('id')->on('products')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hot_deals');
    }
}
