<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOrdersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->comment('Mã đơn hàng');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('name');
            $table->string('phone');
            $table->string('address');
            $table->unsignedInteger('ward_id');
            $table->unsignedInteger('district_id');
            $table->unsignedInteger('province_id');

            $table->integer('quantity');
            $table->double('transport_price')->default(0)->comment('Phí vận chuyển');
            $table->double('discounted')->default(0)->comment('Được giảm (VNĐ)');
            $table->double('subtotal')->comment('Tổng chưa giảm (Tổng phụ)');
            $table->double('total')->comment('Tổng thanh toán');

            $table->text('note')->nullable()->comment('Ghi chú đơn hàng');

            $table->string('payment_method', 32)->default('cod')->comment('Phương thức thanh toán');
            $table->string('payment_bank')->nullable()->comment('Tên ngân hàng thanh toán');

            $table->tinyInteger('status')->default(0)->comment('Trạng thái đơn hàng: 0 => Chờ xác nhận; 1 => Đã xác nhận; 2 => Đang vận chuyển; 3: Hoàn tất');
            $table->boolean('delayed')->default(false)->comment('Trì hoãn');
            $table->boolean('cancelled')->default(false)->comment('Hủy đơn hàng');
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_orders');
    }
}
