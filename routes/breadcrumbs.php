<?php

/**
 * --------------------------
 * Client View
 * ------
 */

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Trang chủ', route('home'));
});

// Trang chủ > Chi tiết sản phẩm
Breadcrumbs::register('product.detail', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push($data->name, route('product.detail', $data->slug));
});

// Trang chủ > Giỏ hàng
Breadcrumbs::register('cart.show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Giỏ hàng', route('cart.show'));
});

// Trang chủ > Thương hiệu
Breadcrumbs::register('brand.list', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Thương hiệu', route('brand.list'));
});

// Trang chủ > Thương hiệu > Tên thương hiệu
Breadcrumbs::register('brand.products', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('brand.list');
    $breadcrumbs->push($data->name, route('brand.detail', $data->slug));
});

// Trang chủ > Tên loại sản phẩm
Breadcrumbs::register('category.detail', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push($data->name, route('category.detail', $data->slug));
});

// Trang chủ > Nhóm sản phẩm
Breadcrumbs::register('group.detail', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push($data->name, route('group.detail', $data->slug));
});

// Trang chủ > Khuyến mãi
Breadcrumbs::register('hotdeal', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Khuyến mãi', route('hotdeal'));
});

// Trang chủ > Khuyến mãi
Breadcrumbs::register('combo.detail', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push($data->name, route('combo.detail', $data->slug));
});

// Trang chủ > Xu hướng
Breadcrumbs::register('article.list', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Bài viết sự kiện', route('article.list'));
});
// Trang chủ > Xu hướng > Tên bài viết
Breadcrumbs::register('article.detail', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('article.list');
    $breadcrumbs->push($data->name, route('article.detail', $data->slug));
});

// Trang chủ > Thông tin tài khoản
Breadcrumbs::register('user.profile', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Thông tin tài khoản', route('user.profile'));
});
// Trang chủ > Quản lý đơn hàng
Breadcrumbs::register('user.order', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Quản lý đơn hàng', route('user.order'));
});
// Trang chủ > Sổ địa chỉ
Breadcrumbs::register('user.address_book', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Sổ địa chỉ', route('user.address_book'));
});
// Trang chủ > Quản lý điểm
Breadcrumbs::register('user.extra', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Quản lý điểm', route('user.extra'));
});

// Trang chủ > Liên hệ
Breadcrumbs::register('contact', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Liên hệ', route('contact'));
});

// Trang chủ > Tìm kiếm
Breadcrumbs::register('search', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Tìm kiếm', route('search'));
});

// Trang chủ > Nhóm trang
Breadcrumbs::register('page.group', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push($data->name, route('page.group', $data->slug));
});
// Trang chủ > Nhóm trang > Trang
Breadcrumbs::register('page.detail', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push($data->name, route('page.detail', $data->slug));
});

/**
 * --------------------------
 * Admin View
 * ------
 */

/**
 * Bảng điều khiểm
 */
Breadcrumbs::register('admin.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Bảng điều khiển', route('admin.dashboard'));
});

/**
 * Hồ sơ cá nhân
 */
Breadcrumbs::register('admin.profile', function ($breadcrumbs) {
    $breadcrumbs->push('Hồ sơ cá nhân', route('admin.profile.show'));
});
// Hồ sơ cá nhân > Cập nhật hồ sơ
Breadcrumbs::register('admin.profile.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.profile');
    $breadcrumbs->push('Cập nhật hồ sơ', route('admin.profile.edit'));
});
// Hồ sơ cá nhân > Đổi mật khẩu
Breadcrumbs::register('admin.profile.password', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.profile');
    $breadcrumbs->push('Đổi mật khẩu', route('admin.profile.password.edit'));
});

/**
 * Quản trị
 */
Breadcrumbs::register('admin.admin', function ($breadcrumbs) {
    $breadcrumbs->push('Quản trị', route('admin.user.admin.index'));
});
// Quản trị > Tạo mới
Breadcrumbs::register('admin.admin.create', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.admin');
    $breadcrumbs->push('Tạo mới', route('admin.user.admin.create'));
});
// Quản trị > Chỉnh sửa
Breadcrumbs::register('admin.admin.edit', function ($breadcrumbs, $data) {
	$breadcrumbs->parent('admin.admin');
    $breadcrumbs->push($data->name, route('admin.user.admin.edit', $data->id));
});
/**
 * Khách hàng
 */
Breadcrumbs::register('admin.customer', function ($breadcrumbs) {
    $breadcrumbs->push('Khách hàng', route('admin.user.customer.index'));
});
// Khách hàng > Xem thông tin
Breadcrumbs::register('admin.customer.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.customer');
    $breadcrumbs->push($data->name, route('admin.user.customer.show', $data->id));
});
/**
 * Nhóm quyền
 */
Breadcrumbs::register('admin.role', function ($breadcrumbs) {
    $breadcrumbs->push('Nhóm quyền', route('admin.role.index'));
});
// Nhóm quyền > Tạo mới
Breadcrumbs::register('admin.role.create', function ($breadcrumbs) {
	$breadcrumbs->parent('admin.role');
    $breadcrumbs->push('Tạo mới', route('admin.role.create'));
});
// Nhóm quyền > Chỉnh sửa
Breadcrumbs::register('admin.role.edit', function ($breadcrumbs, $data) {
	$breadcrumbs->parent('admin.role');
    $breadcrumbs->push($data->name, route('admin.role.edit', $data->id));
});
// Nhóm quyền > Phân quyền
Breadcrumbs::register('admin.permission.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.role');
    $breadcrumbs->push($data->name, route('admin.permission.edit', $data->id));
});

/**
 * Trang nội dung
 */
Breadcrumbs::register('admin.page', function ($breadcrumbs) {
    $breadcrumbs->push('Trang nội dung', route('admin.page.index'));
    if(request('trash'))
        $breadcrumbs->push('Thùng rác', null);
});
// Trang nội dung > Tạo mới
Breadcrumbs::register('admin.page.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.page');
    $breadcrumbs->push('Tạo mới', route('admin.page.create'));
});
// Trang nội dung > Chỉnh sửa
Breadcrumbs::register('admin.page.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.page');
    $breadcrumbs->push($data->name, route('admin.page.edit', $data->id));
});
/**
 * Trang nội dung > Chuyên mục
 */
Breadcrumbs::register('admin.page.group', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.page');
    $breadcrumbs->push('Chuyên mục', route('admin.page.group.index'));
});
// Trang nội dung > Chuyên mục > Tạo mới
Breadcrumbs::register('admin.page.group.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.page.group');
    $breadcrumbs->push('Tạo mới', route('admin.page.group.create'));
});
// Trang nội dung > Chuyên mục > Chỉnh sửa
Breadcrumbs::register('admin.page.group.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.page.group');
    $breadcrumbs->push($data->name, route('admin.page.group.edit', $data->id));
});

/**
 * Sản phẩm
 */
Breadcrumbs::register('admin.product', function ($breadcrumbs) {
    $breadcrumbs->push('Sản phẩm', route('admin.product.index'));
    if(request('trash'))
        $breadcrumbs->push('Thùng rác', null);
});
// Sản phẩm > Tạo mới
Breadcrumbs::register('admin.product.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.product');
    $breadcrumbs->push('Tạo mới', route('admin.product.create'));
});
// Sản phẩm > Chỉnh sửa
Breadcrumbs::register('admin.product.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.product');
    $breadcrumbs->push($data->name, route('admin.product.edit', $data->id));
});
/**
 * Sản phẩm > Loại sản phẩm
 */
Breadcrumbs::register('admin.product.category', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.product');
    $breadcrumbs->push('Loại sản phẩm', route('admin.product.category.index'));
});
// Sản phẩm > Loại sản phẩm > Tạo mới
Breadcrumbs::register('admin.product.category.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.product.category');
    $breadcrumbs->push('Tạo mới', route('admin.product.category.create'));
});
// Sản phẩm > Loại sản phẩm > Chỉnh sửa
Breadcrumbs::register('admin.product.category.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.product.category');
    $breadcrumbs->push($data->name, route('admin.product.category.edit', $data->id));
});
/**
 * Sản phẩm > Thương hiệu
 */
Breadcrumbs::register('admin.product.brand', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.product');
    $breadcrumbs->push('Thương hiệu', route('admin.product.brand.index'));
});
// Sản phẩm > Thương hiệu > Tạo mới
Breadcrumbs::register('admin.product.brand.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.product.brand');
    $breadcrumbs->push('Tạo mới', route('admin.product.brand.create'));
});
// Sản phẩm > Thương hiệu > Chỉnh sửa
Breadcrumbs::register('admin.product.brand.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.product.brand');
    $breadcrumbs->push($data->name, route('admin.product.brand.edit', $data->id));
});
/**
 * Sản phẩm > Nhóm
 */
Breadcrumbs::register('admin.product.group', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.product');
    $breadcrumbs->push('Nhóm sản phẩm', route('admin.product.group.index'));
});
// Sản phẩm > Nhóm > Tạo mới
Breadcrumbs::register('admin.product.group.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.product.group');
    $breadcrumbs->push('Tạo mới', route('admin.product.group.create'));
});
// Sản phẩm > Nhóm > Chỉnh sửa
Breadcrumbs::register('admin.product.group.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.product.group');
    $breadcrumbs->push($data->name, route('admin.product.group.edit', $data->id));
});

/**
 * Bài viết
 */
Breadcrumbs::register('admin.article', function ($breadcrumbs) {
    $breadcrumbs->push('Bài viết', route('admin.article.index'));
    if(request('category')){
        $categoryinfo = \App\ProductCategory::find(request('category'));
        $breadcrumbs->push($categoryinfo['name'], null);
    }elseif(request('brand')){
        $brandinfo = \App\ProductBrand::find(request('brand'));
        $breadcrumbs->push($brandinfo['name'], null);
    }elseif(request('trash')){
        $breadcrumbs->push('Thùng rác', null);
    }
});
// Bài viết > Tạo mới
Breadcrumbs::register('admin.article.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.article');
    $breadcrumbs->push('Tạo mới', route('admin.article.create'));
});
// Bài viết > Chỉnh sửa
Breadcrumbs::register('admin.article.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.article');
    $breadcrumbs->push($data->name, route('admin.article.edit', $data->id));
});
/**
 * Bài viết > Chuyên mục
 */
Breadcrumbs::register('admin.article.category', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.article');
    $breadcrumbs->push('Chuyên mục', route('admin.article.category.index'));
});
// Bài viết > Chuyên mục > Tạo mới
Breadcrumbs::register('admin.article.category.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.article.category');
    $breadcrumbs->push('Tạo mới', route('admin.article.category.create'));
});
// Bài viết > Chuyên mục > Chỉnh sửa
Breadcrumbs::register('admin.article.category.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.article.category');
    $breadcrumbs->push($data->name, route('admin.article.category.edit', $data->id));
});

/**
 * Đơn hàng
 */
Breadcrumbs::register('admin.order', function ($breadcrumbs) {
    $breadcrumbs->push('Đơn hàng', route('admin.product.order.index'));
});
Breadcrumbs::register('admin.order.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.order');
    $breadcrumbs->push('Tạo đơn hàng', route('admin.product.order.create'));
});
Breadcrumbs::register('admin.order.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.order');
    $breadcrumbs->push($data->code, route('admin.product.order.show', $data->id));
});

/**
 * Vận chuyển
 */
Breadcrumbs::register('admin.transportation', function ($breadcrumbs) {
    $breadcrumbs->push('Vận chuyển', route('admin.transportation.index'));
});

/**
 * Cấu hình
 */
Breadcrumbs::register('admin.configuration', function ($breadcrumbs) {
    $breadcrumbs->push('Cấu hình', route('admin.configuration.show'));
});
// Cấu hình > Thay đổi cấu hình
Breadcrumbs::register('admin.configuration.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.configuration');
    $breadcrumbs->push('Thay đổi cấu hình', route('admin.configuration.edit'));
});

/**
 * Bình luận
 */
Breadcrumbs::register('admin.comment', function ($breadcrumbs) {
    $breadcrumbs->push('Bình luận', route('admin.comment.index'));
    if(request('trash'))
        $breadcrumbs->push('Thùng rác', null);
});

/**
 * Đánh giá
 */
Breadcrumbs::register('admin.review', function ($breadcrumbs) {
    $breadcrumbs->push('Đánh giá', route('admin.review.index'));
    if(request('trash'))
        $breadcrumbs->push('Thùng rác', null);
});

/**
 * Báo xấu
 */
Breadcrumbs::register('admin.report', function ($breadcrumbs) {
    $breadcrumbs->push('Báo xấu', route('admin.report.index'));
});

/**
 * Danh sách liên hệ
 */
Breadcrumbs::register('admin.contact', function ($breadcrumbs) {
    $breadcrumbs->push('Danh sách liên hệ', route('admin.contact.index'));
});

/**
 * Đăng ký nhận tin
 */
Breadcrumbs::register('admin.newsletter', function ($breadcrumbs) {
    $breadcrumbs->push('Đăng ký nhận tin', route('admin.newsletter.index'));
});

/**
 * Slide ảnh
 */
Breadcrumbs::register('admin.slide', function ($breadcrumbs) {
    $breadcrumbs->push('Slide ảnh', route('admin.slide.index'));
});

/**
 * Mã khuyến mãi
 */
Breadcrumbs::register('admin.coupon', function ($breadcrumbs) {
    $breadcrumbs->push('Mã khuyến mãi', route('admin.coupon.index'));
});
// Mã khuyến mãi > Tạo mã khuyến mãi
Breadcrumbs::register('admin.coupon.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.coupon');
    $breadcrumbs->push('Tạo mã khuyến mãi', route('admin.coupon.create'));
});
// Mã khuyến mãi > Chỉnh sửa
Breadcrumbs::register('admin.coupon.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.coupon');
    $breadcrumbs->push($data->code, route('admin.coupon.edit', $data->id));
});

/**
 * Hot Deal
 */
Breadcrumbs::register('admin.hotdeal', function ($breadcrumbs) {
    $breadcrumbs->push('Hot Deal', route('admin.hotdeal.index'));
});
// Hot Deal > Tạo mã khuyến mãi
Breadcrumbs::register('admin.hotdeal.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.hotdeal');
    $breadcrumbs->push('Tạo khuyến mãi', route('admin.hotdeal.create'));
});
// Hot Deal > Chỉnh sửa
Breadcrumbs::register('admin.hotdeal.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.hotdeal');
    $breadcrumbs->push($data->code, route('admin.hotdeal.edit', $data->id));
});

/**
 * Free Trial
 */
Breadcrumbs::register('admin.freetrial', function ($breadcrumbs) {
    $breadcrumbs->push('Trải nghiệm sản phẩm', route('admin.freetrial.index'));
});
// Free Trial > Tạo mã khuyến mãi
Breadcrumbs::register('admin.freetrial.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.freetrial');
    $breadcrumbs->push('Thêm sản phẩm', route('admin.freetrial.create'));
});
// Free Trial > Chỉnh sửa
Breadcrumbs::register('admin.freetrial.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.freetrial');
    $breadcrumbs->push($data->code, route('admin.freetrial.edit', $data->id));
});

/**
 * Combo
 */
Breadcrumbs::register('admin.combo', function ($breadcrumbs) {
    $breadcrumbs->push('Combo', route('admin.combo.index'));
});
// Combo > Tạo mã khuyến mãi
Breadcrumbs::register('admin.combo.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.combo');
    $breadcrumbs->push('Thêm gói khuyến mãi', route('admin.combo.create'));
});
// Combo > Chỉnh sửa
Breadcrumbs::register('admin.combo.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.combo');
    $breadcrumbs->push($data->name, route('admin.combo.edit', $data->id));
});

/**
 * Sale
 */
Breadcrumbs::register('admin.sale', function ($breadcrumbs) {
    $breadcrumbs->push('Chương trình khuyến mãi', route('admin.sale.index'));
});

Breadcrumbs::register('admin.sale.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.sale');
    $breadcrumbs->push('Thêm chương trình khuyến mãi', route('admin.sale.create'));
});

Breadcrumbs::register('admin.sale.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.sale');
    $breadcrumbs->push($data->name, route('admin.sale.edit', $data->id));
});

/**
 * Cấu hình
 */
Breadcrumbs::register('admin.extra_point', function ($breadcrumbs) {
    $breadcrumbs->push('Điểm tích lũy', route('admin.extra_point.show'));
});

/**
 * Popup
 */
Breadcrumbs::register('admin.popup', function ($breadcrumbs) {
    $breadcrumbs->push('Popup', route('admin.popup'));
});