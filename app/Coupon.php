<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'coupons';

    public function fk_targets()
    {
    	return $this->hasMany(CouponRelationship::class, 'coupon_id', 'id', 'target_type', 'target_type');
    }
}
