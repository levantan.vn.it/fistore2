<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comboprice extends Model
{
    protected $table = 'combo_details_price';
}
