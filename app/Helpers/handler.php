<?php
/**
 * Upload File
 *
 * @param file $file File ảnh
 * @param string $name Tên ảnh mong muốn
 * @param string $path Đường dẫn thư mục lưu
 * @param array $dimensions Kích thước [width, height];
 */
function upload_file($file, string $name, string $path, array $dimensions = null)
{
    $file_name = str_slug($name) . '.' . $file->getClientOriginalExtension();

    if (!file_exists($path))
        mkdir($path, 0777, true);
/*
    // Resize
    if(is_null($dimensions)) {
      $Image::make($file->getRealPath())->save($path . $file_name);
    }else{
      Image::make($file->getRealPath())->resize($dimensions[0], $dimensions[1], function ($ratio) {
        $ratio->aspectRatio();
      })->save($path . $file_name);
    }
*/
	if (!class_exists('S3')) require_once dirname(__FILE__).'/S3.php';

	// AWS access info
	if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAX5P45HN3LTZG6RXM');
	if (!defined('awsSecretKey')) define('awsSecretKey', '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');

	// Check for CURL
	if (!extension_loaded('curl') && !@dl(PHP_SHLIB_SUFFIX == 'so' ? 'curl.so' : 'php_curl.dll'))
		exit("\nERROR: CURL extension not loaded\n\n");

	// Pointless without your keys!
	if (awsAccessKey == 'change-this' || awsSecretKey == 'change-this')
		exit("\nERROR: AWS access information required\n\nPlease edit the following lines in this file:\n\n" .
			"define('awsAccessKey', 'change-me');\ndefine('awsSecretKey', 'change-me');\n\n");


	$s3 = new S3(awsAccessKey, awsSecretKey,false,'s3.amazonaws.com','ap-southeast-1');

	$bucket = 'cdn.fistore.vn';

	if ($file) {
		var_dump($file);
		$s3->putObject(S3::inputFile($file, false), $bucket, $path . $file_name, S3::ACL_PUBLIC_READ);
	}
	
    return $file_name;
}

/**
 * Kiểm tra quyền Admin
 * @param string $key Mã quyền
 */
function is_admin(string $key = null)
{
	if(!is_null($key)){
		if(Auth::user()->fk_role->key == $key)
			return true;
		return false;
	}

	if(Auth::user()->fk_role->key !== 'member')
		return true;
	return false;
}

/**
 * Cắt chuỗi giới hạn
 */
function str_length_limit(string $string = null, int $length)
{
	if(is_null($string))
		return '';
	$string = html_entity_decode(strip_tags($string));
	return str_limit($string, $length);
}

/**
 * Tạo chuỗi mà nhiều chữ số
 */
function make_rand_string($number)
{
	if($number < 10) {
		return '0000'.$number;
	}else if($number < 100) {
		return '000'.$number;
	}else if($number < 1000) {
		return '00'.$number;
	}else if($number < 10000) {
		return '0'.$number;
	}else{
		return $number;
	}
}
