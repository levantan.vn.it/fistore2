<?php

function get_province($province_id)
{
	return \App\Province::find($province_id);
}

function get_district($district_id)
{
	return \App\District::find($district_id);
}

function get_ward($ward_id)
{
	return \App\Ward::find($ward_id);
}

function get_provinces()
{
	return \App\Province::all();
}

function get_districts_by_province($province_id)
{
	return \App\District::where('province_id', $province_id)->get();
}

function get_wards_by_district($district_id)
{
	return \App\Ward::where('district_id', $district_id)->get();
}

function get_all_brands()
{
	return \App\ProductBrand::whereNull('parent_id')->orderBy('index')->get();
}

function get_all_categories()
{
	return \App\ProductCategory::whereNull('parent_id')->get();
}

function get_category($id)
{
	return \App\ProductCategory::find($id);
}

function get_all_groups()
{
	return \App\ProductGroup::whereNull('parent_id')->get();
}

function get_group($id)
{
	return \App\ProductGroup::whereId($id)->first();
}

function get_setting($key)
{
	return \App\Setting::where('key', $key)->first();
}

function get_combo_details($comboId)
{
	return \App\Combo::where('is_combo', true)->where('id', $comboId)->first();
}