<?php

function get_config()
{
	return \App\SiteConfig::first();
}

function get_slide($id)
{
	return \App\Slide::find($id);
}
