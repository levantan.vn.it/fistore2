<?php

/**
 * -----------------------
 * Đường dẫn rút gọn
 * -------------
 */

/**
 * Đường dẫn hiện tại
 */
function current_url($target = null)
{
	return url()->current() . $target;
}

/**
 * Đường dẫn đến file trong thư mục media
 * Trả về ảnh mặc định (no image) nếu file không tồn tại
 */
 
function media_url($folder = null, $file = null)
{
	if(is_null($file))
		return 'https://s3-ap-southeast-1.amazonaws.com/cdn.fistore.vn/media/default.jpg';
	if($folder)
		return 'https://s3-ap-southeast-1.amazonaws.com/cdn.fistore.vn/media/' . $folder . '/' . $file;
	else
		return 'https://s3-ap-southeast-1.amazonaws.com/cdn.fistore.vn/media/' . $file;
}

/*
function media_url($folder = null, $file = null)
{
	if(is_null($file))
		return asset('media/default.jpg');
	return asset('media/' . $folder . '/' . $file);
}
*/
/**
 * -----------------------
 * Đường dẫn chi tiết cho các chức năng
 * --------------
 */

/**
 * Đường dẫn chi tiết bài viết
 * Fixed
 * Truyền vào một article_slug
 */
function article_link($article_slug)
{
	return route('article.detail', $article_slug);
}

/**
 * Đường dẫn chi tiết trang
 * Fixed
 * Truyền vào một $group_slug, page_slug
 */
function page_link($page_slug)
{
	return route('page.detail', $page_slug);
}

/**
 * Đường dẫn chi tiết sản phẩm
 * Fixed
 * Truyền vào một product_slug
 */
function product_link($product_slug)
{
	return route('product.detail', $product_slug);
}

/**
 * Đường dẫn bình luận
 * Fixed
 * Truyền vào $product_slug và $comment_id
 */
function comment_link($product_slug, $comment_id)
{
	return route('product.detail', $product_slug) . '#comment-' . $comment_id;
}

/**
 * Đường dẫn thương hiệu sản phẩm
 * Fixed
 * Truyền vào một brand_slug
 */
function brand_link($brand_slug = null)
{
	if(!is_null($brand_slug))
		return route('brand.detail', $brand_slug);
}

/**
 * Đường dẫn loại sản phẩm
 * Fixed
 * Truyền vào một category_slug
 */
function category_link($category_slug = null)
{
	if(!is_null($category_slug))
		return route('category.detail', $category_slug);
}

/**
 * Đường dẫn loại sản phẩm
 * Fixed
 * Truyền vào một group_slug
 */
function group_link($group_slug)
{
	return route('group.detail', $group_slug);
}

/**
 * Đường dẫn chi tiết sản phẩm combo
 */
function combo_link($combo_slug)
{
	return route('product.detail', $combo_slug);
}