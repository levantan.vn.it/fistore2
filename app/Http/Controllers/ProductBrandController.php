<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductBrand;
use Illuminate\Http\Request;

class ProductBrandController extends Controller
{
    public function showBrandList()
    {
    	$data = ProductBrand::orderBy('name', 'asc')->get();
    	return view('brand_list', compact('data'));
    }

    public function showProductListInBrand($brand_slug)
    {
    	$brand = ProductBrand::whereSlug($brand_slug)->first();
        if(empty($brand)){
            return redirect()->route('home');
        }
        $brands = ProductBrand::whereNull('parent_id')->orderBy('index')->get();

    	$query = Product::whereHidden(0)
    				->whereDrafted(0)
    				->whereTrashed(0)
    				->where('brand_id', $brand->id);

    	if(request()->filled('sort')) {
    		if(request()->sort == 'min_cost') {
    			$query->orderBy('price', 'asc');
    		}
    		else if(request()->sort == 'max_cost') {
    			$query->orderBy('price', 'desc');
    		}
    	}
    	$data = $query->orderBy('position', 'asc')
                        ->orderBy('reposted_at', 'desc')
    					->orderBy('created_at', 'desc')
    					->paginate(20);
        $data_new = $data;
        $data_new = $data->map(function ($item) {
            $sale = $item->fk_sales()->where('status',1)->where('started_at','<=',date('Y-m-d H:i:s'))->where('expired_at','>=',date('Y-m-d H:i:s'))->first();
            if(!empty($sale)){
                if($sale->price_type == 1){
                    $item->price = floatval($item->original_price) - (floatval($item->original_price)*$sale->price_down/100);
                }
                else{
                    $item->price = floatval($item->original_price) -floatval($sale->price_down);
                }
            }
            return $item;
        });
        if(request()->filled('sort')) {
            if(request()->sort == 'min_cost') {
                $data_new = $data_new->sortBy(function ($item, $key) {
                    return $item->price;
                });
            }else if(request()->sort == 'max_cost') {
                $data_new = $data_new->sortByDesc(function ($item, $key) {
                    return $item->price;
                });
            }
            
        }
        
    	return view('brand_products', compact('brand', 'data', 'brands','data_new'));
    }
}
