<?php

namespace App\Http\Controllers;

use App\HotDeal;
use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;

class HotDealController extends Controller
{
    public function showHotDeal(Request $request)
    {
        $categories = ProductCategory::whereNull('parent_id')->get();
        
        $query = \App\HotDeal::whereDisabled(false)
                ->join('products', 'products.id', '=', 'hot_deals.product_id')
                ->join('product_brands', 'product_brands.id', '=', 'products.brand_id')
                ->where('products.is_combo', false)
                ->where('products.trashed', false)
                ->where('products.drafted', false)
                ->where('products.closed', false);

        if(!$request->filled('status')) {
            $query->where('started_at', '<=', date('Y-m-d H:i:s'))
                  ->where('expired_at', '>', date('Y-m-d H:i:s'))
                  ->isAvail();
        }

        if($request->status == 'ats'){
            $query->where('started_at', '>', date('Y-m-d H:i:s'))
                  ->where('expired_at', '>', date('Y-m-d H:i:s'));
        }

        if($request->status == 'expired'){
            $query->where(function($cond){
                $cond->where('expired_at', '<', date('Y-m-d H:i:s'))
                     ->orWhere('available_qty', '<=', 0);
            });
        }

        if($request->filled('sort')) {
            if($request->sort == 'min_cost') {
                $query->orderBy('price', 'asc');
            }
            else if($request->sort == 'max_cost') {
                $query->orderBy('price', 'desc');
            }
        }

        $data = $query->orderBy('hot_deals.created_at', 'desc')
                ->select('products.name', 'products.slug', 'hot_deals.price', 'hot_deals.original_price', 'hot_deals.quantity', 'hot_deals.available_qty', 'products.thumbnail', 'started_at', 'expired_at', 'product_brands.slug as brand_slug', 'product_brands.name as brand_name')
                ->paginate(20);
                

        return view('sale_of', compact('categories', 'data'));
    }
}
