<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use App\ProductBrand;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    public function showProductListInCategory($category_slug)
    {
    	$category = ProductCategory::whereSlug($category_slug)->first();
        if(empty($category)){
            return redirect()->route('home');
        }
        $child_cats = $category->fk_childs->pluck('id');

    	$categories = ProductCategory::whereNull('parent_id')->get();
    	$brands = ProductBrand::whereNull('parent_id')->orderBy('index')->get();

    	$query = Product::whereHidden(0)
    				->whereDrafted(0)
    				->whereTrashed(0)
    				->where(function($string) use ($category, $child_cats){
                        $string->where('category_id', $category->id)->orWhereIn('category_id', $child_cats);
                    });

    	if(request()->filled('brand')) {
    		$query = $query->where('brand_id', request()->brand);
    	}

    	if(request()->filled('sort')) {
    		if(request()->sort == 'min_cost') {
    			$query = $query->orderBy('price', 'asc');
    		}
    		else if(request()->sort == 'max_cost') {
    			$query = $query->orderBy('price', 'desc');
    		}
    	}
    	$data = $query->orderBy('position_in_cat', 'asc')
                        ->orderBy('reposted_at', 'desc')
    					->orderBy('created_at', 'desc')
    					->paginate(20);
        $data_new = $data;
        $data_new = $data->map(function ($item) {
            $sale = $item->fk_sales()->where('status',1)->where('started_at','<=',date('Y-m-d H:i:s'))->where('expired_at','>=',date('Y-m-d H:i:s'))->first();
            if(!empty($sale)){
                if($sale->price_type == 1){
                    $item->price = floatval($item->original_price) - (floatval($item->original_price)*$sale->price_down/100);
                }
                else{
                    $item->price = floatval($item->original_price) -floatval($sale->price_down);
                }
            }
            return $item;
        });
        if(request()->filled('sort')) {
            if(request()->sort == 'min_cost') {
                $data_new = $data_new->sortBy(function ($item, $key) {
                    return $item->price;
                });
            }else if(request()->sort == 'max_cost') {
                $data_new = $data_new->sortByDesc(function ($item, $key) {
                    return $item->price;
                });
            }
            
        }
    	return view('category_products', compact('category', 'categories', 'brands', 'data','data_new'));
    }
}
