<?php

namespace App\Http\Controllers;

use Auth;
use App\AddressBook;
use Illuminate\Http\Request;

class UserAddressBookController extends Controller
{
    public function getAddressBook()
    {
    	$data = AddressBook::where('user_id', Auth::guard('customer')->user()->id)->orderBy('defaulted', 'desc')->get();
    	return view('user_address_book', compact('data'));
    }

    public function createAddress(Request $request)
    {
    	$address_book = new AddressBook;
    	$address_book->user_id = Auth::guard('customer')->user()->id;
    	$address_book->name = $request->name;
    	$address_book->phone = $request->phone;
    	$address_book->province_id = $request->province;
    	$address_book->district_id = $request->district;
    	$address_book->ward_id = $request->ward;
    	$address_book->address = $request->address;
    	$address_book->address_type = $request->address_type;
    	$address_book->defaulted = $request->filled('default') ? true : false;
    	$address_book->save();

    	return back()->with([
    		'alert'	=> 'success',
    		'title'	=> 'Hoàn tất',
    		'message' => 'Đã thêm địa chỉ giao hàng mới.'
    	]);
    }

    public function updateAddress(Request $request)
    {
    	$address_book = AddressBook::find($request->id);
    	$address_book->user_id = Auth::guard('customer')->user()->id;
    	$address_book->name = $request->name;
    	$address_book->phone = $request->phone;
    	$address_book->province_id = $request->province;
    	$address_book->district_id = $request->district;
    	$address_book->ward_id = $request->ward;
    	$address_book->address = $request->address;
    	$address_book->address_type = $request->address_type;
    	$address_book->defaulted = $request->filled('default') ? true : false;
    	$address_book->save();

    	return back()->with([
    		'alert'	=> 'success',
    		'title'	=> 'Hoàn tất',
    		'message' => 'Đã cập nhật địa chỉ giao hàng.'
    	]);
    }

    public function deleteAddress(Request $request)
    {
        $address_book = AddressBook::destroy($request->id);
        return response()->json([
            'alert'         =>  'success',
            'title'         =>  'Thành công!',
            'msg'           =>  'Đã xóa địa chỉ khỏi sổ địa chỉ.'
        ]);
    }
}
