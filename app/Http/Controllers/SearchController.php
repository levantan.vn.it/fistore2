<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
    	$query = Product::whereHidden(0)->whereDrafted(0)->whereTrashed(0);

    	if($request->filled('category')) {
            $category = \App\ProductCategory::whereSlug($request->category)->first();
            $query->where('category_id', $category->id);
        }
    	if($request->filled('brand')) {
            $brand = \App\ProductBrand::whereSlug($request->brand)->first();
            $query->where('brand_id', $brand->id);
        }
    	if($request->filled('keywords'))
    		$query->where(function($string) use ($request){
    			$string->where('name', 'LIKE', '%' . str_replace(' ', '%', $request->keywords) . '%');
    			$string->orWhere('slug', 'LIKE', '%' . str_replace(' ', '%', $request->keywords) . '%');
    			$string->orWhere('description', 'LIKE', '%' . str_replace(' ', '%', $request->keywords) . '%');
    			$string->orWhere('title', 'LIKE', '%' . str_replace(' ', '%', $request->keywords) . '%');
    			$string->orWhere('tags', 'LIKE', '%' . str_replace(' ', '%', $request->keywords) . '%');
    		});

    	if($request->filled('tag'))
    		$string->where('tags', 'LIKE', '%' . str_replace(' ', '%', $request->tag) . '%');

    	if(request()->filled('sort')) {
            if(request()->sort == 'min_cost') {
                $query->orderBy('price', 'asc');
            }
            else if(request()->sort == 'max_cost') {
                $query->orderBy('price', 'desc');
            }
        }
        $data = $query->orderBy('reposted_at', 'desc')
                        ->orderBy('created_at', 'desc')
                        ->paginate(20);
        $data_new = $data->map(function ($item) {
            $sale = $item->fk_sales()->where('status',1)->where('started_at','<=',date('Y-m-d H:i:s'))->where('expired_at','>=',date('Y-m-d H:i:s'))->first();
            if(!empty($sale)){
                if($sale->price_type == 1){
                    $item->price = floatval($item->original_price) - (floatval($item->original_price)*$sale->price_down/100);
                }
                else{
                    $item->price = floatval($item->original_price) -floatval($sale->price_down);
                }
            }
            return $item;
        });
        if(request()->filled('sort')) {
            if(request()->sort == 'min_cost') {
                $data_new = $data_new->sortBy(function ($item, $key) {
                    return $item->price;
                });
            }else if(request()->sort == 'max_cost') {
                $data_new = $data_new->sortByDesc(function ($item, $key) {
                    return $item->price;
                });
            }
            
        }

        $categories = \App\ProductCategory::whereNull('parent_id')->get();
        $brands = \App\ProductBrand::whereNull('parent_id')->get();

        return view('search', compact('categories', 'brands', 'data'));
    }
}
