<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class HomeController extends Controller
{
    public function home()
    {
        $freetrialProducts = $this->getFreeTrialProducts();


        $SaleOfProducts = $this->getProductListInGroup(6);
        $comboProducts = $this->getComboProducts();

        $newestProducts = $this->getProductListInGroup(8);
        $goodSaleProducts = $this->getProductListInGroup(9);

        // PICK ME NOW -> GO (đang chạy)
        $hotDealSelling = $this->getHotDealProducts('SLG');
        // PICK ME NOW -> READY (sắp mở bán)
        $hotDealAboutToSell = $this->getHotDealProducts('ATS');
        // PICK ME NOW -> OVER (hết hạn)
        $hotDealExpired = $this->getHotDealProducts('EXP');

        // TODO: Lấy danh sách chiến dịch khuyến mãi (chương trình khuyến mãi)
        $campaignSale = $this->getProductInCampaignSale();

        $articles = \App\Article::whereTrashed(false)->whereDrafted(false)->whereHidden(false)->orderBy('reposted_at', 'desc')->take(5)->get();
        return view('home', compact(
            'freetrialProducts',
            'SaleOfProducts',
            'comboProducts',
            'newestProducts',
            'goodSaleProducts',
            'hotDealSelling',
            'hotDealAboutToSell',
            'hotDealExpired',
            'articles',
            'campaignSale'
        ));
    }

    public function getFreeTrialProducts()
    {
        return \App\FreeTrial::whereDisabled(false)
            ->where('expired_at', '>', date('Y-m-d H:i:s'))
            ->join('products', 'products.id', '=', 'free_trial.product_id')
            ->join('product_brands', 'product_brands.id', '=', 'products.brand_id')
            ->where('products.is_combo', false)
            ->where('products.trashed', false)
            ->where('products.drafted', false)
            ->orderBy('free_trial.position', 'asc')
            ->orderBy('free_trial.created_at', 'desc')
            ->select('products.name', 'free_trial.quantity', 'products.thumbnail', 'started_at', 'expired_at', 'target_url', 'product_brands.slug as brand_slug', 'product_brands.name as brand_name')
            ->get();
    }

    public function getHotDealProducts($status = 'SLG')
    {
        $query = \App\HotDeal::whereDisabled(false)
            ->join('products', 'products.id', '=', 'hot_deals.product_id')
            ->join('product_brands', 'product_brands.id', '=', 'products.brand_id')
            ->where('products.is_combo', false)
            ->where('products.trashed', false)
            ->where('products.drafted', false)
            ->where('products.closed', false);

        if ($status == 'SLG') {
            $query->where('started_at', '<=', date('Y-m-d H:i:s'))
                ->where('expired_at', '>', date('Y-m-d H:i:s'));
        }

        if ($status == 'ATS') {
            $query->where('started_at', '>', date('Y-m-d H:i:s'))
                ->where('expired_at', '>', date('Y-m-d H:i:s'));
        }
        /*
            ducngo
            add take 10
        */
        if ($status == 'EXP') {
            $query->where(function ($cond) {
                $cond->where('expired_at', '<', date('Y-m-d H:i:s'));
            })->take(10);
        }

        return $result = $query
            ->orderBy('hot_deals.created_at', 'desc')
            ->select('products.name', 'products.slug', 'hot_deals.price', 'hot_deals.original_price', 'hot_deals.quantity', 'hot_deals.available_qty', 'products.thumbnail', 'started_at', 'expired_at', 'product_brands.slug as brand_slug', 'product_brands.name as brand_name')
            ->get();
    }

    public function getComboProducts()
    {
        return \App\Combo::where('products.is_combo', true)->where('products.hidden', false)
            ->join('combo_details', 'combo_details.combo_id', '=', 'products.id')
            ->select('products.*')
            ->orderBy('products.position', 'asc')
            ->orderBy('products.reposted_at', 'desc')
            ->orderBy('products.created_at', 'desc')
            ->distinct()
            ->take(6)
            ->get();
    }

    public function getProductListInGroup($group_id)
    {
        return \App\Product::where('products.is_combo', false)
            ->where('products.trashed', false)
            ->where('products.drafted', false)
            ->where('products.hidden', false)
            ->where('products.closed', false)
            ->where(function ($query) {
                $query->where('products.status', '!=' , -1) // bỏ sản phẩm có trạng thái là Ngừng kinh doanh
                      ->orWhereNull('products.status');
            })
            ->join('product_group_relationships', 'product_group_relationships.product_id', '=', 'products.id')
            ->where('product_group_relationships.group_id', $group_id)
            ->orderBy('products.position', 'asc')
            ->orderBy('products.reposted_at', 'desc')
            ->orderBy('products.created_at', 'desc')
            ->distinct()
            ->take(6)
            ->get();
    }


    /**
     * Lấy danh sách các sản phẩm trong "Chương trình khuyến mãi" [table Sale]
     *  (Không phải các sản phẩm khuyến mãi trong nhóm "Khuyến mãi")
     */
    public function getProductInCampaignSale()
    {
        
        $listSales = \App\Sales::where('sales.status', 1)
            ->where('sales.started_at', '<=', date('Y-m-d H:i:s'))
            ->where('sales.expired_at', '>=', date('Y-m-d H:i:s'))
            ->orderBy('sales.created_at', 'desc')
            ->get();

        $listProduct = array();
        foreach ($listSales as $key => $sales) {

            $listProductTemp = $sales->fk_products()
            ->whereHidden(false)->whereDrafted(false)->whereTrashed(false)
            ->where(function ($query) {
                $query->where('status', '!=' , -1) // bỏ sản phẩm có trạng thái là Ngừng kinh doanh
                    ->orWhereNull('status');
            })
            ->select('products.id', 'products.thumbnail', 'products.slug', 'products.name', 'products.drafted',
            'products.code', 'products.closed', 'products.hidden', 'products.position', 'products.position_in_cat',
            'products.price', 'products.original_price', 'products.display_price', 'products.quantity', 'products.category_id', 'products.brand_id')
            ->get();

            foreach ($listProductTemp as $product) {
                if (array_search($product->id, array_column($listProduct, 'id')) === false){
                    $product->started_at = $sales->started_at;
                    $product->expired_at = $sales->expired_at;

                    // TODO: Tính lại giá sau giảm của sản phẩm
                    //      (nếu price_type của sale = 1 là giảm giá trị, = 2 là giảm theo %)
                    if($sales->status == 1){
                        if($product->original_price > 0 ){
                            if($sales->price_type == 1){
                                $product->price = floatval($product->original_price) - (floatval($product->original_price)*$sales->price_down/100);
                            }else{
                                $product->price = floatval($product->original_price) -floatval($sales->price_down);
                            }
                        }else{
                            if($sales->price_type == 1){
                                $product->original_price = $product->price;
                                $product->price = floatval($product->original_price) - (floatval($product->original_price)*$sales->price_down/100);
                            }else{
                                $product->price = floatval($product->original_price) -floatval($sales->price_down);
                            }
                        }
                    }
                    array_push($listProduct, $product);
                }
            }
        }
        return $listProduct;
    }
}
