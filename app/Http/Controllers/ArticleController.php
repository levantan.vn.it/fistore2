<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleCategory;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function showArticleList()
    {
        $data = Article::whereHidden(0)->whereDrafted(0)->whereTrashed(0)->orderBy('reposted_at', 'desc')->orderBy('created_at')->paginate(8);
        $featured = Article::whereHidden(0)->whereDrafted(0)->whereTrashed(0)->whereFeatured(true)->orderBy('reposted_at', 'desc')->orderBy('created_at')->take(5)->get();

        $mostedArticles = $this->mostedArticles();
        return view('show_all_articles', compact('data', 'featured', 'mostedArticles'));
    }

	public function showArticleDetail($article_slug)
	{
		$data = Article::whereHidden(0)->whereDrafted(0)->whereTrashed(0)->whereSlug($article_slug)->firstOrFail();

        $mostedArticles = $this->mostedArticles();
        $newestArticles = $this->newestArticles();
        $relatedArticles = $this->relatedArticles();

        return view('show_article_content', compact('data', 'mostedArticles', 'newestArticles', 'relatedArticles'));
	}

    public function showArticleListInCategory($category_slug)
    {
    	$list = ArticleCategory::whereSlug($category_slug)->firstOrFail();
        $data = $list->fk_paginated_articles(16);
    	return view('show_articles_in_list', compact('list', 'data'));
    }

    public function mostedArticles()
    {
        return Article::whereHidden(0)->whereDrafted(0)->whereTrashed(0)->orderBy('view', 'asc')->orderBy('reposted_at', 'desc')->orderBy('created_at')->take(8)->get();
    }


    public function newestArticles()
    {
        return Article::whereHidden(0)->whereDrafted(0)->whereTrashed(0)->orderBy('reposted_at', 'desc')->orderBy('created_at')->take(8)->get();
    }

    public function relatedArticles()
    {
        return Article::whereHidden(0)->whereDrafted(0)->whereTrashed(0)->orderBy('reposted_at', 'desc')->orderBy('created_at')->take(9)->get();
    }
}
