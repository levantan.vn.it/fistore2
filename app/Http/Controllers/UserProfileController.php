<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    public function getProfile()
    {
    	$data = Auth::guard('customer')->user();
    	return view('user_profile', compact('data'));
    }

    public function updateProfile(Request $request)
    {
    	$data = User::find(Auth::guard('customer')->user()->id);
    	$data->name = $request->name;
    	$data->email = $request->email;
    	$data->phone = $request->phone;
    	$data->gender = $request->gender ?? null;
        if($request->filled('year') && $request->filled('month') && $request->filled('day'))
    	   $data->date_of_birth = $request->year .'-'. $request->month .'-'. $request->day;
    	$data->save();
        //$this->customerUpdate($data);

    	if($request->filled('change_password')) {
            if(!empty($data->password)){
                if($request->old_password == $request->password)
                    return back()->with(['password_error' => 'Mật khẩu mới phải khác mật khẩu cũ.']);

                if(!(\Hash::check($request->old_password, Auth::guard('customer')->user()->password)))
                    return back()->with(['old_password_error' => 'Mật khẫu cũ không chính xác.']);
            }
    		
    		$data = User::find(Auth::guard('customer')->user()->id);
    		$data->password = bcrypt($request->password);
    		$data->save();
    	}

    	return back()->with([
    		'alert'	=> 'success',
    		'title'	=> 'Hoàn tất',
    		'message'	=> 'Đã cập nhật thông tin tài khoản.'
    	]);
    }

    public function customerUpdate($data)
    {
        if(empty($data->id)){
            return response()->json([
                'alert' => 'error',
                'title' => 'Lỗi!',
                'msg' => 'Không tìm thấy khách hàng.'
            ]);
        }
        $id = $data->id;
        $data = User::where('id',$id)->first();
        if(empty($data)){
            return response()->json([
                'alert' => 'error',
                'title' => 'Lỗi!',
                'msg' => 'Không tìm thấy khách hàng.'
            ]);
        }
        $data_result = [];
        $url = 'services/apexrest/afrfistore/customerUpdate';
        $params = [
            'customerName'   =>  $data->name,
            'customerCode'  =>  $data->id,
            'phoneNumber'   =>  $data->phone,
            'email'   =>  $data->email,
            'gender'   =>  $data->gender == 1?'Male':'Female',
            'dateOfBirth'   =>  $data->date_of_birth

        ];
        $access_totken = $this->getAccessTokenByDB();
        // $access_totken = "00DR0000001znou!AQcAQGgbujba0eAAhQB6lDdGlf_K6GWb2CCoMO71BhBbVImlCZeO1laCYlk2F9_UBMhH.TNmCfG8WSVWi7PuxKpTwOhhMxpV";
        $result = $this->getAPI($url,$params,$access_totken);
        
        if(!empty($result['errorCode']) && $result['errorCode'] == 'INVALID_SESSION_ID'){
            // Save log lỗi access token vào get lại access token mới
            $access_totken = $this->getAccessToken();
            $result = $this->getAPI($url,$params,$access_totken);
        }
        // dd($result);
        if(empty($result['success'])){
            if(!empty($result['errorMessages']['customerCode'])){
                $message = $result['errorMessages']['customerCode'];
            }elseif(!empty($result['errorMessages']['customerName'])){
                $message = $result['errorMessages']['customerName'];
            }elseif(!empty($result['errorMessages']['phoneNumber'])){
                $message = $result['errorMessages']['phoneNumber'];
            }elseif(!empty($result['errorMessages']['email'])){
                $message = $result['errorMessages']['email'];
            }elseif(!empty($result['errorMessages']['gender'])){
                $message = $result['errorMessages']['gender'];
            }elseif(!empty($result['errorMessages']['dateOfBirth'])){
                $message = $result['errorMessages']['dateOfBirth'];
            }else{
                $message = 'error';
            }
            // save log lỗi
            return response()->json([
                'alert' => 'error',
                'title' => 'Lỗi',
                'msg' => $message
            ]);
        }
        
        $customerId = $result['data'];
        return response()->json([
            'alert' => 'success',
            'title' => 'Hoàn tất!',
            'msg' => 'Cập nhật khách hàng thành công.',
            'data'  =>  $data->id
        ]);
    }
}
