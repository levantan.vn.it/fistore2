<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Order API 
 */
use App\AccessCode;
use App\ProductOrder;
class OrderApiController extends ApiController
{ 
  
  	public function deliveryStatusUpdate(Request $request)
  	{
  	    $data = $request->all();
  	    
  	    if(empty($data['accessCode'])){
  	    	$mes = ['accessCode'=>'Access Code is required'];
  	    	return $this->sendErrorResponse($mes);
  	    }
  	    if(empty($data['webOrderCode'])){
  	    	$mes = ['webOrderCode'=>'webOrderCode is required'];
  	    	return $this->sendErrorResponse($mes);
  	    }
  	    if(empty($data['orderStatus'])){
  	    	$mes = ['orderStatus'=>'orderStatus is required'];
  	    	return $this->sendErrorResponse($mes);
  	    }
  	    try {
  	    	$accessCode = AccessCode::where('access_code',$data['accessCode'])->first();
  	    	if(empty($accessCode)){
  	    		$mes = ['accessCode'=>'Access Code expired or invalid'];
  	    		return $this->sendErrorResponse($mes);
  	    	}
  	    } catch (\Illuminate\Database\QueryException $e) {
  			$mes = ['accessCode'=>'Access Code expired or invalid'];
  			return $this->sendErrorResponse($mes);
  	    }
  	    
  	    $order = ProductOrder::where('external_id',$data['webOrderCode'])->first();
  	    if(empty($order)){
  	    	$mes = ['webOrderCode'=>"Could not find order with webOrderCode '".$data['webOrderCode']."'"];
  	    	return $this->sendErrorResponse($mes);
  	    }
  	    if($data['orderStatus'] != 'In Delivery'){
  	    	$mes = ['orderStatus'=>'orderStatus invalid'];
  	    	return $this->sendErrorResponse($mes);
  	    }
        if($order->status == 2){
            $mes = ['webOrderCode'=>"Order with webOrderCode '".$data['webOrderCode']."' is shipping"];
            return $this->sendErrorResponse($mes);
        }
        if($order->status == 3){
            $mes = ['webOrderCode'=>"Order with webOrderCode '".$data['webOrderCode']."' have completed"];
            return $this->sendErrorResponse($mes);
        }
  	    // Vận chuyển đơn hàng
  	    $order->status = 2;
  	    try {
  	    	$update =  $order->save();
  	    	if(empty($update)){
  	    		$mes = ['webOrderCode'=>"Could not update with webOrderCode '".$data['webOrderCode']."'"];
  	    		return $this->sendErrorResponse($mes);
  	    	}
  	    } catch (\Illuminate\Database\QueryException $e) {
  	    	$mes = ['webOrderCode'=>"Could not update with webOrderCode '".$data['webOrderCode']."'"];
  	    	return $this->sendErrorResponse($mes);
  	    }
  	    
  	    $result = ['webOrderCode'=>$data['webOrderCode']];
  	    return $this->sendSuccessResponse($result);
  	}  
}
