<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;

class AjaxController extends Controller
{
    public function getDistrict(Request $request)
    {
        $html = '';
        if ($request->ajax()) {
            if($request->filled('province_id')){
                $districts = \App\District::where('province_id', $request->province_id)->get();
                $html = '<option value="">Chọn Quận/Huyện</option>';
                foreach($districts as $item) {
                    $html.= '<option value="' . $item->id . '">' . $item->name . '</option>';
                }
            }else{
                $html = '<option value=""></option>';
            }
        }
        echo $html;
    }

    public function getWard(Request $request)
    {
        $html = '';
        if ($request->ajax()) {
            if($request->filled('district_id')){
                $wards = \App\Ward::where('district_id', $request->district_id)->get();
                $html = '<option value="">Chọn Phường/Xã</option>';
                foreach($wards as $item) {
                    $html.= '<option value="' . $item->id . '">' . $item->name . '</option>';
                }
            }else{
                $html = '<option value=""></option>';
            }
        }
        echo $html;
    }
    public function deliveryOrder()
    {
        $data_customer = session('data_customer');
        $check = $this->customerUpdate($data_customer);
        if($check){
            $product_delivery = session('product_delivery');
            $orderDetail = session('order');
            
            if(empty($product_delivery) || empty($orderDetail)){
                session()->forget('data_customer');
                session()->forget('product_delivery');
                session()->forget('order');
                // save log lỗi
                echo 'false0';return;
            }
            $data_result = [];
            $url = 'services/apexrest/afrfistore/deliveryOrder';
            $paymentMethod = "Cash";
            $handOverMethod = "Drop-Off";
            $cargoInsuranceType = "Premium(100% Value)";
            $purchaseDate = date('Y-m-d');
            $deliveryTargetDate = date('Y-m-d', strtotime($purchaseDate . ' +3 day'));
            $productLocator  = "DMNC Warehouse";
            $shippingService = "SDP(DHL Parcel Metro)";
            $shippingTariff = $orderDetail['transport_price'];
            $customerCode = $orderDetail['user_id'];
            $vasType = "Testing";
            $vasRequirement = "Test Requirement";
            $cod = $orderDetail['total'];
            $deliveryAddress = $orderDetail['address'];
            $params = [
                'webOrderCode'   =>  $orderDetail['code'],
                'products'  =>  $product_delivery,
                'productLocator'   =>  $productLocator,
                'customerCode'   =>  (string)$customerCode,
                'vasType'   =>  $vasType,
                'vasRequirement'   =>  $vasRequirement,
                'shippingService'   =>  $shippingService,
                'cod'   =>  number_format($cod,0,'.',''),
                'shippingTariff'   =>  number_format($shippingTariff,0,'.',''),
                'purchaseDate'   =>  $purchaseDate,
                'deliveryTargetDate'   =>  $deliveryTargetDate,
                'deliveryAddress'   =>  $deliveryAddress,
                'paymentMethod'   =>  $paymentMethod,
                'handOverMethod'   =>  $handOverMethod,
                'cargoInsuranceType'   =>  $cargoInsuranceType

            ];
            // dd($params);
            $access_totken = $this->getAccessTokenByDB();
            // $access_totken = "00DR0000001znou!AQcAQGgbujba0eAAhQB6lDdGlf_K6GWb2CCoMO71BhBbVImlCZeO1laCYlk2F9_UBMhH.TNmCfG8WSVWi7PuxKpTwOhhMxpV";
            $result = $this->getAPI($url,$params,$access_totken);
            
            if(!empty($result[0]) && !empty($result[0]['errorCode']) && $result[0]['errorCode'] == 'INVALID_SESSION_ID'){
                // Save log lỗi access token vào get lại access token mới
                $access_totken = $this->getAccessToken();
                $result = $this->getAPI($url,$params,$access_totken);
            }
            if(empty($result['success'])){
                $message = $result['errorMessages'];
                session()->forget('data_customer');
                session()->forget('product_delivery');
                session()->forget('order');
                // save log lỗi
                echo 'false1';return;
            }
            session()->forget('data_customer');
            session()->forget('product_delivery');
            session()->forget('order');
            $orderResult = $result['data'];
            echo 'true';return;
        }else{
            session()->forget('data_customer');
            session()->forget('product_delivery');
            session()->forget('order');
            echo 'false2';return;
            // save log error
        }
            
        
    }

    public function customerUpdate($data)
    {
        if(empty($data)){
            return false;
        }
        $data_result = [];
        $url = 'services/apexrest/afrfistore/customerUpdate';
        $params = [
            'customerName'   =>  $data['name'],
            'customerCode'  =>  (string)$data['id'],
            'phoneNumber'   =>  $data['phone'],
            'email'   =>  $data['email'],
            'gender'   =>  $data['gender'] == 1?'Male':'Female',
            'dateOfBirth'   =>  date('Y-m-d',strtotime($data['date_of_birth']))

        ];
        $access_totken = $this->getAccessTokenByDB();
        $result = $this->getAPI($url,$params,$access_totken);

        if(!empty($result[0]) && !empty($result[0]['errorCode']) && $result[0]['errorCode'] == 'INVALID_SESSION_ID'){
            // Save log lỗi access token vào get lại access token mới
            $access_totken = $this->getAccessToken();
            $result = $this->getAPI($url,$params,$access_totken);
        }
        if(empty($result['success'])){
            $message = "Lỗi";
            if(!empty($result['errorMessages']['customerCode'])){
                $message = $result['errorMessages']['customerCode'];
            }elseif(!empty($result['errorMessages']['customerName'])){
                $message = $result['errorMessages']['customerName'];
            }elseif(!empty($result['errorMessages']['phoneNumber'])){
                $message = $result['errorMessages']['phoneNumber'];
            }elseif(!empty($result['errorMessages']['email'])){
                $message = $result['errorMessages']['email'];
            }elseif(!empty($result['errorMessages']['gender'])){
                $message = $result['errorMessages']['gender'];
            }elseif(!empty($result['errorMessages']['dateOfBirth'])){
                $message = $result['errorMessages']['dateOfBirth'];
            }
            // save log lỗi
            return false;
        }
        
        $customerId = $result['data'];
        // save log success
        return true;
    }
}


