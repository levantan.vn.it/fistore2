<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductGroup;
use App\ProductBrand;
use App\ProductCategory;
use Illuminate\Http\Request;

class ProductGroupController extends Controller
{
    public function showProductListInGroup($group_slug)
    {
    	$group = ProductGroup::whereSlug($group_slug)->first();
        if(empty($group)){
            return redirect()->route('home');
        }
    	$categories = ProductCategory::whereNull('parent_id')->get();
    	$brands = ProductBrand::whereNull('parent_id')->get();

        $query = Product::where('products.is_combo', false)->where('products.trashed', false)
                    ->where('products.drafted', false)
                    ->where('products.hidden', false)
                    ->join('product_group_relationships', 'product_group_relationships.product_id', '=', 'products.id')
                    ->where('product_group_relationships.group_id', $group->id);
                    

    	if(request()->filled('category')) {
    		$query->where('products.category_id', request()->category);
    	}

        if(request()->filled('brand')) {
            $query->where('products.brand_id', request()->brand);
        }

    	if(request()->filled('sort')) {
    		if(request()->sort == 'min_cost') {
    			$query->orderBy('products.price', 'asc');
    		}
    		else if(request()->sort == 'max_cost') {
    			$query->orderBy('products.price', 'desc');
    		}
    	}
    	$data = $query
                    ->orderBy('products.position', 'asc')
                    ->orderBy('products.reposted_at', 'desc')
                    ->orderBy('products.created_at', 'desc')
                    ->distinct()
                    ->paginate(20);
        $data_new = $data;
        $data_new = $data->map(function ($item) {
            $sale = $item->fk_sales()->where('status',1)->where('started_at','<=',date('Y-m-d H:i:s'))->where('expired_at','>=',date('Y-m-d H:i:s'))->first();
            if(!empty($sale)){
                if($sale->price_type == 1){
                    $item->price = floatval($item->original_price) - (floatval($item->original_price)*$sale->price_down/100);
                }
                else{
                    $item->price = floatval($item->original_price) -floatval($sale->price_down);
                }
            }
            return $item;
        });
        if(request()->filled('sort')) {
            if(request()->sort == 'min_cost') {
                $data_new = $data_new->sortBy(function ($item, $key) {
                    return $item->price;
                });
            }else if(request()->sort == 'max_cost') {
                $data_new = $data_new->sortByDesc(function ($item, $key) {
                    return $item->price;
                });
            }
            
        }
    	return view('group_products', compact('group', 'categories', 'brands', 'data','data_new'));
    }
}
