<?php

namespace App\Http\Controllers\Admin;

use App\Newsletter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsletterController extends Controller
{
    public function index()
    {
        $this->authorize('newsletter-access');
        $data = Newsletter::orderBy('created_at', 'desc')->get();
        return view('admin.newsletter-list', compact('data'));
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('newsletter-access');
        if($request->ajax()){
            Newsletter::destroy($id);
            
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }
}
