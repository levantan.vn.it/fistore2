<?php

namespace App\Http\Controllers\Admin;

use App\Media;
use App\Slide;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlideController extends Controller
{
    protected $path = 'media/slides/';
    protected $dimensions = null;

    public function index()
    {
        $this->authorize('slide-access');
        $data = Slide::whereNotIn('id', [1,6])->get();
        return view('admin.slide-list', compact('data'));
    }

    public function banner()
    {
        $this->authorize('slide-access');
        $data = Slide::whereIn('id', [1,6])->get();
        return view('admin.slide-list', compact('data'));
    }

    public function store(Request $request)
    {
        $this->authorize('slide-access');
        $main = Slide::find($request->target_id);

        if($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $name = str_slug(get_config()->name) .'-'. str_slug($main->name) .'-'. str_random('5');
            $slug = upload_file($file, $name, $this->path, $this->dimensions);

            $media = new Media;
            $media->target_id = $request->target_id;
            $media->target_type = 'slide';
            $media->target_url = $request->target_url;
            $media->name = $name;
            $media->size = $file->getClientSize();
            $media->type = $file->getClientMimeType();
            $media->path = $slug;
            $media->index = $request->index ?? 0;
            $media->save();
        }

        return back()->with([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg'   => 'Đã thêm ảnh vào Slide'
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->authorize('slide-access');
        $main = Slide::find($request->target_id);
        $media = Media::find($id);

        if($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $name = str_slug(get_config()->name) .'-'. str_slug($main->name) .'-'. str_random('5');
            $slug = upload_file($file, $name, $this->path, $this->dimensions);

            /** Xóa ảnh cũ */
            if(\File::exists($this->path.$media->path)) \File::delete($this->path.$media->path);

            $media->name = $name;
            $media->size = $file->getClientSize();
            $media->type = $file->getClientMimeType();
            $media->path = $slug;
        }
        $media->target_url = $request->target_url;
        $media->index = $request->index ?? 0;
        $media->save();

        return back()->with([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg'   => 'Đã thêm ảnh vào Slide'
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('slide-access');
        if($request->ajax()){
            $media = Media::find($id);
            if(\File::exists($this->path.$media->path)) \File::delete($this->path.$media->path);

            Media::destroy($id);

            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }
}
