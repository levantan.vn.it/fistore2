<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\FreeTrial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FreeTrialController extends Controller
{
    public function index()
    {
        $this->authorize('freetrial-access');
        $data = FreeTrial::orderBy('position')->get();
        return view('admin.freetrial-list', compact('data'));
    }

    public function create()
    {
        $this->authorize('freetrial-access');
        $products = Product::where('is_combo', false)->where('trashed', false)->where('drafted', false)->get();
        return view('admin.freetrial-create', compact('products'));
    }

    public function store(Request $request)
    {
        $this->authorize('freetrial-access');
        $product = Product::find($request->product);
        $data = new FreeTrial;
        $data->product_id = $request->product;
        $data->quantity = $request->quantity;
        $data->target_url = $request->target_url;
        $data->started_at = substr($request->started_at, 6, 4) . '-' . substr($request->started_at, 3, 2) . '-' . substr($request->started_at, 0, 2) . ' ' . str_replace(' ', '', $request->started_at_time);
        $data->expired_at = substr($request->expired_at, 6, 4) . '-' . substr($request->expired_at, 3, 2) . '-' . substr($request->expired_at, 0, 2) . ' ' . str_replace(' ', '', $request->expired_at_time);
        $data->disabled = $request->disabled ? true : false;
        $data->save();

        return redirect()->route('admin.freetrial.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã thêm sản phẩm trải nghiệm mới.'
        ]);
    }

    public function edit($id)
    {
        $this->authorize('freetrial-access');
        $products = Product::where('is_combo', false)->where('trashed', false)->where('drafted', false)->get();
        $data = FreeTrial::find($id);
        return view('admin.freetrial-edit', compact('products', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('freetrial-access');
        $data = FreeTrial::find($id);
        $data->quantity = $request->quantity;
        $data->target_url = $request->target_url;
        $data->started_at = substr($request->started_at, 6, 4) . '-' . substr($request->started_at, 3, 2) . '-' . substr($request->started_at, 0, 2) . ' ' . str_replace(' ', '', $request->started_at_time);
        $data->expired_at = substr($request->expired_at, 6, 4) . '-' . substr($request->expired_at, 3, 2) . '-' . substr($request->expired_at, 0, 2) . ' ' . str_replace(' ', '', $request->expired_at_time);
        $data->disabled = $request->disabled ? true : false;
        $data->save();

        return redirect()->route('admin.freetrial.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã cập nhật sản phẩm.'
        ]);
    }

    public function destroy($id)
    {
        $this->authorize('freetrial-access');
        FreeTrial::destroy($id);
        return response()->json([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg' => 'Đã xóa bỏ 1 mục.'
        ]);
    }

    public function position(Request $request)
    {
        $current = FreeTrial::find($request->id);
        $cur_pos = $current->position;
        $new_pos = $request->position;

        if($cur_pos < $new_pos) {
            $before = FreeTrial::where('position', '>', $cur_pos)->where('position', '<=', $new_pos)->where('id', '!=', $current->id)->orderBy('position')->get();
            foreach($before as $bf){
                $up = FreeTrial::find($bf->id);
                $up->position -= 1;
                $up->save();
            }
        }else if($cur_pos > $new_pos) {
            $after = FreeTrial::where('position', '<', $cur_pos)->where('position', '>=', $new_pos)->where('id', '!=', $current->id)->orderBy('position')->get();
            foreach($after as $at){
                $dn = FreeTrial::find($at->id);
                $dn->position += 1;
                $dn->save();
            }
        }

        $current->position = $new_pos;
        $current->save();

        return response()->json([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg' => 'Đã cập nhật thứ tự hiển thị.'
        ]);
    }
}
