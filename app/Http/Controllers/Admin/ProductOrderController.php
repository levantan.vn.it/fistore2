<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Province;
use App\District;
use App\Ward;
use App\Product;
use App\ProductOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\ProductOrderExport;
use DB;

class ProductOrderController extends Controller
{
    protected $status = array(
        0 => 'Chờ duyệt',
        4 => 'Đã gọi',
        1 => 'Xác nhận',
        2 => 'Đóng gói',
        3 => 'Hoàn thành',
    );

    protected $actions = array(
        3 => 'Đã gọi',
        0 => 'Xác nhận',
        1 => 'Đóng gói',
        2 => 'Hoàn thành',
    );

    protected $arrStatus = array(
        [
            'name'  => 'Chờ duyệt',
            'value' => 0,
            'sort'  => 0,
        ],
        [
            'name'  => 'Đã gọi',
            'value' => 4,
            'sort'  => 1,
        ],
        [
            'name'  => 'Xác nhận',
            'value' => 1,
            'sort'  => 2,
        ],
        [
            'name'  => 'Đóng gói',
            'value' => 2,
            'sort'  => 3,
        ],
        [
            'name'  => 'Hoàn thành',
            'value' => 3,
            'sort'  => 4,
        ],
    );

    public function searchFilter(Request $request)
    {
        $query = DB::table('product_orders');

        if(request()->filled('tab')){
            if(request()->tab == 'cancelled'){
                $query->whereDelayed(false)->whereContacted(false)->whereCancelled(true)->whereReturn(false);
            }else if(request()->tab == 'contacted'){
                $query->whereDelayed(false)->whereContacted(true)->whereCancelled(false)->whereReturn(false);
            }else if(request()->tab == 'delayed'){
                $query->whereDelayed(true)->whereContacted(false)->whereCancelled(false)->whereReturn(false);
            }else if(request()->tab == 'return'){
                $query = ProductOrder::whereDelayed(false)->whereContacted(false)->whereCancelled(false)->whereReturn(true);
            }else{
                $query->whereDelayed(false)->whereContacted(false)->whereStatus(request()->tab)->whereCancelled(false)->whereReturn(false);
            }
        }
        
        $query->select([
            'product_orders.id',
        ]);

        if($request->filled('code'))
            $query->where('product_orders.code', $request->code);
        if($request->filled('created_at_from'))
            $query->where('product_orders.created_at', '>=', substr($request->created_at_from, -4).'-'.substr($request->created_at_from, 3, 2).'-'.substr($request->created_at_from, 0,2));
        if($request->filled('created_at_to'))
            $query->where('product_orders.created_at', '<=', substr($request->created_at_to, -4).'-'.substr($request->created_at_to, 3, 2).'-'.substr($request->created_at_to, 0,2).' 23:59:59');
        if($request->filled('admin_id'))
            $query->where('product_orders.admin_id', $request->admin_id);
        if($request->filled('name'))
            $query->where('product_orders.name', 'LIKE', '%'.str_replace(' ', '%', $request->name).'%');
        if($request->filled('email'))
            $query->where('product_orders.email', 'LIKE', '%'.$request->email.'%');
        if($request->filled('phone'))
            $query->where('product_orders.phone', 'LIKE', '%'.$request->phone.'%');
        if(!\Auth::guard('admin')->user()->can('order-ascribe'))
            $query->where('product_orders.admin_id', \Auth::guard('admin')->user()->id);
        $query->orderBy('product_orders.id', 'desc');

        $exportIds = $query->paginate($request->ex_num ?? 25)->pluck('id');

        return $exportIds;
    }

    public function filterExport(Request $request)
    {
        $qExport = DB::table('product_orders')->whereIn('product_orders.id', $this->searchFilter($request));
        $qExport->leftJoin('product_order_details', 'product_order_details.order_id', 'product_orders.id');
        $qExport->leftJoin('wards', 'product_orders.ward_id', 'wards.id');
        $qExport->leftJoin('districts', 'product_orders.district_id', 'districts.id');
        $qExport->leftJoin('provinces', 'product_orders.province_id', 'provinces.id');
        $qExport->rightJoin('products', 'products.id', 'product_order_details.product_id');
        $qExport->select([
            'product_orders.id',
            'product_orders.code',
            'product_orders.name',
            'product_orders.phone',
            'product_orders.total',
            'product_orders.note',
            'product_orders.todo',
            'product_orders.status',
            'product_orders.created_at',
            'product_orders.cancelled',
            'product_orders.contacted',
            'product_orders.delayed',
            'product_orders.return',
            'product_order_details.name as productName',
            'product_order_details.code as productCode',
            'product_order_details.quantity as productQuantity',
            'products.id as productId',
            'products.is_combo as productIsCombo',
            'product_orders.address',
            'wards.name as ward_name',
            'districts.name as district_name',
            'provinces.name as province_name'
        ]);

        $exportData = $qExport->orderBy('product_orders.id', 'desc')->get();

        return $this->export($exportData);
    }

    public function index(Request $request)
    {
        return $this->indexExportFilterDB($request);
    }

    public function indexExportFilterDB(Request $request)
    {
        $this->authorize('order-access');

        if($request->filled('export')){
            return $this->filterExport($request);
        }else{

            if(request()->filled('tab')){
                if(request()->tab == 'cancelled'){
                    $query = ProductOrder::whereDelayed(false)->whereContacted(false)->whereCancelled(true)->whereReturn(false);
                }else if(request()->tab == 'contacted'){
                    $query = ProductOrder::whereDelayed(false)->whereContacted(true)->whereCancelled(false)->whereReturn(false);
                }else if(request()->tab == 'delayed'){
                    $query = ProductOrder::whereDelayed(true)->whereContacted(false)->whereCancelled(false)->whereReturn(false);
                }else if(request()->tab == 'return'){
                    $query = ProductOrder::whereDelayed(false)->whereContacted(false)->whereCancelled(false)->whereReturn(true);
                }else{
                    $query = ProductOrder::whereDelayed(false)->whereContacted(false)->whereStatus(request()->tab)->whereCancelled(false)->whereReturn(false);
                }
            }else{
                $query = ProductOrder::whereNotNull('id');
            }

            if($request->filled('code'))
                $query->where('code', $request->code);
            if($request->filled('created_at_from'))
                $query->where('created_at', '>=', substr($request->created_at_from, -4).'-'.substr($request->created_at_from, 3, 2).'-'.substr($request->created_at_from, 0,2));
            if($request->filled('created_at_to'))
                $query->where('created_at', '<=', substr($request->created_at_to, -4).'-'.substr($request->created_at_to, 3, 2).'-'.substr($request->created_at_to, 0,2).' 23:59:59');
            if($request->filled('admin_id'))
                $query->where('admin_id', $request->admin_id);
            if($request->filled('name'))
                $query->where('name', 'LIKE', '%'.str_replace(' ', '%', $request->name).'%');
            if($request->filled('email'))
                $query->where('email', 'LIKE', '%'.$request->email.'%');
            if($request->filled('phone'))
                $query->where('phone', 'LIKE', '%'.$request->phone.'%');
            if(!\Auth::guard('admin')->user()->can('order-ascribe'))
                $query->where('admin_id', \Auth::guard('admin')->user()->id);
            $query->orderBy('created_at', 'desc');

            $data = $query->paginate($request->show ?? 25);

            $status = $this->arrStatus;
            $actions = $this->actions;

            $admins = \App\Admin::whereDisabled(false)->where('role_id', '!=', 1)->get();
            $data->appends(request()->input())->links();
            return view('admin.order-list', compact('data', 'status', 'actions', 'admins'));
        }
    }

    public function indexExportElquent(Request $request)
    {
        $this->authorize('order-access');

        if(request()->filled('tab')){
            if(request()->tab == 'cancelled'){
                $query = ProductOrder::whereDelayed(false)->whereContacted(false)->whereCancelled(true);
            }else if(request()->tab == 'contacted'){
                $query = ProductOrder::whereDelayed(false)->whereContacted(true)->whereCancelled(false);
            }else if(request()->tab == 'delayed'){
                $query = ProductOrder::whereDelayed(true)->whereContacted(false)->whereCancelled(false);
            }else{
                $query = ProductOrder::whereDelayed(false)->whereContacted(false)->whereStatus(request()->tab)->whereCancelled(false);
            }
        }else{
            $query = ProductOrder::whereNotNull('id');
        }

        if($request->filled('code'))
            $query->where('code', $request->code);
        if($request->filled('created_at_from'))
            $query->where('created_at', '>=', substr($request->created_at_from, -4).'-'.substr($request->created_at_from, 3, 2).'-'.substr($request->created_at_from, 0,2));
        if($request->filled('created_at_to'))
            $query->where('created_at', '<=', substr($request->created_at_to, -4).'-'.substr($request->created_at_to, 3, 2).'-'.substr($request->created_at_to, 0,2).' 23:59:59');
        if($request->filled('admin_id'))
            $query->where('admin_id', $request->admin_id);
        if($request->filled('name'))
            $query->where('name', 'LIKE', '%'.str_replace(' ', '%', $request->name).'%');
        if($request->filled('email'))
            $query->where('email', 'LIKE', '%'.$request->email.'%');
        if($request->filled('phone'))
            $query->where('phone', 'LIKE', '%'.$request->phone.'%');
        if(!\Auth::guard('admin')->user()->can('order-ascribe'))
            $query->where('admin_id', \Auth::guard('admin')->user()->id);
            $query->orderBy('created_at', 'desc');

        if($request->filled('export')){
            $exportData = $query->paginate($request->ex_num ?? 25);
            return $this->export($exportData);
        }else{
            $data = $query->paginate($request->show ?? 25);
        }

        $status = $this->status;
        $actions = $this->actions;

        $admins = \App\Admin::whereDisabled(false)->where('role_id', '!=', 1)->get();
        
        $data->appends(request()->input())->links();
        return view('admin.order-list', compact('data', 'status', 'actions', 'admins'));
    }

    public function create()
    {
        $this->authorize('order-access');
        $users = User::where('disabled', false)->get();
        $products = Product::where('display_price', true)->where('closed', false)->where('hidden', false)->where('drafted', false)->where('trashed', false)->get();
        $provinces = Province::all();
        return view('admin.order-create', compact('users', 'products', 'provinces'));
    }

    public function store(Request $request)
    {
        $this->authorize('order-access');
        //
    }

    public function show($id)
    {
        $this->authorize('order-access');
        $data = ProductOrder::find($id);
        $status = $this->status;
        $actions = $this->actions;
        return view('admin.order-show', compact('data', 'status', 'actions'));
    }

    public function edit($id)
    {
        $this->authorize('order-access');
    }

    public function update(Request $request, $id)
    {
        $this->authorize('order-access');
        $update = ProductOrder::find($id);
        if($request->status == 'cancel') {
            $update->delayed = false;
            $update->contacted = false;
            $update->cancelled = true;

            // Update điểm extra
            if(!is_null($update->user_id)) {
                $user = User::find($update->user_id);
                $user->extra_point -= number_format($update->total/get_setting('money_to_extra_point')->value,0,',','');
                $user->save();
            }
        }else if($request->status == 'delay') {
            $update->contacted = false;
            $update->cancelled = false;
            $update->delayed = true;
        }else if($request->status == 'contact') {
            $update->contacted = true;
            $update->cancelled = false;
            $update->delayed = false;
        }else{
            $update->contacted = false;
            $update->cancelled = false;
            $update->delayed = false;
            $update->status = $request->status;
        }
        $update->save();

        return back()->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã lưu trạng thái đơn hàng.'
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('order-access');
        if($request->ajax()){
            ProductOrder::destroy($id);
            
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }

    public function updateStatus(Request $request)
    {
        $update = ProductOrder::find($request->id);
        if($request->status == 'cancel') {
            $update->contacted = false;
            $update->delayed = false;
            $update->cancelled = true;
            
            // Update điểm extra
            if(!is_null($update->user_id)) {
                $user = User::find($update->user_id);
                $user->extra_point -= (number_format($update->total/get_setting('money_to_extra_point')->value,0,',',''));
                $user->save();
            }

            //  Cập nhật (hoàn) lại số lượng sản phẩm khi hủy đơn hàng
             DB::select('call product_order_delete (?)', array($update->id));

        }else if($request->status == 'delay') {
            $update->contacted = false;
            $update->delayed = true;
            $update->cancelled = false;
            $update->return = false;
        }else if($request->status == 'contact') {
            $update->contacted = true;
            $update->delayed = false;
            $update->cancelled = false;
            $update->return = false;
        }else if($request->status == 'return') {
            $update->contacted = false;
            $update->delayed = false;
            $update->cancelled = false;
            $update->return = true;
        }else{
            $update->contacted = false;
            $update->cancelled = false;
            $update->delayed = false;
            $update->return = false;
            $update->status = $request->status;
        }
        $update->save();

        return response()->json([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg' => 'Đã cập nhật trạng thái đơn hàng.'
        ]);
    }

    public function updateAdmin(Request $request)
    {
        $update = ProductOrder::find($request->order_id);
        $update->admin_id = $request->admin_id;
        $update->save();

        return response()->json([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg' => 'Đã lưu chỉ định.'
        ]);
    }

    public function updateNote(Request $request, $id)
    {
        $update = ProductOrder::find($id);
        $update->todo = $request->todo;
        $update->save();

        return back()->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã cập nhật ghi chú đơn hàng.'
        ]);
    }

    public function export($data)
    {
        return \Excel::download(new ProductOrderExport($data), 'DanhSachDonHang_'.date('U').'.xlsx');
    }
}
