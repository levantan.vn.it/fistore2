<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Coupon;
use App\CouponRelationship;
use App\Product;
use App\ProductCategory;
use App\ProductBrand;
use App\ProductGroup;
use App\CouponUser;
class CouponController extends Controller
{
	protected $units = [
		0 => 'VND',
		1 => '%',
	];

	protected $target_types = [
		0 => 'Tất cả đơn hàng',
		1 => 'Sản phẩm',
		2 => 'Sản phẩm thuộc nhóm',
		3 => 'Sản phẩm thuộc danh mục',
		4 => 'Sản phẩm thuộc thương hiệu',
		5 => 'Giảm phí ship',
	];

	/**
	 * Hiển thị trang danh sách
	 */
	public function index(Request $request)
	{
		$this->authorize('coupon-access');
		if ($request->has('trash'))
			$data = Coupon::where('trashed', 1);
		else
			$data = Coupon::where('trashed', 0);
		$data = $data->orderBy('created_at', 'desc')->get();

		$units = $this->units;
		$target_types = $this->target_types;

		return view('admin.coupon-list', compact('data', 'units', 'target_types'));
	}

	public function create()
	{
		$this->authorize('coupon-access');

		$units = $this->units;
		$target_types = $this->target_types;

		$products = Product::where('trashed', false)->where('drafted', false)->where('hidden', false)->get();
		$categories = ProductCategory::all();
		$brands = ProductBrand::all();
		$groups = ProductGroup::all();

		return view('admin.coupon-create', compact('units', 'target_types', 'products', 'categories', 'brands', 'groups'));
	}

	/**
	 * Xử lý thêm mới mã khuyến mãi
	 */
	public function store(Request $request)
	{
		$this->authorize('coupon-access');

		$quantity = $request->quantity;
		$code =  $request->code;
		if (empty($code)) {
			return redirect()->route('admin.coupon.index')->with([
				'alert' => 'error',
				'title' => 'Lỗi!',
				'message' => 'Vui lòng nhập Mã khuyến mãi.'
			]);
		}

		$arrCode = array();
		// TODO: Tạo ra danh sách code theo số lượng mã được nhập
		if (strpos($code, '[')) {
			$start = strpos($code, '[');
			$end = strpos($code, ']');
			$countX = $end  - $start - 1;
			$strReplace = substr($code, $start,  $end - $start + 1);
			for ($i = 0; $i < $quantity;) {
				
				$random = Str::random($countX);
				$code_tmp = strtoupper(str_replace($strReplace, $random, $code));

				if (!in_array($code_tmp, $arrCode)) {
					$query = Coupon::whereCode($code_tmp);
					if ($query->get()->count() == 0) {
						array_push($arrCode, $code_tmp);
						$i++;
					}
				}
			}
		} else { // Chỉ 1 mã code
			$query = Coupon::whereCode(strtoupper($code));
			if ($query->get()->count() == 0) {
				array_push($arrCode, strtoupper($code));
			} else {
				return redirect()->route('admin.coupon.index')->with([
					'alert' => 'error',
					'title' => 'Lỗi!',
					'message' => 'Mã khuyến mãi đã tồn tại.'
				]);
			}
		}

		foreach ($arrCode as &$value) {
			$data = new Coupon;
			$data->code = $value;
			$data->limited = $request->limited;
			$data->discount = str_replace(',', '', $request->discount);
			$data->unit = $request->unit;
			$data->target_type = $request->target_type;
			$data->min_value = str_replace(',', '', $request->min_value);
			$data->started_at = substr($request->started_at, 6, 4) . '-' . substr($request->started_at, 3, 2) . '-' . substr($request->started_at, 0, 2) . ' ' . str_replace(' ', '', $request->started_at_time);
			$data->expired_at = substr($request->expired_at, 6, 4) . '-' . substr($request->expired_at, 3, 2) . '-' . substr($request->expired_at, 0, 2) . ' ' . str_replace(' ', '', $request->expired_at_time);
			$data->disabled = $request->disabled ? true : false;
			$data->save();

			if ($request->has('target_ids')) {
				foreach ($request->target_ids as $target_id) {
					$target = new CouponRelationship;
					$target->coupon_id = $data->id;
					$target->target_type = $data->target_type;
					$target->target_id = $target_id;
					$target->save();
				}
			}
		}

		return redirect()->route('admin.coupon.index')->with([
			'alert' => 'success',
			'title' => 'Hoàn tất!',
			'message' => 'Đã thêm Mã khuyến mãi mới.'
		]);
	}

	public function edit($id)
	{
		$this->authorize('coupon-access');

		$units = $this->units;
		$target_types = $this->target_types;

		$products = Product::where('trashed', false)->where('drafted', false)->where('hidden', false)->get();
		$categories = ProductCategory::all();
		$brands = ProductBrand::all();
		$groups = ProductGroup::all();

		$data = Coupon::find($id);

		return view('admin.coupon-edit', compact('data', 'units', 'target_types', 'products', 'categories', 'brands', 'groups'));
	}

	/**
	 * Xử lý cập nhật thông tin mã khuyễn mãi
	 */
	public function update(Request $request, $id)
	{
		$this->authorize('coupon-access');

		$data = Coupon::find($id);
		$data->code = $request->code;
		$data->limited = $request->limited;
		$data->discount = str_replace(',', '', $request->discount);
		$data->unit = $request->unit;
		$data->min_value = str_replace(',', '', $request->min_value);
		$data->started_at = substr($request->started_at, 6, 4) . '-' . substr($request->started_at, 3, 2) . '-' . substr($request->started_at, 0, 2) . ' ' . str_replace(' ', '', $request->started_at_time);
		$data->expired_at = substr($request->expired_at, 6, 4) . '-' . substr($request->expired_at, 3, 2) . '-' . substr($request->expired_at, 0, 2) . ' ' . str_replace(' ', '', $request->expired_at_time);
		$data->disabled = $request->disabled ? true : false;
		$data->save();

		if ($request->has('target_ids')) {
			CouponRelationship::where('coupon_id', $id)->delete();

			foreach ($request->target_ids as $target_id) {
				$target = new CouponRelationship;
				$target->coupon_id = $data->id;
				$target->target_type = $data->target_type;
				$target->target_id = $target_id;
				$target->save();
			}
		}

		return redirect()->route('admin.coupon.index')->with([
			'alert' => 'success',
			'title' => 'Hoàn tất!',
			'message' => 'Đã cập nhật thông tin Mã khuyễn mãi'
		]);
	}

	/**
	 * Xử lý xóa Mã khuyến mãi hoàn toàn
	 * Dữ liệu truyền vào là một mảng danh sách các id
	 * theo phương thức POST
	 */
	public function destroy($id)
	{
		$this->authorize('coupon-access');
		Coupon::destroy($id);
		return response()->json([
			'alert'         => 'success',
			'title'         => 'Hoàn tất!',
			'message'       => 'Đã xóa mã khuyến mãi vĩnh viễn.',
		]);
	}

	public function trash(Request $request)
	{
		$this->authorize('coupon-access');
		if ($request->ajax()) {
			Coupon::whereId($request->id)->update(['trashed' => true]);
			return response()->json([
				'alert' => 'success',
				'title' => 'Hoàn tất',
				'msg' => 'Đã chuyển 1 mục vào thùng rác.'
			]);
		}
	}

	public function restore(Request $request)
	{
		$this->authorize('coupon-access');
		if ($request->ajax()) {
			Coupon::whereId($request->id)->update(['trashed' => false]);
			return response()->json([
				'alert' => 'success',
				'title' => 'Hoàn tất',
				'msg' => 'Đã khôi phục 1 mục.'
			]);
		}
	}

	public function checkCodeExists(Request $request)
	{
		if ($request->ajax()) {
			$query = Coupon::whereCode($request->code);
			if ($request->has('id'))
				$query->where('id', '!=', $request->id);

			if ($query->get()->count() == 0)
				return response()->json(true);
		}
		return response()->json('Mục này đã tồn tại.');
	}

	public function export(Request $request)
	{
		
		$coupons  = CouponUser::with(['fk_coupon','fk_user','fk_order'])->get();
		$data = [];
		if(!empty($coupons)){
			foreach ($coupons as $key) {
				$data[] = [
					'customer_name'	=>	$key->fk_user->name,
					'customer_email'	=>	$key->fk_user->email,
					'customer_phone'	=>	$key->fk_user->phone,
					'promotion_code'	=>	$key->fk_coupon->code,
				];
			}
		}
	    return \Excel::download(new \App\Exports\CouponExport($data), 'MaKhuyenMai_'.date('U').'.xlsx');
	}
}
