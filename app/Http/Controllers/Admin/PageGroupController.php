<?php

namespace App\Http\Controllers\Admin;

use App\PageGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageGroupController extends Controller
{
    public function index()
    {
        $this->authorize('page-cat-access');
        $data = PageGroup::whereNull('parent_id')->get();
        return view('admin.page-group-list', compact('data'));
    }

    public function create()
    {
        $this->authorize('page-cat-access');
        $page_groups = PageGroup::whereNull('parent_id')->get();
        return view('admin.page-group-create', compact('page_groups'));
    }

    public function store(Request $request)
    {
        $this->authorize('page-cat-access');
        $data = new PageGroup;
        $data->name = $request->name;
        $data->slug = $request->slug;
        $data->parent_id = $request->parent;
        $data->save();

        return redirect()->route('admin.page.group.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã tạo mới nhóm trang.'
        ]);
    }

    public function edit($id)
    {
        $this->authorize('page-cat-access');
        $data = PageGroup::find($id);
        $page_groups = PageGroup::whereNull('parent_id')->where('id', '!=', $id)->get();
        return view('admin.page-group-edit', compact('data', 'page_groups'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('page-cat-access');
        $data = PageGroup::find($id);
        $data->name = $request->name;
        $data->slug = $request->slug;
        $data->parent_id = $request->parent;
        $data->save();
        
        return redirect()->route('admin.page.group.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã cập nhật thông tin nhóm trang.'
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('page-cat-access');
        if($request->ajax()){
            PageGroup::destroy($id);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }

    public function checkNameExists(Request $request)
    {
        if($request->ajax()) {
            $query = PageGroup::whereName($request->name);
            if($request->has('id'))
                $query->where('id', '!=', $request->id);

            if($query->get()->count() == 0) 
                return response()->json(true);
        }
        return response()->json('Mục này đã tồn tại.');
    }

    public function checkSlugExists(Request $request)
    {
        if($request->ajax()) {
            $query = PageGroup::whereSlug($request->slug);
            if($request->has('id'))
                $query->where('id', '!=', $request->id);

            if($query->get()->count() == 0) 
                return response()->json(true);
        }
        return response()->json('Mục này đã tồn tại.');
    }
}
