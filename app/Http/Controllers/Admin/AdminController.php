<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index()
    {
        $this->authorize('admin-access');
        $data = Admin::all();
        return view('admin.admin-list', compact('data'));
    }

    public function create()
    {
        $this->authorize('admin-access');
        $roles = $this->getLowerLevelRoles();
        return view('admin.admin-create', compact('roles'));
    }

    public function store(Request $request)
    {
        $this->authorize('admin-access');
        \Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'role' => 'required',
        ]);

        $data = new Admin;
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = \Hash::make($request->password);
        $data->role_id = $request->role;
        $data->save();

        return redirect()->route('admin.user.admin.index')->with([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg' => 'Đã tạo tài khoản người dùng.'
        ]);
    }

    public function edit($id)
    {
        $this->authorize('admin-access');
        $data = Admin::find($id);

        if(!$this->isLevelHigher($data))
            abort(403);

        $roles = $this->getLowerLevelRoles();

        return view('admin.admin-edit', compact('data', 'roles'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('admin-access');
        $data = Admin::find($id);

        if(!$this->isLevelHigher($data))
            abort(403);

        \Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'role' => 'required',
        ]);

        $data->name = $request->name;
        $data->email = $request->email;
        $data->role_id = $request->role;
        $data->save();

        return redirect()->route('admin.user.admin.index')->with([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg' => 'Đã cập nhật thông tin người dùng.'
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('admin-access');

        if(!$this->isLevelHigher(Admin::find($id)))
            abort(403);

        if($request->ajax()){
            if(Admin::destroy($id)){
                return response()->json([
                    'alert' => 'success',
                    'title' => 'Hoàn tất',
                    'msg'   => 'Đã xóa người dùng.'
                ]);
            }
        }
    }

    public function disable(Request $request)
    {
        $this->authorize('admin-access');

        if(!$this->isLevelHigher(Admin::find($request->id)))
            abort(403);

        if($request->ajax()){
            $data = Admin::find($request->id);
            $data->disabled = true;
            if($data->save()){
                return response()->json([
                    'alert' => 'success',
                    'title' => 'Hoàn tất',
                    'msg'   => 'Đã vô hiệu hóa người dùng.'
                ]);
            }
        }
    }

    public function activate(Request $request)
    {
        $this->authorize('admin-access');

        if(!$this->isLevelHigher(Admin::find($request->id)))
            abort(403);

        if($request->ajax()){
            $data = Admin::find($request->id);
            $data->disabled = false;
            if($data->save()){
                return response()->json([
                    'alert' => 'success',
                    'title' => 'Hoàn tất',
                    'msg'   => 'Đã mở khóa người dùng.'
                ]);
            }
        }
    }

    public function isLevelHigher($user)
    {
        if(!is_null($user->fk_role) && \Auth::guard('admin')->user()->fk_role->level >= $user->fk_role->level)
            return false;
        return true;
    }

    public function getLowerLevelRoles()
    {
        $level = \Auth::guard('admin')->user()->fk_role->level;        
        return Role::where('level', '>', $level)->get();
    }

    public function checkEmailExists(Request $request)
    {
        $query = Admin::where('email', $request->email);
        if($request->has('id'))
            $query->where('id', '!=', $request->id);
        if($query->get()->count() > 0)
            return response()->json('Email đã tồn tại.');
        return response()->json(true);
    }
}
