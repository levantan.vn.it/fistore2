<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('trash'))
            $data = Comment::where('trashed', 1);
        else
            $data = Comment::where('trashed', 0);
        $data = $data->orderBy('created_at', 'desc')->get();

        return view('admin.comment-list', compact('data'));
    }

    public function trash(Request $request)
    {
        $this->authorize('edit-comment');
        if($request->ajax()){
            Comment::whereId($request->id)->update(['trashed' => true]);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã chuyển 1 mục vào thùng rác.'
            ]);
        }
    }

    public function restore(Request $request)
    {
        $this->authorize('edit-comment');
        if($request->ajax()){
            Comment::whereId($request->id)->update(['trashed' => false]);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã khôi phục 1 mục.'
            ]);
        }
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('destroy-comment');
        if($request->ajax()){
            Comment::destroy($id);
            
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }
}
