<?php

namespace App\Http\Controllers\Admin;

use App\ArticleCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleCategoryController extends Controller
{
    public function index()
    {
        $this->authorize('article-cat-access');
        $data = ArticleCategory::whereNull('parent_id')->get();
        return view('admin.article-category-list', compact('data'));
    }

    public function create()
    {
        $this->authorize('article-cat-access');
        $categories = ArticleCategory::whereNull('parent_id')->get();
        return view('admin.article-category-create', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->authorize('article-cat-access');
        
        $data = new ArticleCategory;
        $data->name = $request->name;
        $data->slug = $request->slug;
        $data->parent_id = $request->parent;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->pinned = $request->has('pinned') ? true : false;
        $data->save();

        return redirect()->route('admin.article.category.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã tạo mới chuyên mục sản phẩm.'
        ]);
    }

    public function edit($id)
    {
        $this->authorize('article-cat-access');
        $data = ArticleCategory::find($id);
        $categories = ArticleCategory::whereNull('parent_id')->where('id', '!=', $id)->get();
        return view('admin.article-category-edit', compact('data', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('article-cat-access');
        
        $data = ArticleCategory::find($id);
        $data->name = $request->name;
        $data->slug = $request->slug;
        $data->parent_id = $request->parent;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->pinned = $request->has('pinned') ? true : false;
        $data->save();

        return redirect()->route('admin.article.category.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã cập nhật thông tin chuyên mục sản phẩm.'
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('article-cat-access');
        if($request->ajax()){
            ArticleCategory::destroy($id);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }

    public function checkNameExists(Request $request)
    {
        if($request->ajax()) {
            $query = ArticleCategory::whereName($request->name);
            if($request->has('id'))
                $query->where('id', '!=', $request->id);

            if($query->get()->count() == 0) 
                return response()->json(true);
        }
        return response()->json('Mục này đã tồn tại.');
    }

    public function checkSlugExists(Request $request)
    {
        if($request->ajax()) {
            $query = ArticleCategory::whereSlug($request->slug);
            if($request->has('id'))
                $query->where('id', '!=', $request->id);

            if($query->get()->count() == 0) 
                return response()->json(true);
        }
        return response()->json('Mục này đã tồn tại.');
    }
}
