<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TrialPackageFunction;

class PackageFunctionController extends Controller
{
    public function store(Request $request, $package_id)
    {
        $data = new TrialPackageFunction;
        $data->package_id = $package_id;
        $data->position = $request->position ?? 0;
        $data->name = $request->name;
        $data->save();

        return redirect()->route('admin.package.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã tạo mới tính năng gói.'
        ]);
    }

    public function update(Request $request, $package_id, $id)
    {
        //
    }

    public function destroy($package_id, $id)
    {
        //
    }
}
