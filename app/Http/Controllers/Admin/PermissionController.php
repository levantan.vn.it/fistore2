<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    public function edit($id)
    {
        $this->authorize('permission-access');
        $data = Role::find($id);

        // Kiểm tra cấp độ quyền
        // Nếu cấp quyền thấp hơn thì từ chối truy cập
        if(\Auth::guard('admin')->user()->fk_role->level >= $data->level)
            abort(403);

        $modules = Module::all();
        return view('admin.permission-edit', compact('data', 'modules'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('permission-access');
        $role = Role::find($id);

        // Kiểm tra cấp độ quyền
        // Nếu cấp quyền thấp hơn thì từ chối truy cập
        if(\Auth::guard('admin')->user()->fk_role->level >= $role->level)
            abort(403);

        $role->fk_permissions()->sync($request->allow);
        return back()->with([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg'   => 'Đã cập nhật quyền truy cập nhóm ' . $role->name . '.'
        ]);
    }
}
