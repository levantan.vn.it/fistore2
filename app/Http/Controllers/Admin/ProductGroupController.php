<?php

namespace App\Http\Controllers\Admin;

use App\ProductGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
class ProductGroupController extends Controller
{
    /**
     * Upload file path
     */
    protected $path = 'media/product-groups/';

    /**
     * Upload file size
     */
    protected $dimensions = [1024, 1024];
    public function index()
    {
        $this->authorize('product-group-access');
        $data = ProductGroup::whereNull('parent_id')->get();
        return view('admin.product-group-list', compact('data'));
    }

    public function create()
    {
        $this->authorize('product-group-access');
        $groups = ProductGroup::whereNull('parent_id')->get();
        return view('admin.product-group-create', compact('groups'));
    }

    public function store(Request $request)
    {
        $this->authorize('product-group-access');
        /** Upload Thumbnail */
        $thumbnail = null;
        if($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        }

        $data = new ProductGroup;
        $data->name = $request->name;
        $data->slug = $request->slug;
        $data->parent_id = $request->parent;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->pinned = $request->filled('pinned') ? true : false;
        $data->hidden = $request->filled('showed') ? false : true;
        $data->thumbnail = $thumbnail;
        $data->save();
        
        return redirect()->route('admin.product.group.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã tạo mới nhóm sản phẩm.'
        ]);
    }

    public function edit($id)
    {
        $this->authorize('product-group-access');
        $data = ProductGroup::find($id);
        $groups = ProductGroup::whereNull('parent_id')->where('id', '!=', $id)->get();
        return view('admin.product-group-edit', compact('data', 'groups'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('product-group-access');
        $data = ProductGroup::find($id);
        /** Upload Thumbnail */
        $thumbnail = $data->thumbnail;
        if($request->hasFile('thumbnail')) {
            /** Xóa thumbnail cũ */
            if(File::exists($this->path.$data->thumbnail)) File::delete($this->path.$data->thumbnail);
            /** Upload thumbnail mới */
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        }

        
        $data->name = $request->name;
        $data->slug = $request->slug;
        $data->parent_id = $request->parent;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->pinned = $request->filled('pinned') ? true : false;
        $data->hidden = $request->filled('showed') ? false : true;
        $data->thumbnail = $thumbnail;
        $data->save();
        
        return redirect()->route('admin.product.group.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã cập nhật thông tin nhóm sản phẩm.'
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('product-group-access');
        if($request->ajax()){
            ProductGroup::destroy($id);
            \DB::table('product_group_relationships')->where('group_id',$id)->delete();
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }

    public function checkNameExists(Request $request)
    {
        if($request->ajax()) {
            $query = ProductGroup::whereName($request->name);
            if($request->has('id'))
                $query->where('id', '!=', $request->id);

            if($query->get()->count() == 0) 
                return response()->json(true);
        }
        return response()->json('Mục này đã tồn tại.');
    }

    public function checkSlugExists(Request $request)
    {
        if($request->ajax()) {
            $query = ProductGroup::whereSlug($request->slug);
            if($request->has('id'))
                $query->where('id', '!=', $request->id);

            if($query->get()->count() == 0) 
                return response()->json(true);
        }
        return response()->json('Mục này đã tồn tại.');
    }
}
