<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\HotDeal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HotDealController extends Controller
{
    public function index()
    {
        $this->authorize('hotdeal-access');
        $data = HotDeal::orderBy('created_at', 'desc')->get();
        return view('admin.hotdeal-list', compact('data'));
    }

    public function create()
    {
        $this->authorize('hotdeal-access');
        $products = Product::where('is_combo', false)->where('trashed', false)->where('drafted', false)->where('closed', false)->where('display_price', true)->get();
        return view('admin.hotdeal-create', compact('products'));
    }

    public function store(Request $request)
    {
        $this->authorize('hotdeal-access');
        $product = Product::find($request->product);
        $data = new HotDeal;
        $data->product_id = $request->product;
        $data->price = str_replace(',', '', $request->price);
        $data->original_price = str_replace(',', '', $product->price);
        $data->available_qty = $request->quantity;
        $data->quantity = $request->quantity;
        $data->started_at = substr($request->started_at, 6, 4) . '-' . substr($request->started_at, 3, 2) . '-' . substr($request->started_at, 0, 2) . ' ' . str_replace(' ', '', $request->started_at_time);
        $data->expired_at = substr($request->expired_at, 6, 4) . '-' . substr($request->expired_at, 3, 2) . '-' . substr($request->expired_at, 0, 2) . ' ' . str_replace(' ', '', $request->expired_at_time);
        $data->disabled = $request->disabled ? true : false;
        $data->save();

        return redirect()->route('admin.hotdeal.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã thêm khuyến mãi mới.'
        ]);
    }

    public function edit($id)
    {
        $this->authorize('hotdeal-access');
        $products = Product::where('is_combo', false)->where('trashed', false)->where('drafted', false)->where('closed', false)->where('display_price', true)->get();
        $data = HotDeal::find($id);
        return view('admin.hotdeal-edit', compact('products', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('hotdeal-access');
        $data = HotDeal::find($id);
        $data->price = str_replace(',', '', $request->price);
        $data->available_qty = $request->available_qty;
        $data->quantity = $request->quantity;
        $data->started_at = substr($request->started_at, 6, 4) . '-' . substr($request->started_at, 3, 2) . '-' . substr($request->started_at, 0, 2) . ' ' . str_replace(' ', '', $request->started_at_time);
        $data->expired_at = substr($request->expired_at, 6, 4) . '-' . substr($request->expired_at, 3, 2) . '-' . substr($request->expired_at, 0, 2) . ' ' . str_replace(' ', '', $request->expired_at_time);
        $data->disabled = $request->disabled ? true : false;
        $data->save();

        return redirect()->route('admin.hotdeal.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã cập nhật khuyến mãi.'
        ]);
    }

    public function destroy($id)
    {
        $this->authorize('hotdeal-access');
        HotDeal::destroy($id);
        return response()->json([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg' => 'Đã xóa bỏ 1 mục.'
        ]);
    }
}
