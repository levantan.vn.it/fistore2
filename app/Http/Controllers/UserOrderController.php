<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\ProductOrder;
use App\ProductOrderDetail;
use Illuminate\Http\Request;

class UserOrderController extends Controller
{
	protected $status = array(
        0 => 'Chờ duyệt',
        1 => 'Đóng gói',
        2 => 'Đang vận chuyển',
        3 => 'Hoàn thành',
        4 => 'Không liên hệ được',
    );

    public function getOrder()
    {
    	$data = ProductOrder::where('user_id', Auth::guard('customer')->user()->id)->orderBy('created_at', 'desc')->get();

    	$status = $this->status;
    	return view('user_order', compact('data', 'status'));
    }

    public function cancelOrder(Request $request)
    {
    	$update = ProductOrder::find($request->id);
        $update->cancelled = true;
        $update->save();

        // Update điểm extra
        if(!is_null($update->user_id)) {
            $user = \App\User::find($update->user_id);
            $user->extra_point -= (number_format($update->total/get_setting('money_to_extra_point')->value,0,',',''));
            $user->save();
        }

        //  Cập nhật (hoàn) lại số lượng sản phẩm khi hủy đơn hàng
        DB::select('call product_order_delete (?)', array($update->id));

    	return back()->with([
    		'alert'	=> 'success',
    		'title'	=> 'Hoàn tất',
    		'message'	=> 'Đã hủy đơn hàng'
    	]);
    }

    public function orderDetail($code)
    {
        $order = ProductOrder::whereCode($code)->with('fk_details')->first();
        $status = $this->status;

        return view('user_order_detail', compact('order', 'status'));
    }
}
