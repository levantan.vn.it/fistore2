<?php

namespace App\Http\Middleware;

use Closure;

class IsCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Kiểm tra trạng thái đăng nhập bằng tài khoản quản trị
        if(\Auth::guard('customer')->check()) {
            return $next($request);
        }
        return redirect('/login');
    }
}
