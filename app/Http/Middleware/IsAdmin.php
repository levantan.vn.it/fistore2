<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Kiểm tra trạng thái đăng nhập bằng tài khoản quản trị
        if(\Auth::guard('admin')->check()) {
            // Kiểm tra tồn tại quyền quản trị hay không (Đối với tài khoản thuộc nhóm quyền đã bị xóa trước đó)
            if(isset(\Auth::guard('admin')->user()->fk_role))
                return $next($request);
            return abort(403);
        }
        return redirect('/admin/login');
    }
}
