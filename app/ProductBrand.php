<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBrand extends Model
{
    protected $table = 'product_brands';

    public function fk_childs()
    {
    	return $this->hasMany(ProductBrand::class, 'parent_id');
    }

    public function fk_products_pagination($take = null)
    {
    	return $this->hasMany(Product::class, 'brand_id')
    				->whereHidden(0)
    				->whereDrafted(0)
    				->whereTrashed(0)
    				->orderBy('reposted_at', 'desc')
    				->orderBy('created_at', 'desc')
    				->paginate($take);
    }
}
