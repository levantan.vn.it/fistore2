<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    public function fk_author()
    {
    	return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function fk_product()
    {
    	return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function fk_parent()
    {
    	return $this->hasOne(Comment::class, 'id', 'parent_id');
    }

    public function fk_reports()
    {
        return $this->hasMany(CommentReport::class, 'comment_id');
    }
}
