<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function fk_address_book()
    {
        return $this->hasMany(AddressBook::class, 'user_id');
    }

    public function fk_coupons()
    {
        return $this->belongsToMany(Coupon::class, 'coupon_user', 'coupon_id', 'user_id');
    }

    public function hasUsedCoupon(Coupon $coupon)
    {
        return !! optional($this->fk_coupons)->contains($coupon);
    }
}
