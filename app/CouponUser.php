<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponUser extends Model
{
    protected $table = 'coupon_user';

    public $timestamps = false;

    public function fk_coupon()
    {
    	return $this->belongsTo(Coupon::class, 'coupon_id');
    }

    public function fk_user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function fk_order()
    {
    	return $this->belongsTo(ProductOrder::class, 'order_id');
    }
}
