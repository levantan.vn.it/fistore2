<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    public function fk_author()
    {
    	return $this->hasOne(User::class, 'id', 'author_id');
    }

    public function fk_categories()
    {
    	return $this->belongsToMany(ArticleCategory::class, 'article_category_relationships', 'article_id', 'category_id');
    }
}
