<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponRelationship extends Model
{
    protected $table = 'coupon_relationships';

    public $timestamps = false;
}
