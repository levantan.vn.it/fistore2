<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreeTrial extends Model
{
    protected $table = 'free_trial';

    public function fk_product()
    {
    	return $this->hasOne(Product::class, 'id', 'product_id')->whereTrashed(false)->whereDrafted(false);
    }
}
