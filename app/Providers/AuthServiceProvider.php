<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Permission;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        if(Schema::hasTable('permissions')) {

            Gate::before(function($admin) {
                if($admin->isSuperAdmin()) 
                    return true; # Nếu là Quản trị cao cấp thì cho phép truy cập tất cả
            });

            $permissions = Permission::all();

            if(!empty($permissions)) {
                try{
                    foreach($permissions as $permission) {
                        Gate::define($permission->key, function($admin) use ($permission) {
                            return $admin->hasPermission($permission);
                        });
                    }
                } catch(\Exception $e) {
                    throw new \Exception("Cannot get user's permissions", 501);
                }
            }

        }
   }
}
