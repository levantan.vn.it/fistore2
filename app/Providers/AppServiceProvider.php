<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\ProductGroup;
use App\ProductCategory;
use App\ProductBrand;
use App\Setting;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \View::share('configs', get_config());
    
    	$listcategory = ProductCategory::where('parent_id',null)->get();
        foreach ($listcategory as $key => $value) {
            if(ProductCategory::where('parent_id',$value['id'])->get()){
                $listchild = ProductCategory::where('parent_id',$value['id'])->get();
                $listcategory[$key]['listchild'] = $listchild;
            }
        }

        $listbrand = ProductBrand::get();
        $listgroup = ProductGroup::all();
        $productBorder = Setting::where('key', 'product_border')->first();

        \View::share('listcategory', $listcategory);
        \View::share('listbrand', $listbrand);
        \View::share('listgroup', $listgroup);
        \View::share('productBorder', $productBorder);
        if($this->app->environment('production')) {
         \URL::forceScheme('https');
        }
        $this->app['request']->server->set('HTTPS','on');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
