<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessCode extends Model
{
	protected $primaryKey = 'id';
    protected $table = 'access_codes';
    protected $fillable = [
        'access_code'
    ];
    public $timestamps = true;
    
}
