<?php 
namespace App\Exports;

use App\ProductOrder;
use Maatwebsite\Excel\Concerns\FromCollection;

class Order implements FromCollection
{
    public function collection()
    {
        return ProductOrder::all();
    }
}