<?php 
namespace App\Exports;

use App\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CouponExport implements FromView
{
	protected $data = null;

	function __construct($data) {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('exports.coupon', [
            'data' => $this->data
        ]);
    }
}