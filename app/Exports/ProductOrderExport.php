<?php 
namespace App\Exports;

use App\ProductOrder;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProductOrderExport implements FromView
{
	protected $data = null;

	function __construct($data) {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('exports.product_order', [
            'data' => $this->data
        ]);
    }
}