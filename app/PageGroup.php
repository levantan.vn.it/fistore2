<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageGroup extends Model
{
    protected $table = 'page_groups';

    public function fk_childs()
    {
    	return $this->hasMany(PageGroup::class, 'parent_id');
    }
}
