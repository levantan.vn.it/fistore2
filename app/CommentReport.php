<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentReport extends Model
{
    protected $table = 'comment_reports';

    public function fk_user()
    {
    	return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function fk_comment()
    {
    	return $this->hasOne(Comment::class, 'id', 'comment_id');
    }
}
