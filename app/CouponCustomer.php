<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponCustomer extends Model
{
    protected $table = 'coupon_customer';

    public $timestamps = false;
}
