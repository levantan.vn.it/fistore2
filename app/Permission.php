<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';

    public function fk_roles()
    {
    	return $this->belongsToMany(Role::class, 'role_permission_relationships');
    }
}
