<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressBook extends Model
{
    protected $table = 'address_book';

    public function fk_user()
    {
    	return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function fk_province()
    {
    	return $this->hasOne(Province::class, 'id', 'province_id');
    }

    public function fk_district()
    {
    	return $this->hasOne(District::class, 'id', 'district_id');
    }

    public function fk_ward()
    {
    	return $this->hasOne(Ward::class, 'id', 'ward_id');
    }
}
