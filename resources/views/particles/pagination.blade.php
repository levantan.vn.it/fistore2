<ul class="pagination">
    @if($data->currentPage() != 1)
    <li class="page-item">
        <a class="page-link" href="{{$data->url(1)}}">
            <i class="fa fa-angle-double-left"></i>
        </a>
    </li>
    @endif

    @for($i = 1; $i <= $data->lastPage(); $i++)
        @if($i <= ($data->currentPage() + 2) && $i >= ($data->currentPage() - 2))
            @if($data->lastPage() > 1)
                <li class="page-item{{($data->currentPage() == $i) ? ' active' : null}}">
                    <a class="page-link" href="{{$data->url($i)}}">{{$i}}</a>
                </li>
            @endif
        @endif
    @endfor
    
    @if($data->currentPage() < $data->lastPage())
    <li class="page-item">
        <a class="page-link" href="{{$data->url($data->lastPage())}}">
            <i class="fa fa-angle-double-right"></i>
        </a>
    </li>
    @endif
</ul>