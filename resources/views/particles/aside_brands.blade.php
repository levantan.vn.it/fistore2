<aside class="aside-products mb-4 d-none d-md-block">
	<div class="aside-header">
		<div class="aside-title">Thương hiệu</div>
	</div>
	<div class="aside-body">
		<div class="form-group mb-2">
			<label class="custom-control custom-checkbox">
				<input type="radio" name="brand" value="" class="custom-control-input" onchange="this.form.submit()" form="mainForm"{!! !request()->filled('brand') ? ' checked' : null !!}>
				<span class="custom-control-indicator"></span>
				<span class="custom-control-description text-uppercase">Tất cả</span>
			</label>
		</div>
		@foreach($brands as $key => $item)
			@if($key == 7)
			<div class="collapse" id="collapseBrands">
			@endif
			<div class="form-group mb-2">
				<label class="custom-control custom-checkbox">
					<input type="radio" name="brand" value="{{ $item->id }}" class="custom-control-input" onchange="this.form.submit()" form="mainForm"{!! request()->brand == $item->id ? ' checked' : null !!}>
					<span class="custom-control-indicator"></span>
					<span class="custom-control-description text-uppercase">{{ $item->name }}</span>
				</label>
			</div>
			@if($key == 7)
			</div>
			@endif
		@endforeach
	</div>
	<div class="aside-footer text-center">
		<button class="btn btn-link" data-toggle="collapse" href="#collapseBrands" id="brandViewMore">Xem thêm <i class="fa fa-angle-down"></i></button>
	</div>
</aside>
<script>
	$("#collapseBrands").on("show.bs.collapse", function(){
		$("#brandViewMore").html('Thu gọn <i class="fa fa-angle-up"></i>');
	});
	$("#collapseBrands").on("hide.bs.collapse", function(){
		$("#brandViewMore").html('Xem thêm <i class="fa fa-angle-down"></i>');
	});
</script>
@if(request()->brand >= 8)
<script>
	$("#collapseBrands").collapse('show');
</script>
@endif