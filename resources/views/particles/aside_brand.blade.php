<aside class="aside-products mb-4">
	<div class="aside-header">
		<div class="aside-title">Thương hiệu</div>
	</div>
	<div class="aside-body">
		<div class="mb-3">
			<div class="form-group">
				<label class="custom-control custom-checkbox">
					<input type="radio" name="brand" value="" class="custom-control-input" onchange="this.form.submit()" form="mainForm"{!! !request()->filled('brand') ? ' checked' : null !!}>
					<span class="custom-control-indicator"></span>
					<span class="custom-control-description text-uppercase">Xem tất cả</span>
				</label>
			</div>
		</div>
		@foreach($brands as $item)
		<div class="mb-3">
			<div class="form-group">
				<label class="custom-control custom-checkbox">
					<input type="radio" name="brand" value="{{ $item->slug }}" class="custom-control-input" onchange="this.form.submit()" form="mainForm"{!! request()->brand == $item->slug ? ' checked' : null !!}>
					<span class="custom-control-indicator"></span>
					<span class="custom-control-description text-uppercase">{{ $item->name }}</span>
				</label>
			</div>
			@foreach($item->fk_childs as $child)
			<div class="form-group">
				&emsp;<label class="custom-control custom-checkbox">
					<input type="radio" name="brand" value="{{ $child->slug }}" class="custom-control-input" onchange="this.form.submit()" form="mainForm"{!! request()->brand == $child->slug ? ' checked' : null !!}>
					<span class="custom-control-indicator"></span>
					<span class="custom-control-description">{{ $child->name }}</span>
				</label>
			</div>
			@endforeach

		</div>
		@endforeach
	</div>
	{{-- <div class="aside-footer text-center">
		<button class="btn btn-link">Xem thêm <i class="fa fa-angle-down"></i></button>
	</div> --}}
</aside>