<aside class="aside-products mb-4 d-none d-md-block">
	<div class="aside-header">
		<div class="aside-title">Danh mục sản phẩm</div>
	</div>
	<div class="aside-body">

		<div class="mb-1 lh-2">
			<div class="text-uppercase collapsed" data-toggle="collapse" data-arrow="true" href="#categoryAsideHotShopa" style="cursor: pointer">Hot Shopa</div>
			<div class="collapse" id="categoryAsideHotShopa">
				@if(!empty(get_group(6)) && !get_group(6)->hidden)
				<div>
					<a href="{{ group_link(get_group(6)->slug) }}" class="custom-control custom-checkbox">
						<input type="radio" value="" class="custom-control-input" form="mainForm"{!! route('group.detail', get_group(6)->slug) == url()->current() ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">{{ get_group(6)->name }}</span>
					</a>
				</div>
				@endif
				<div>
					<a href="{{ route('combo') }}" class="custom-control custom-checkbox">
						<input type="radio" value="" class="custom-control-input" form="mainForm"{!! route('combo') == url()->current() ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">Combo sản phẩm</span>
					</a>
				</div>
				<div>
					<a href="{{ route('hotdeal') }}" class="custom-control custom-checkbox">
						<input type="radio" value="" class="custom-control-input" form="mainForm"{!! route('hotdeal') == url()->current() ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">Flash sale</span>
					</a>
				</div>
				@if(!empty(get_group(12)) && !get_group(12)->hidden)
				<div>
					<a href="{{ group_link(get_group(12)->slug) }}" class="custom-control custom-checkbox">
						<input type="radio" value="" class="custom-control-input" form="mainForm"{!! route('group.detail', get_group(12)->slug) == url()->current() ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">{{ get_group(12)->name }}</span>
					</a>
				</div>
				@endif
				@if(!empty(get_group(9)) && !get_group(9)->hidden)
				<div>
					<a href="{{ group_link(get_group(9)->slug) }}" class="custom-control custom-checkbox">
						<input type="radio" value="" class="custom-control-input" form="mainForm"{!! route('group.detail', get_group(9)->slug) == url()->current() ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">{{ get_group(9)->name }}</span>
					</a>
				</div>
				@endif
				@if(!empty(get_group(8)) && !get_group(8)->hidden)
				<div>
					<a href="{{ group_link(get_group(8)->slug) }}" class="custom-control custom-checkbox">
						<input type="radio" value="" class="custom-control-input" form="mainForm"{!! route('group.detail', get_group(8)->slug) == url()->current() ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">{{ get_group(8)->name }}</span>
					</a>
				</div>
				@endif
				<div>
					<a href="{{ route('freetrial') }}" class="custom-control custom-checkbox">
						<input type="radio" value="" class="custom-control-input" form="mainForm"{!! route('freetrial') == url()->current() ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">Trải nghiệm 0 đồng</span>
					</a>
				</div>
			</div>
		</div>

		<div class="mb-1 lh-2">
			<div class="text-uppercase collapsed" data-toggle="collapse" data-arrow="true" href="#categoryAsideTrangDiem" style="cursor: pointer">{{ get_category(11)->name }}</div>
			<div class="collapse" id="categoryAsideTrangDiem">
				@foreach(get_category(11)->fk_childs as $key => $item)
				<div>
					<a href="{{ category_link($item->slug) }}" class="custom-control custom-checkbox">
						<input type="radio" value="" class="custom-control-input" form="mainForm"{!! route('category.detail', $item->slug) == url()->current() ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">{{ $item->name }}</span>
					</a>
				</div>
				@endforeach
			</div>
		</div>

		<div class="mb-1 lh-2">
			<div class="text-uppercase collapsed" data-toggle="collapse" data-arrow="true" href="#categoryAsideChamSocDa" style="cursor: pointer">{{ get_category(8)->name }}</div>
			<div class="collapse" id="categoryAsideChamSocDa">
				@foreach(get_category(8)->fk_childs as $key => $item)
				<div>
					<a href="{{ category_link($item->slug) }}" class="custom-control custom-checkbox">
						<input type="radio" value="" class="custom-control-input" form="mainForm"{!! route('category.detail', $item->slug) == url()->current() ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">{{ $item->name }}</span>
					</a>
				</div>
				@endforeach
			</div>
		</div>

		<div class="mb-1 lh-2">
			<div class="text-uppercase collapsed" data-toggle="collapse" data-arrow="true" href="#categoryAsideMatNa" style="cursor: pointer">{{ get_category(9)->name }}</div>
			<div class="collapse" id="categoryAsideMatNa">
				<div>
					<a href="{{ category_link(get_category(9)->slug) }}" class="custom-control custom-checkbox">
						<input type="radio" value="" class="custom-control-input" form="mainForm"{!! route('category.detail', get_category(9)->slug) == url()->current() ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">Tất cả</span>
					</a>
				</div>
			</div>
		</div>
		
		<div class="mb-1 lh-2">
			<div class="text-uppercase collapsed" data-toggle="collapse" data-arrow="true" href="#categoryAsideSanPhamKhac" style="cursor: pointer">{{ get_category(10)->name }}</div>
			<div class="collapse" id="categoryAsideSanPhamKhac">
				@foreach(get_category(10)->fk_childs as $key => $item)
				<div>
					<a href="{{ category_link($item->slug) }}" class="custom-control custom-checkbox">
						<input type="radio" value="" class="custom-control-input" form="mainForm"{!! route('category.detail', $item->slug) == url()->current() ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">{{ $item->name }}</span>
					</a>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</aside>
<script>
	$(document).ready(function(){
		$("input[type=radio]:checked").parents(".collapse").addClass("show");
	});
</script>