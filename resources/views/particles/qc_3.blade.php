<div class="block-qcs my-2 my-md-4">
	<div class="owl-carousel sub-banner-slider">
		@foreach(get_slide(5)->fk_images as $item)
		<div class="item">
			<a href="{{ $item->target_url ?? '#' }}">
				<img src="{{ media_url('slides', $item->path) }}" alt="{{ $item->name }}" width="100%">
			</a>
		</div>
		@endforeach
	</div>
</div>