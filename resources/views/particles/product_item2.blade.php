<div class="item-product">
	@if(!$productBorder->disabled)
	<img src="{{ media_url('popups', $productBorder->value) }}" class="khungsp" class="khungsp">
	@endif
	
	<div class="item-thumbnail">
		<a href="{{ product_link($item->slug) }}">
			<img src="{{ media_url('products', $item->thumbnail) }}" alt="{{ $item->title ?? $item->name }}" title="{{ $item->title ?? $item->name }}">
		</a>
		@if($item->price > 0)
		@if(isset($item->original_price) && ($item->original_price > $item->price))
		<label class="saleof">- {{ round(($item->original_price-$item->price)/$item->original_price*100) }}%</label>
		@endif
		@endif
	</div>
	<div class="item-info">
		<div class="item-brand-name">
			<a href="{{ brand_link(optional($item->fk_brand)->slug) }}">{{ str_length_limit(optional($item->fk_brand)->name, 13) }}</a>
		</div>
		<div class="item-name">
			<a href="{{ product_link($item->slug) }}">{{ str_length_limit($item->name, 17) }}</a>
		</div>
		<div class="item-price">
			@if($item->price > 0)
			{{ number_format($item->price,0,',','.') }} đ
			@if(isset($item->original_price) && ($item->original_price > $item->price))
			<strike class="fz-down-1 text-muted">{{ number_format($item->original_price,0,',','.') }} đ</strike>
			@endif
			@else
			<span class="text-muted font-weight-normal">Liên hệ</span>
			@endif
		</div>
		<div class="item-action">
			@if($item->price > 0)
			@if($item->closed)
			<a href="{{ product_link($item->slug) }}" class="btn btn-primary btn-block btn-soldout">HẾT HÀNG</a>
			@else
			<a href="{{ product_link($item->slug) }}" class="btn btn-primary btn-block">XEM NGAY</a>
			@endif
			@else
			@if($item->closed)
			<a href="{{ product_link($item->slug) }}" class="btn btn-danger btn-block btn-soldout">HẾT HÀNG</a>
			@else
			<a href="{{ product_link($item->slug) }}" class="btn btn-danger btn-block fz-down-1">LIÊN HỆ</a>
			@endif
			@endif
		</div>
	</div>
</div>
