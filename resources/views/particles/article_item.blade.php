<div class="item-article">
  <div class="item-thumbnail">
    <a href="{{ article_link($item->slug) }}">
      <img src="{{ media_url('articles', $item->thumbnail) }}" alt="{{ $item->name }}" title="{{ $item->name }}">
    </a>
  </div>
  <div class="item-name">
    <a href="{{ article_link($item->slug) }}">{{ str_length_limit($item->name, 30) }}</a>
  </div>
</div>
