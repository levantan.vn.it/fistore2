<div class="item-product hot-deal">
  <div class="item-thumbnail">
    <a href="{{ $item->target_url }}">
      <img src="{{ media_url('products', $item->fk_product->thumbnail) }}" alt="{{ $item->fk_product->title ?? $item->fk_product->name }}" title="{{ $item->fk_product->title ?? $item->fk_product->name }}">
    </a>
  </div>
  <div class="item-info">
    <div class="item-brand-name">
      <a href="{{ brand_link_by_product($item->fk_product) }}">{{ str_length_limit(optional($item->fk_product->fk_brand)->name, 13) }}</a>
    </div>
    <div class="item-name">
      <a href="{{ $item->target_url }}">{{ str_length_limit($item->fk_product->name, 17) }}</a>
    </div>
    {{-- <div class="item-price countdown" data-countdown="{{ date('Y/m/d H:i:s', strtotime($item->expired_at)) }}" onload="countdown(event)">Loading...</div> --}}
    <div class="item-action">
      <a href="{{ $item->target_url }}" target="_blank" class="btn btn-primary btn-block">TRẢI NGHIỆM</a>
    </div>
  </div>
</div>
