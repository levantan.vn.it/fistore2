<div class="block-brands my-4">
  <div class="owl-carousel" id="brandSlider">
    @foreach(get_slide(2)->fk_images as $item)
      <div class="item">
        <a href="{{ $item->target_url }}">
          <img src="{{ media_url('slides', $item->path) }}" alt="{{ $item->name }}">
        </a>
      </div>
    @endforeach
  </div>
  <script>
    $('#brandSlider').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        dots:false,
        autoplay:true,
        autoplayTimeout:3000,
        smartSpeed:700,
        responsive:{
          0:{
            items:4
          },
          768:{
            items:7
          },
          992:{
            items:9
          },
          1200:{
            items:10
          },
        },
    })
  </script>
</div>
