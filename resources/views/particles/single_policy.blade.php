<div class="block-single-policy mb-4">
	<div class="block-single-policy-title">DUY NHẤT TẠI SHOPA</div>
	<div class="block-single-policy-body">
		<a class="block-single-policy-item" href="{{ url('gioi-thieu') }}" style="color: #676767">
			<div>
				<img src="{{ asset('images/ico/single-policy-1.png') }}">
			</div>
			<span>Chính chủ Hàn Quốc.</span>
		</a>
		<a class="block-single-policy-item" href="{{ url('gioi-thieu') }}" style="color: #676767">
			<div>
				<img src="{{ asset('images/ico/single-policy-2.jpg') }}">
			</div>
			<span>Mức giá cạnh tranh.</span>
		</a>
		<a class="block-single-policy-item" href="{{ url('gioi-thieu') }}" style="color: #676767">
			<div>
				<img src="{{ asset('images/ico/single-policy-3.png') }}">
			</div>
			<span>Đối tác chính thức của 3CE, Black Rouge, I’m Meme</span>
		</a>
		{{-- <a class="block-single-policy-item" href="{{ url('chinh-sach-van-chuyen-giao-nhan') }}" style="color: #676767">
			<div>
				<img src="{{ asset('images/ico/single-policy-4.png') }}">
			</div>
			<span>Freeship từ 399k.</span>
		</a> --}}
	</div>
</div>