<aside class="aside-cart">
	<div class="aside-header">Hiện có <span class="data-cart-count fz-up-2">{{ Cart::count() }}</span> sản phẩm</div>
	<div class="aside-body">
		<table width="100%" cellpadding="10">
			<tbody>
				<tr>
					<td>Tạm tính:</td>
					<td align="right"><span class="data-cart-subtotal">{{ Cart::subtotal(0,',','.') }}</span>đ</td>
				</tr>

				@if(isset($shippingFee))
				<tr style="border-bottom: 1px solid #dedede">
					<td>Phí vận chuyển:</td>
					<td align="right"><span>{{ number_format($shippingFee,0,',','.') }}</span>đ</td>
				</tr>
				@endif

				@if(session()->has('coupon'))
					@switch(session('coupon')->type)
					 {{-- Khi loại giảm giá là giảm bình thường --}}
						@case(1)
							<tr>
								<td>Giảm giá:</td>
								<td align="right"><span
										class="data-cart-discount">-{{ number_format(session('coupon')->discount,0,',','.') }}</span>đ
								</td>
							</tr>
							<?php $discount_price = session('coupon')->discount; ?>							
							@break
						{{-- Khi loại giảm giá là giảm giá ship --}}
						@case(2)
							{{-- Nếu có phí ship => lấy min giá trị có thể giảm giá --}}
							@if (isset($shippingFee)) 
								<tr>
									<td>Giảm giá phí vận chuyển:</td>
									<td align="right">
										<span class="data-cart-discount">
											<?php $discount_price = session('coupon')->discount;
												if (session('coupon')->unit == '%') {
													$discount_price = $shippingFee * session('coupon')->discount / 100;
												}
												
												$discount_price = $shippingFee < $discount_price ? $shippingFee : $discount_price;
											 ?>
											-{{ number_format($discount_price ,0,',','.') }}</span>đ
									</td>
								</tr>
							@else
								<tr>
									<td>Giảm giá phí vận chuyển:</td>
									<td align="right">
										<span class="data-cart-discount">
											-{{ number_format(session('coupon')->discount ,0,',','.') }}</span>{{ session('coupon')->unit }}
									</td>
								</tr>
							@endif
							@break
						@default
							
					@endswitch
				@endif
				<tr>
					<td>Thành tiền:</td>
					@php
						$total = Cart::subtotal(0,',','') - ($discount_price ?? 0) + ($shippingFee ?? 0);
			            if($total <= 0)
			                $total = 0;
					@endphp
					<td align="right">
						<strong class="strong-text">
							<span class="data-cart-total">
								{{ number_format($total,0,',','.') }}</span>đ</strong><br />
					</td>
				</tr>
				{{-- <tr>
                    <td colspan="2" align="center" class="pt-0">
                        <span style="text-transform:uppercase;color:red;">(Chưa bao gồm phí vận chuyển)</span>
                    </td>
                </tr> --}}
			</tbody>
		</table>
	</div>
</aside>