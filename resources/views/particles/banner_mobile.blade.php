<div class="block-banner mb-4">
  <div class="owl-carousel" id="bannerSlider">
    @foreach(get_slide(6)->fk_images as $item)
      <div class="item">
        <a href="{{ $item->target_url ?? '#' }}">
          <img src="{{ media_url('slides', $item->path) }}" alt="{{ $item->name }}" width="100%">
        </a>
      </div>
    @endforeach
  </div>
  <script>
    $('#bannerSlider').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        navText:['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        dots:true,
        items:1,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        autoplaySpeed:1000,
        animateOut:'fadeOut',
        smartSpeed:1000,
        fluidSpeed:1000,
    })
  </script>
</div>
