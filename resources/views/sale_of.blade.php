@extends('layouts.master')

@section('title', 'Khuyến mãi')
@section('description', null)
@section('keywords', null)

@section('header')
	{{-- Owl Carousel --}}
	<script src="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.css') }}">
@stop

@section('main')
	{!! Breadcrumbs::render('hotdeal') !!}

	<div class="row">
		<div class="col-md-one-fifth">
			@include('particles.aside_menu_href')
			@include('particles.aside_brands')
		</div>
		<div class="col-md-four-fifth">
			<div class="block-products">
				<div class="block-header">
					<div class="block-title">Sản phẩm khuyến mãi: <strong>{{ $data->total() }}</strong> kết quả</div>
					@if(request()->route()->getName() == 'hotdeal')
					<form class="block-description d-block" method="GET" id="mainForm">
						<a href="{{ route('hotdeal') }}" class="sort{{ !request()->filled('status') || request()->status == 'selling' ? ' active' : '' }}">ĐANG BÁN</a>
						<a href="{{ route('hotdeal') }}?status=ats" class="sort{{ request()->status == 'ats' ? ' active' : '' }}">SẮP BÁN</a>
						<a href="{{ route('hotdeal') }}?status=expired" class="sort{{ request()->status == 'expired' ? ' active' : '' }}">HẾT HẠN</a>
					</form>
					@else
					<form class="block-description d-block" method="GET" id="mainForm">
						{{-- <a class="sort{!! request()->route()->getName() == 'hotdeal' ? ' active' : null !!}" href="{{ route('hotdeal') }}">HOT DEAL</a> --}}
						<a class="sort{!! request()->route()->getName() == 'freetrial' ? ' active' : null !!}" href="{{ route('freetrial') }}">TRẢI NGHIỆM 0 ĐỒNG</a>
						<a class="sort{!! request()->route()->getName() == 'combo' ? ' active' : null !!}" href="{{ route('combo') }}">COMBO</a>
					</form>
					@endif
				</div>
				<div class="block-body">
					<div class="row row-mx-10">
						@foreach($data as $item)
							<div class="col-6 col-md-4 col-lg-3 mb-4">
								@if(request()->route()->getName() == 'hotdeal')

										@include('particles.hotdeal_item')

								@elseif(request()->route()->getName() == 'freetrial')

										@include('particles.freetrial_item')

								@elseif(request()->route()->getName() == 'combo')

										@include('particles.combo_item')
										
								@endif
							</div>
						@endforeach
					</div>
				</div>
				<div class="block-footer">
					@include('particles.pagination')
				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
@stop