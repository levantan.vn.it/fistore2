@extends('layouts.master')

@section('title', 'Thương hiệu')
@section('description', null)
@section('keywords', null)

@section('header')
	{{-- Owl Carousel --}}
	<script src="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.css') }}">
@stop

@section('main')
	{!! Breadcrumbs::render('brand.list') !!}

	<div class="block-brands">
		<div class="block-header mb-5">
			<div class="mb-3">Xem 200 thương hiệu</div>
			<div class="block-sort">
				<label>
				<input type="radio" value="0-9" name="sort" checked>
				<span>0-9</span>
			</label>
			<label>
				<input type="radio" value="A" name="sort">
				<span>A</span>
			</label>
			<label>
				<input type="radio" value="B" name="sort">
				<span>B</span>
			</label>
			<label>
				<input type="radio" value="C" name="sort">
				<span>C</span>
			</label>
			<label>
				<input type="radio" value="D" name="sort">
				<span>D</span>
			</label>
			<label>
				<input type="radio" value="E" name="sort">
				<span>E</span>
			</label>
			<label>
				<input type="radio" value="F" name="sort">
				<span>F</span>
			</label>
			<label>
				<input type="radio" value="G" name="sort">
				<span>G</span>
			</label>
			<label>
				<input type="radio" value="H" name="sort">
				<span>H</span>
			</label>
			<label>
				<input type="radio" value="I" name="sort">
				<span>I</span>
			</label>
			<label>
				<input type="radio" value="J" name="sort">
				<span>J</span>
			</label>
			<label>
				<input type="radio" value="K" name="sort">
				<span>K</span>
			</label>
			<label>
				<input type="radio" value="L" name="sort">
				<span>L</span>
			</label>
			<label>
				<input type="radio" value="M" name="sort">
				<span>M</span>
			</label>
			<label>
				<input type="radio" value="N" name="sort">
				<span>N</span>
			</label>
			<label>
				<input type="radio" value="O" name="sort">
				<span>O</span>
			</label>
			<label>
				<input type="radio" value="P" name="sort">
				<span>P</span>
			</label>
			<label>
				<input type="radio" value="Q" name="sort">
				<span>Q</span>
			</label>
			<label>
				<input type="radio" value="R" name="sort">
				<span>R</span>
			</label>
			<label>
				<input type="radio" value="S" name="sort">
				<span>S</span>
			</label>
			<label>
				<input type="radio" value="T" name="sort">
				<span>T</span>
			</label>
			<label>
				<input type="radio" value="U" name="sort">
				<span>U</span>
			</label>
			<label>
				<input type="radio" value="V" name="sort">
				<span>V</span>
			</label>
			<label>
				<input type="radio" value="W" name="sort">
				<span>W</span>
			</label>
			<label>
				<input type="radio" value="X" name="sort">
				<span>X</span>
			</label>
			<label>
				<input type="radio" value="Y" name="sort">
				<span>Y</span>
			</label>
			<label>
				<input type="radio" value="Z" name="sort">
				<span>Z</span>
			</label>
			</div>
		</div>
		<div class="block-body">
			<div class="block-item mb-5" id="sort0-9">
				<div class="block-item-title"><hr><span>0-9</span></div>
				<div class="row row-mx-10">
					@foreach($data as $item)

						@if(is_numeric(substr($item->name, 0, 1)))
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach
				</div>
			</div>
			<div class="block-item mb-5" id="sortA">
				<div class="block-item-title"><hr><span>A</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'A')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortB">
				<div class="block-item-title"><hr><span>B</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'B')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortC">
				<div class="block-item-title"><hr><span>C</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'C')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortD">
				<div class="block-item-title"><hr><span>D</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'D')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortE">
				<div class="block-item-title"><hr><span>E</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'E')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortF">
				<div class="block-item-title"><hr><span>F</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'F')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortG">
				<div class="block-item-title"><hr><span>G</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'G')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortH">
				<div class="block-item-title"><hr><span>H</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'H')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortI">
				<div class="block-item-title"><hr><span>I</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'I')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortJ">
				<div class="block-item-title"><hr><span>J</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'J')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortK">
				<div class="block-item-title"><hr><span>K</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'K')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortL">
				<div class="block-item-title"><hr><span>L</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'L')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortM">
				<div class="block-item-title"><hr><span>M</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'M')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortN">
				<div class="block-item-title"><hr><span>N</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'N')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortO">
				<div class="block-item-title"><hr><span>O</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'O')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortP">
				<div class="block-item-title"><hr><span>P</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'P')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortQ">
				<div class="block-item-title"><hr><span>Q</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'Q')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortR">
				<div class="block-item-title"><hr><span>R</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'R')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortS">
				<div class="block-item-title"><hr><span>S</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'S')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortT">
				<div class="block-item-title"><hr><span>T</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'T')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortU">
				<div class="block-item-title"><hr><span>U</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'U')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortV">
				<div class="block-item-title"><hr><span>V</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'V')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortW">
				<div class="block-item-title"><hr><span>W</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'W')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortX">
				<div class="block-item-title"><hr><span>X</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'X')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortY">
				<div class="block-item-title"><hr><span>Y</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'Y')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>
			<div class="block-item mb-5" id="sortZ">
				<div class="block-item-title"><hr><span>Z</span></div>
				<div class="row row-mx-10">

					@foreach($data as $item)
					
						@if(ucwords(substr($item->name, 0, 1)) == 'Z')
							<div class="col-6 col-md-3 col-lg-2 mb-4">
								@include('particles.brand_item')
							</div>
						@endif

					@endforeach

				</div>
			</div>


		</div>
	</div>
@stop

@section('footer')
	<script>
		$("input[name=sort]").change(function(){
			$("html, body").animate({ scrollTop: $('#sort' + $(this).val()).offset().top }, "slow");
		});
		$(document).ready(function(){
			for (var i = 65; i <= 90; i++) {
			    if($('#sort' + String.fromCharCode(i)).find(".row div").length == 0)
			    	$('#sort' + String.fromCharCode(i)).hide();
			}
		});
	</script>
@stop