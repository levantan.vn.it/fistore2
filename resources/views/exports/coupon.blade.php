<table>
	<thead>
		<tr>
			<th>Customer Name</th>
			<th>Customer Email</th>
			<th>Customer Phone</th>
			<th>Promotion Code</th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $row)
			<tr>
				<td>{{ $row['customer_name'] }}</td>
				<td>{{ $row['customer_email'] }}</td>
				<td>{{ $row['customer_phone'] }}</td>
				<td>{{ $row['promotion_code'] }}</td>
			</tr>
		@endforeach
	</tbody>
</table>