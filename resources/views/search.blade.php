@extends('layouts.master')

@section('title', 'Kết quả tìm kiếm ' . (request()->keywords ?? request()->tag))
@section('description', null)
@section('keywords', null)

@section('header')
@stop

@section('main')
	{!! Breadcrumbs::render('search') !!}

	<div class="row">
		<div class="col-md-one-fifth d-none d-md-block">
			@include('particles.aside_menu_href')
			@include('particles.aside_brand')
		</div>
		<div class="col-md-four-fifth">
			<div class="block-products">
				<div class="block-header">
					<div class="block-title">{{ (request()->keywords ?? request()->tag) ?? 'Tất cả' }}: <strong>{{ $data->total() }}</strong> kết quả</div>
					<form class="block-description" method="GET" id="mainForm">
						Ưu tiên xem:
						<label>
							<input type="radio" value="newest" name="sort" onchange="this.form.submit()"{!! !request()->filled('sort') || request()->sort == 'newest' ? ' checked' : null !!}>
							<span>Hàng mới</span>
						</label>
						<label>
							<input type="radio" value="good_sale" name="sort" onchange="this.form.submit()"{!! request()->filled('sort') && request()->sort == 'good_sale' ? ' checked' : null !!}>
							<span>Bán chạy</span>
						</label>
						<label>
							<input type="radio" value="min_cost" name="sort" onchange="this.form.submit()"{!! request()->filled('sort') && request()->sort == 'min_cost' ? ' checked' : null !!}>
							<span>Giá thấp</span>
						</label>
						<label>
							<input type="radio" value="max_cost" name="sort" onchange="this.form.submit()"{!! request()->filled('sort') && request()->sort == 'max_cost' ? ' checked' : null !!}>
							<span>Giá cao</span>
						</label>
					</form>
				</div>
				<div class="block-body">
					<div class="row row-mx-10">

					@foreach($data as $item)
						<div class="col-6 col-md-4 col-lg-3 mb-4">
							@include('particles.product_item2')
						</div>
					@endforeach

					</div>
				</div>
				<div class="block-footer">
					{{ $data->appends(request()->except('page'))->links() }}

					
				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
	@include('api.mtag.searched_product')
@stop