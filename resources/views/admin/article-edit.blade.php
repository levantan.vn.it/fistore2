@extends('admin.layouts.master')

{{-- Title --}}
@section('title', $data->name)

{{-- Import CSS, JS --}}
@section('header')
	{{-- Jquery Validation --}}
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>

	{{-- CKEditor --}}
	<script src="{{ asset('libs/ckeditor/4.10.0/ckeditor.js') }}"></script>

	{{-- Selectize --}}
	<script type="text/javascript" src="{{ asset('libs/selectize/0.12.4/js/standalone/selectize.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/selectize/0.12.4/css/selectize.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.article.edit', $data))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Cập nhật</button>
	<button class="btn btn-primary" name="reposted" value="1" form="mainForm"><i class="fa fa-level-up fa-fw"></i> Đăng lên trang đầu</button>
	<button class="btn btn-info" name="draft" value="1" form="mainForm"><i class="fa fa-clipboard fa-fw"></i> Lưu bản nháp</button>
	<a href="{{ route('admin.article.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Hủy bỏ</a>
@stop

{{-- Main --}}
@section('main')
	<form action="{{ route('admin.article.update', $data->id) }}" method="POST" enctype="multipart/form-data" id="mainForm">
		@method('PUT')
		@csrf
		<div class="row">
			<div class="col-lg-8">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="name">Tên bài viết <span class="required">*</span></label>
							<input type="text" name="name" class="form-control" value="{{ $data->name }}" onkeyup="slug_make();" required>
						</div>
						<div class="form-group">
							<label for="content">Nội dung</label>
							<textarea name="content" id="content">{!! $data->content !!}</textarea>
							<script>
								CKEDITOR.replace('content');
							</script>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="float-right">
							<button type="button" class="btn btn-link" data-toggle="collapse" href="#seoOption">Tùy chỉnh nội dung SEO</button>
						</div>
						<label>Xem trước kết quả tìm kiếm</label>

						<div class="seo-container">
							<p class="seo-title">{{ $data->title ?? $data->name }}</p>
							<p class="seo-url">{{ url('/bai-viet/'.$data->slug.'.html') }}</p>
							<p class="seo-description">{{ $data->description ?? 'Trích đoạn từ nội dung bài viết một cách ngắn gọn...' }}</p>

							<div class="collapse" id="seoOption">
								<div class="card">
									<hr/>
									<div class="form-group">
										<label>Tiêu đề</label>
										<input type="text" name="title" class="form-control" value="{{ $data->title }}" onkeyup="title_make();">
									</div>

									<div class="form-group">
										<label>Mô tả</label>
										<textarea rows="3" name="description" class="form-control" onkeyup="description_make();">{!! $data->description !!}</textarea>
									</div>

									<div class="form-group">
										<label>Đường dẫn <span class="required">*</span></label>
										<div class="input-group">
											<span class="input-group-addon addon-info">{{ url('') }}</span>
											<input type="text" name="slug" class="form-control" value="{{ $data->slug }}" placeholder="ten-bai-viet" required>
											<span class="input-group-btn">
												<button class="btn btn-info slug-refresh" type="button" onclick="slug_make();"><i class="fa fa-refresh"></i></button>
											</span>
										</div>
										<label id="slug-error" class="error" for="slug" style="display: none;"></label>
									</div>
									<script>
										$(".slug-refresh").click(function() {
											var event = $(this);
											$(this).find("i").addClass("fa-spin");
											setTimeout(function() {
												$(event).find("i").removeClass("fa-spin");
											}, 1000);
										});
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg 4">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<div class="flex-row">
								<span>Ngày đăng</span>
								<b>{{ date('d/m/Y H:s', strtotime($data->created_at)) }}</b>
							</div>
						</div>
						<div class="form-group">
							<div class="flex-row">
								<span>Chỉnh sửa lần cuối</span>
								<b>{{ date('d/m/Y H:s', strtotime($data->updated_at)) }}</b>
							</div>
						</div>
						<div class="form-group">
							<div class="flex-row">
								<span>Đăng bởi</span>
								<b>{{ optional($data->fk_author)->name }}</b>
							</div>
						</div>
						<div class="form-group">
							<div class="flex-row">
								<label for="show">Hiển thị công khai</label>
								<label class="custom-toggle toggle-info mb-0">
									<input type="checkbox" name="show" class="toggle-checkbox"{!! $data->hidden ? null : ' checked' !!}>
									<div class="toggle-inner mr-0">
										<span class="toggle-button"></span>
									</div>
								</label>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Ảnh hiển thị bài viết</label>
							<div class="custom-image-upload custom-iu-md image-preview">
								<img class="img-preview" src="{{ media_url('articles', $data->thumbnail) }}">
								<label class="custom-label">
									<input type="file" name="thumbnail" onchange="upload_img_preview(event);" accept="image/*">
								</label>
								<svg id="i-close" onclick="remove_file(event);" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
									<path d="M2 30 L30 2 M30 30 L2 2"></path>
								</svg>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="group">Chuyên mục</label>
							<div class="card px-4 py-3" style="background: #F8F8F8">
								<label class="custom-control custom-checkbox">
									<input type="checkbox" name="featured" class="custom-control-input"{!! $data->featured ? ' checked' : null !!}>
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description">Bài viết nổi bật</span>
								</label>

								@foreach($categories as $category)
									<div class="form-group">
										<label class="custom-control custom-checkbox">
											<input type="checkbox" name="categories[]" value="{{ $category->id }}" class="custom-control-input"{!! $data->fk_categories->contains($category->id) ? ' checked' : null !!}>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">{{ $category->name }}</span>
										</label>
									</div>
									@foreach($category->fk_childs as $child)
										<div class="form-group">
											<label class="custom-control custom-checkbox">
												<input type="checkbox" name="categories[]" value="{{ $child->id }}" class="custom-control-input"{!! $data->fk_categories->contains($child->id) ? ' checked' : null !!}>
												<span class="custom-control-indicator"></span>
												<span class="custom-control-description">— {{ $child->name }}</span>
											</label>
										</div>
										@foreach($child->fk_childs as $child_2)
											<div class="form-group">
												<label class="custom-control custom-checkbox">
													<input type="checkbox" name="categories[]" value="{{ $child_2->id }}" class="custom-control-input"{!! $data->fk_categories->contains($child_2->id) ? ' checked' : null !!}>
													<span class="custom-control-indicator"></span>
													<span class="custom-control-description">—— {{ $child_2->name }}</span>
												</label>
											</div>
										@endforeach
									@endforeach
								@endforeach
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Thêm từ khóa liên quan (Tags)</label>
							<div class="control-group">
								<select id="tags" name="tags[]" multiple placeholder="Chọn trong danh sách hoặc Enter để thêm từ khóa">
									@foreach($tags as $tag)
										<option value="{{ $tag->keyword }}"{!! strpos($data->tags, $tag->keyword) !== false ? ' selected' : null !!}>{{ $tag->keyword }}</option>
									@endforeach
								</select>
							</div>
							<script>
								$('#tags').selectize({
									plugins: ['remove_button'],
									delimiter: ',',
									persist: false,
									createOnBlur: true,
									create: true
								});
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navArticle").addClass("active").find("ul").addClass("show").find(".article").addClass("active");

		$("#mainForm").validate();
	</script>
	<script src="{{ asset('js/seo-preview.article.handler.js') }}"></script>
	<script type="text/javascript">
		if (typeof CKEDITOR != "undefined") {
			for (var i in CKEDITOR.instances) {
				CKEDITOR.instances[i].on('key', function(e) {
					$(window).bind('beforeunload', function() {
						return 'Bạn có chắc chắn chuyển sang trang khác, Nếu thực hiện dữ liệu sẽ không được lưu';
					});
				});
			}
		}
	</script>
@stop