@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Mã khuyến mãi')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.coupon'))

{{-- Navs --}}
@section('nav')
	@if(request('trash'))
	<a href="{{ current_url('/') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
	@else
	<a href="{{ route('admin.coupon.create') }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Tạo mã khuyến mãi</a>
	<a href="{{ current_url('?trash=1') }}" class="btn btn-default"><i class="fa fa-trash fa-fw"></i> Thùng rác</a>
	@endif
@stop

{{-- Main --}}
@section('main')
	<a class="btn btn-primary mr-1 mb-4" href="{{ route('admin.coupon.export') }}" value="excel" form="filterForm"><i class="fa fa-file-excel-o"></i> Export Excel</a>
	<table class="table table-primary table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th class="text-center">Mã</th>
				<th class="text-center">Giới hạn</th>
				<th>Mức giảm giá</th>
				<th>Đối tượng áp dụng</th>
				<th>Thời điểm</th>
				<th>Số lần<br>sử dụng</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				<tr>
					<td class="text-center">
						@if(date('U', strtotime($item->started_at)) > date('U'))
						<mark class="mark mark-block" style="background-color: #B5B5B5">{{ $item->code }}</mark>
						@elseif((!is_null($item->limited) && $item->limited <= 0) || date('U', strtotime($item->expired_at)) < date('U'))
						<mark class="mark mark-danger mark-block">{{ $item->code }}</mark>
						@else
						<mark class="mark mark-success mark-block">{{ $item->code }}</mark>
						@endif
					</td>
					<td class="text-center">{{ $item->limited ?? 'Không giới hạn' }}</td>
					<td>
						{{ number_format($item->discount,0,',','.') }}{{ $units[$item->unit] }}
						
					</td>
					<td>
						{{ $target_types[$item->target_type] }}
						@if($item->min_value > 0)
							<div>Đơn hàng tối thiểu: {{ number_format($item->min_value,0,',','.') }}đ</div>
						@endif
					</td>
					<td>
						<div>Bắt đầu: {{ date('d/m/Y H:i', strtotime($item->started_at)) }}</div>
						<div>Kết thúc: {{ date('d/m/Y H:i', strtotime($item->expired_at)) }}</div>
					</td>
					<td>
						{{ $item->number_use }}
					</td>
					<td>
						<ul class="table-options justify-content-start">
							@if(request('trash'))
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Khôi phục" onclick="restoreItem(event, {{ $item->id }}, '{{ route('admin.coupon.restore') }}')"><i class="fa fa-undo"></i></button>
								</li>
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa bản ghi" onclick="destroyItem(event, '{{ route('admin.coupon.destroy', $item->id) }}')"><i class="fa fa-remove"></i></button>
								</li>
							@else
								<li>
									<a href="{{ route('admin.coupon.edit', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
								</li>
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Chuyển vào thùng rác" onclick="trashItem(event, {{ $item->id }}, '{{ route('admin.coupon.trash') }}')"><i class="fa fa-trash"></i></button>
								</li>
							@endif
						</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navOrder").addClass("active").find("ul").addClass("show").find(".coupon").addClass("active");
	</script>
	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop