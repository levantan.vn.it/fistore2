@extends('admin.layouts.master')

{{-- Title --}}
@section('title', $data->name)

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.role.edit', $data))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Lưu</button>
	<a href="{{ route('admin.role.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
@stop

{{-- Main --}}
@section('main')
	<div class="row">
		<div class="col-lg-6">
			<div class="card">
				<div class="card-body">
					<form action="{{ route('admin.role.update', $data->id) }}" method="POST" id="mainForm">
						@method('PUT')
						@csrf
						<div class="form-group">
							<label for="name">Tên nhóm quyền <span class="required">*</span></label>
							<input type="text" name="name" value="{{ $data->name }}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="key">Key <span class="required">*</span></label>
							<input type="text" name="key" value="{{ $data->key }}" class="form-control" required>
							<span class="help-block">Sử dụng bởi lập trình viên phát triển<br/>Chỉ nhập chữ thường, số, dấu gạch dưới "_" và không có khoảng trắng.</span>
						</div>
						<div class="form-group">
							<label for="level">Cấp bậc <span class="required">*</span></label>
							<input type="number" name="level" value="{{ $data->level }}" class="form-control" min="1" required{{ $data->key === 'member' ? ' disabled' : null }}>
							@if($data->key === 'member')
							<span class="help-block">Không phân cấp đối với nhóm quyền Thành viên</span>
							@endif
						</div>
						<div class="form-group">
							<label for="password_confirmation">Ghi chú</label>
							<textarea name="comment" rows="5" class="form-control">{!! $data->comment !!}</textarea>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navAccount").addClass("active").find("ul").addClass("show").find(".role").addClass("active");
	</script>
	<script>
		$("#mainForm").validate({
			rules: {
				key: {
					remote: {
						url: "{!! route('admin.role.checkKeyExist') !!}",
						type: "POST",
						data: {
							id: {{ $data->id }},
							key: function () {
								return $('input[name="key"]').val();
							},
							_token:  function () {
								return $('meta[name="csrf-token"]').attr('content');
							}
						}
					}
				}
			}
		});
	</script>
@stop