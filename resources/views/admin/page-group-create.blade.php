@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Tạo nhóm')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.page.group.create'))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Lưu</button>
	<a href="{{ route('admin.page.group.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
@stop

{{-- Main --}}
@section('main')
	<div class="row">
		<div class="col-lg-6">
			<div class="card">
				<div class="card-body">
					<form action="{{ route('admin.page.group.store') }}" method="POST" id="mainForm">
						@csrf
						<div class="form-group">
							<label for="name">Tên nhóm <span class="required">*</span></label>
							<input type="text" name="name" value="{{ old('name') }}" class="form-control" onkeyup="slug_make(event);" required>
						</div>
						<div class="form-group">
							<label for="parent">Thuộc chuyên mục</label>
							<select name="parent" class="custom-select form-control">
								<option value=""></option>
								@foreach($page_groups as $item)
									<option value="{{ $item->id }}">{{ $item->name }}</option>
									@foreach($item->fk_childs as $child)
										<option value="{{ $child->id }}">— {{ $child->name }}</option>
									@endforeach
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Đường dẫn <span class="required">*</span></label>
							<div class="input-group">
								<span class="input-group-addon addon-info">{{ url('') }}</span>
								<input type="text" name="slug" value="{{ old('slug') }}" class="form-control" placeholder="ten-chuyen-muc" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,255}$" required>
								<span class="input-group-btn">
									<button class="btn btn-info spinner-refresh" type="button" onclick="slug_make(event);"><i class="fa fa-refresh"></i></button>
								</span>
							</div>
							<label id="slug-error" class="error" for="slug" style="display: none"></label>
						</div>
						<script>
							$(".spinner-refresh").click(function() {
								var event = $(this);
								$(this).find("i").addClass("fa-spin");
								setTimeout(function() {
									$(event).find("i").removeClass("fa-spin");
								}, 1000);
							});
						</script>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navPage").addClass("active").find("ul").addClass("show").find(".group").addClass("active");
	</script>
	<script>
		var slug_make = function(event)
		{
			var value = $("form").find("input[name=name]").val();
			$("form").find("input[name=slug]").val(convertToSlug(value));
		}
		$("#mainForm").validate({
			rules: {
				name: {
					remote: {
						url : '{!! route("admin.page.group.checkNameExist") !!}',
						type: "POST",
						data: 
						{
							name:  function () {
								return $("input[name=name]").val();
							},
							_token:  function () {
								return $('meta[name="csrf-token"]').attr('content');
							}
						}
					},
				},
				slug: {
					remote: {
						url : '{!! route("admin.page.group.checkSlugExist") !!}',
						type: "POST",
						data: 
						{
							slug:  function () {
								return $("input[name=slug]").val();
							},
							_token:  function () {
								return $('meta[name="csrf-token"]').attr('content');
							}
						}
					},
				}
			}
		});
	</script>
@stop