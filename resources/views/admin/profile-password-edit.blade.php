@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Đổi mật khẩu')

{{-- Import CSS, JS --}}
@section('header')
	{{-- Jquery Validation --}}
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.profile.password'))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Cập nhật</button>
	<a href="{{ url('admin/profile') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
@stop

{{-- Main --}}
@section('main')
	<div class="row">
		<div class="col-lg-6">
			<form action="{{ route('admin.profile.password.update') }}" method="POST" id="mainForm">
				@csrf
				<div class="card card-primary">
					<div class="card-body">
						<div class="form-group">
							<label for="old_password">Mật khẩu hiện tại</label>
							<input type="password" name="old_password" class="form-control" required autofocus>
							@if(Session::has('password_error'))
								<label id="old_password-error" class="error" for="old_password">{!! Session::get('password_error') !!}</label>
							@endif
						</div>
						<div class="form-group">
							<label for="password">Mật khẩu mới <span class="required">*</span></label>
							<input type="password" name="password" class="form-control" id="password" required>
							@if(Session::has('new_password_error'))
								<label id="password-error" class="error" for="new_password_error">{!! Session::get('new_password_error') !!}</label>
							@endif
						</div>
						<div class="form-group">
							<label for="password_confirmation">Nhập lại mật khẩu mới <span class="required">*</span></label>
							<input type="password" name="password_confirmation" class="form-control" required>
							@if(Session::has('confirmation_password_error'))
								<label id="password_confirmation-error" class="error" for="confirmation_password_error">{!! Session::get('confirmation_password_error') !!}</label>
							@endif
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
@stop

{{-- Footer --}}
@section('footer')
<script>
	$("#mainForm").validate({
		rules: {
			old_password: {
				required: true
			},
			password: {
				required: true,
				minlength: 8
			},
			password_confirmation: {
				equalTo: "#password"
			}
		}
	});
	</script>
@stop