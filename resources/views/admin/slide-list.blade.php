@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Slide ảnh')

{{-- Import CSS, JS --}}
@section('header')
	{{-- Fancybox --}}
	<link rel="stylesheet" href="{{ asset('libs/fancybox/3/jquery.fancybox.min.css') }}" />
	<script src="{{ asset('libs/fancybox/3/jquery.fancybox.min.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.slide'))

{{-- Navs --}}
@section('nav')
@stop

{{-- Main --}}
@section('main')
	@foreach($data as $key => $item)
		<div class="card card-primary">
			<div class="card-header" data-toggle="collapse" data-target="#slide{{ $item->id }}" {!! $key == 0 ? 'aria-expanded="true"' : null !!} style="cursor: pointer">
				<div class="close">
				    <i class="fa fa-angle-down fa-lg"></i>
				</div>
				<div class="card-title mb-0">{{ $item->name }}</div>
			</div>
			<div class="collapse{{ $key == 0 ? ' show' : null }}" id="slide{{ $item->id }}">
				<div class="card-body p-0">
					@if(count($item->fk_images) > 0)
					<table class="table mb-0">
						<thead>
							<tr>
								<th width="100" class="text-center">Hình ảnh</th>
								<th width="150" class="text-center">Thứ tự hiển thị</th>
								<th>Đường dẫn trỏ tới</th>
								<th width="50"></th>
							</tr>
						</thead>
						<tbody>
							@foreach($item->fk_images as $image)
								<tr>
									<td>
										<a data-fancybox="slide-{{ $item->id }}" href="{{ media_url('slides', $image->path) }}">
											<img src="{{ media_url('slides', $image->path) }}" width="100">
										</a>
									</td>
									<td class="text-center">
										{{ $image->index }}
									</td>
									<td>
										{{ $image->target_url }}
									</td>
									<td>
										<ul class="table-options">
											<li data-toggle="tooltip" data-placement="top" title="Sửa">
												<button type="button" data-toggle="modal" data-target="#updateImageModal{{ $image->id }}"><i class="fa fa-pencil"></i></button>
											</li>
											<li>
												<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.slide.destroy', $image->id) }}')"><i class="fa fa-remove"></i></button>
											</li>
										</ul>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					@else
					<div class="alert alert-info m-3">Chưa có ảnh nào trong Slide này.</div>
					@endif
				</div>
				<div class="card-footer text-right">
					<button class="btn btn-info" data-toggle="modal" data-target="#createImageModal{{ $item->id }}"><i class="fa fa-plus fa-fw"></i> Thêm ảnh</button>
				</div>
			</div>
		</div>
		<form action="{{ route('admin.slide.store') }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="modal" id="createImageModal{{ $item->id }}" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<input type="hidden" name="target_id" value="{{ $item->id }}">
							<div class="form-group">
								<label for="thumbnail">Hình ảnh <span class="required">*</span></label>
								<div class="custom-image-upload custom-iu-md image-preview">
									<img class="img-preview">
									<label class="custom-label">
										<input type="file" name="thumbnail" onchange="upload_img_preview(event);" accept="image/*" required>
									</label>
									<svg id="i-close" onclick="remove_file(event);" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
										<path d="M2 30 L30 2 M30 30 L2 2"></path>
									</svg>
								</div>
							</div>
							<div class="form-group">
								<label for="target_url">Đường dẫn trỏ tới</label>
								<input type="text" name="target_url" class="form-control">
							</div>
							<div class="form-group">
								<label for="index">Thứ tự hiển thị</label>
								<input type="number" name="index" min="0" class="form-control">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
							<button class="btn btn-success">Thêm</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	@endforeach
@stop

{{-- Footer --}}
@section('footer')
	@foreach($data as $item)
		@foreach($item->fk_images as $image)
			<form action="{{ route('admin.slide.update', $image->id) }}" method="POST" enctype="multipart/form-data">
				@csrf
				@method('PUT')
				<div class="modal" id="updateImageModal{{ $image->id }}" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<input type="hidden" name="target_id" value="{{ $item->id }}">
								<div class="form-group">
									<label for="thumbnail">Hình ảnh <span class="required">*</span></label>
									<div class="custom-image-upload custom-iu-md image-preview">
										<img class="img-preview" src="{{ media_url('slides', $image->path) }}">
										<label class="custom-label">
											<input type="file" name="thumbnail" onchange="upload_img_preview(event);" accept="image/*">
										</label>
										<svg id="i-close" onclick="remove_file(event);" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
											<path d="M2 30 L30 2 M30 30 L2 2"></path>
										</svg>
									</div>
								</div>
								<div class="form-group">
									<label for="target_url">Đường dẫn trỏ tới</label>
									<input type="text" name="target_url" class="form-control" value="{{ $image->target_url }}">
								</div>
								<div class="form-group">
									<label for="index">Thứ tự hiển thị</label>
									<input type="number" name="index" min="0" class="form-control" value="{{ $image->index }}">
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
								<button class="btn btn-success">Lưu thay đổi</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		@endforeach
	@endforeach
	<script>
		@if(request()->route()->getName() == 'admin.slide.banner.index')
			$("#navInterface").addClass("active").find("ul").addClass("show").find(".banner").addClass("active");
		@else
			$("#navInterface").addClass("active").find("ul").addClass("show").find(".slide").addClass("active");
		@endif

		function destroyItem(event, url){
			swal({
				title: 'Bạn có chắc',
				text: "Thực hiện thao tác xóa mục này?",
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: 'Xóa',
				cancelButtonText: 'Bỏ qua'
			}).then(function(isConfirm) {
				if (isConfirm.value) {
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
					$.ajax({
						url : url,
						type : 'DELETE',
						success : function (data) {
							$(event.target).parents("tr").remove();
							toastr[data.alert]("<b>" + data.title + "</b><br/>" + data.msg);
						},
						error: function (xhr, ajaxOptions, thrownError) {
							toastr.error(xhr.responseText);
						}
					});
				}
			})
		}
	</script>
@stop