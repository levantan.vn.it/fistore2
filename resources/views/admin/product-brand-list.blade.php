@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Thương hiệu sản phẩm')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">

	{{-- Fancybox --}}
	<link rel="stylesheet" href="{{ asset('libs/fancybox/3/jquery.fancybox.min.css') }}" />
	<script src="{{ asset('libs/fancybox/3/jquery.fancybox.min.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.product.brand'))

{{-- Navs --}}
@section('nav')
	<a href="{{ route('admin.product.brand.create') }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Tạo mới</a>
@stop

{{-- Main --}}
@section('main')
	<table class="table table-primary table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th width="10">Index</th>
				<th class="no-sort" width="50"></th>
				<th>Thương hiệu</th>
				<th>Đường dẫn</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)			
				<tr>
					<td>{{ $item->index != 99 ? $item->index : 'N/A' }}</td>
					<td>
						<a data-fancybox="gallery-{{ $item->id }}" href="{{ media_url('product-brands', $item->thumbnail) }}">
							<img src="{{ media_url('product-brands', $item->thumbnail) }}" width="60">
						</a>
					</td>
					<td>
						{{ $item->name }}&emsp;{!! $item->pinned ? '<i class="fa fa-thumb-tack" data-toggle="tooltip" data-placement="top" title="Đã ghim lên trang chủ"></i>' : null !!}
					</td>
					<td>{{ $item->slug }}</td>
					<td>
						<ul class="table-options">
							<li>
								<a href="{{ route('admin.product.brand.edit', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
							</li>
							<li>
								<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.product.brand.destroy', $item->id) }}')"><i class="fa fa-remove"></i></button>
							</li>
						</ul>
					</td>
				</tr>

				@foreach($item->fk_childs as $child)
					<tr>
						<td>
							<a data-fancybox="gallery-{{ $child->id }}" href="{{ media_url('product-brands', $child->thumbnail) }}">
								<img src="{{ media_url('product-brands', $child->thumbnail) }}" width="60">
							</a>
						</td>
						<td>
							— {{ $child->name }}&emsp;{!! $child->pinned ? '<i class="fa fa-thumb-tack" data-toggle="tooltip" data-placement="top" title="Đã ghim lên trang chủ"></i>' : null !!}
						</td>
						<td>{{ $child->slug }}</td>
						<td>
							<ul class="table-options">
								<li>
									<a href="{{ route('admin.product.brand.edit', $child->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
								</li>
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.product.brand.destroy', $child->id) }}')"><i class="fa fa-remove"></i></button>
								</li>
							</ul>
						</td>
					</tr>

					@foreach($child->fk_childs as $grandchild)
						<tr>
							<td>
								<a data-fancybox="gallery-{{ $grandchild->id }}" href="{{ media_url('product-brands', $grandchild->thumbnail) }}">
									<img src="{{ media_url('product-brands', $grandchild->thumbnail) }}" width="60">
								</a>
							</td>
							<td>
								——— {{ $grandchild->name }}&emsp;{!! $grandchild->pinned ? '<i class="fa fa-thumb-tack" data-toggle="tooltip" data-placement="top" title="Đã ghim lên trang chủ"></i>' : null !!}
							</td>
							<td>{{ $grandchild->slug }}</td>
							<td>
								<ul class="table-options">
									<li>
										<a href="{{ route('admin.product.brand.edit', $grandchild->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
									</li>
									<li>
										<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.product.brand.destroy', $grandchild->id) }}')"><i class="fa fa-remove"></i></button>
									</li>
								</ul>
							</td>
						</tr>
					@endforeach
				@endforeach
			@endforeach
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navProduct").addClass("active").find("ul").addClass("show").find(".brand").addClass("active");
	</script>

	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop