@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Hồ sơ cá nhân')

{{-- Import CSS, JS --}}
@section('header', null)

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.profile'))

{{-- Navs --}}
@section('nav')
	<a href="{{ current_url('/edit') }}" class="btn btn-info"><i class="fa fa-cog fa-fw"></i> Cập nhật hồ sơ</a>
@stop

{{-- Main --}}
@section('main')
	<div class="row">
		<div class="col-lg-6">
			<div class="card card-primary">
				<div class="card-body">
					<table class="table table-hover table-responsive fz-up-1">
						<tbody>
							<tr>
								<td class="py-4" width="25%">Họ tên</td>
								<td class="white-space-unset"><b>{{ $data->name }}</b></td>
							</tr>
							<tr>
								<td class="py-4">Địa chỉ email</td>
								<td class="white-space-unset"><b>{{ $data->email }}</b></td>
							</tr>
							<tr>
								<td class="py-4">Quyền</td>
								<td class="white-space-unset"><b>{{ $data->fk_role->name }}</b></td>
							</tr>
						</tbody>
					</table>
					<a href="{{ current_url('/password') }}" class="btn btn-info"><i class="fa fa-lock fa-fw"></i> Đổi mật khẩu</a>
				</div>
			</div>
		</div>
	</div>
@stop

{{-- Footer --}}
@section('footer', null)