@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Báo xấu')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.report'))

{{-- Navs --}}
@section('nav')
@stop

{{-- Main --}}
@section('main')
	<table class="table table-primary table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th width="200">Người báo cáo</th>
				<th>Nội dung phản ánh</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				<tr>
					<td>
						<div class="font-weight-bold">{{ $item->fk_user->name }}</div>
						<div class="font-italic"><a href="mailto:{{ $item->fk_user->email }}">{{ $item->fk_user->email }}</a></div>
					</td>
					<td class="white-space-unset">
						<div class="mb-1">Đã phản ánh người dùng <b>{{ $item->fk_comment->fk_author->name }}</b> có hành vi <b>{{ $item->cause }}</b> trong một <a href="{{ comment_link($item->fk_comment) }}" target="_blank" class="font-weight-bold">bình luận</a> tại <a href="{{ product_link($item->fk_comment->fk_product) }}" target="_blank" class="font-weight-bold">{{ $item->fk_comment->fk_product->name }}</a> vào <span class="text-muted">{{ date('d/m/Y', strtotime($item->created_at)) }}</span> lúc <span class="text-muted">{{ date('H:s', strtotime($item->created_at)) }}</span>.</div>
						@if($item->content)
							<div class="font-italic text-danger mb-1">{!! str_limit($item->content, 200) !!}</div>
						@endif
						<b>Trích dẫn bình luận:</b>
						<div class="font-italic card-body" style="background: #dedede"><i class="fa fa-quote-left"></i> {!! str_limit($item->fk_comment->content, 200) !!}</div>
					</td>
					<td>
						<ul class="table-options">
							<li>
								<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.report.destroy', $item->id) }}')"><i class="fa fa-remove"></i></button>
							</li>
						</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navFeedback").addClass("active").find("ul").addClass("show").find(".report").addClass("active");
	</script>
	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop