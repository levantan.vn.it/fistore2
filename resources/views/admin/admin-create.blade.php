@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Tạo người dùng')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.admin.create'))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Lưu</button>
	<a href="{{ route('admin.user.admin.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
@stop

{{-- Main --}}
@section('main')
	<div class="row">
		<div class="col-lg-6">
			<div class="card">
				<div class="card-body">
					<form action="{{ route('admin.user.admin.store') }}" method="POST" id="mainForm">
						@csrf
						<div class="form-group">
							<label for="name">Họ tên <span class="required">*</span></label>
							<input type="text" name="name" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="email">Email <span class="required">*</span></label>
							<input type="email" name="email" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="password">Mật khẩu <span class="required">*</span></label>
							<input type="password" name="password" class="form-control" id="password" required>
						</div>
						<div class="form-group">
							<label for="password_confirmation">Nhập lại mật khẩu <span class="required">*</span></label>
							<input type="password" name="password_confirmation" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="">Vai trò <span class="required">*</span></label>
							<select name="role" class="custom-select form-control" required>
								<option value=""></option>
								@foreach($roles as $role)
								<option value="{{ $role->id }}">{{ $role->name }}</option>
								@endforeach
							</select>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navAccount").addClass("active").find("ul").addClass("show").find(".user").addClass("active");
	</script>
	<script>
		$("#mainForm").validate({
			rules: {
				email: {
					remote: {
						url: "{!! route('admin.user.admin.checkEmailExist') !!}",
						type: "POST",
						data: {
							email: function () {
								return $('input[name="email"]').val();
							},
							_token:  function () {
								return $('meta[name="csrf-token"]').attr('content');
							}
						}
					}
				},
				password: {
					minlength: 8
				},
				password_confirmation: {
					equalTo: "#password"
				}
			}
		});
	</script>
@stop