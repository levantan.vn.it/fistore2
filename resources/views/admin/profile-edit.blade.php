@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Cập nhật hồ sơ')

{{-- Import CSS, JS --}}
@section('header')
	{{-- Jquery Validation --}}
	<script src="{{ asset('libs/jquery-validation/jquery-validation-1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/jquery-validation-1.17.0/localization/messages_vi.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.profile.edit'))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Cập nhật</button>
	<a href="{{ url('admin/profile') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
@stop

{{-- Main --}}
@section('main')
	<div class="row">
		<div class="col-lg-6">
			<form action="{{ route('admin.profile.update') }}" method="POST" id="mainForm">
				@csrf
				<div class="card card-primary">
					<div class="card-body">
						<div class="form-group">
							<label for="name">Họ tên <span class="required">*</span></label>
							<input type="text" name="name" value="{{ $data->name }}" class="form-control" required autofocus>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#mainForm").validate();
	</script>
@stop