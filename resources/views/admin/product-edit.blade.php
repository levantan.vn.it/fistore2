@extends('admin.layouts.master')

{{-- Title --}}
@section('title', $data->name)

{{-- Import CSS, JS --}}
@section('header')
	{{-- Jquery Validation --}}
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>

	{{-- CKEditor --}}
	<script src="{{ asset('libs/ckeditor/4.10.0/ckeditor.js') }}"></script>

	{{-- Selectize --}}
	<script type="text/javascript" src="{{ asset('libs/selectize/0.12.4/js/standalone/selectize.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/selectize/0.12.4/css/selectize.css') }}">

	{{-- Wheel Color Picker --}}
	<link rel="stylesheet" href="{{ asset('libs/wheelcolorpicker/3.0.3/wheelcolorpicker.css') }}">
	<script src="{{ asset('libs/wheelcolorpicker/3.0.3/wheelcolorpicker.min.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.product.edit', $data))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Cập nhật</button>
	<button class="btn btn-primary" name="repost" value="1" form="mainForm"><i class="fa fa-level-up fa-fw"></i> Đăng lên trang đầu</button>
	<button class="btn btn-info" name="draft" value="1" form="mainForm"><i class="fa fa-clipboard fa-fw"></i> Lưu bản nháp</button>
	<a href="{{ route('admin.product.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Hủy bỏ</a>
@stop

{{-- Main --}}
@section('main')
	<form action="{{ route('admin.product.update', $data->id) }}" method="POST" enctype="multipart/form-data" id="mainForm">
		@method("PUT")
		@csrf
		<div class="row">
			<div class="col-lg-8">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="name">Tên sản phẩm <span class="required">*</span></label>
							<input type="text" name="name" class="form-control" value="{{ $data->name }}" required>
						</div>
						<div class="form-group">
							<label for="content">Thông tin sản phẩm</label>
							<textarea name="content" id="content">{!! $data->content !!}</textarea>
							<script>
								CKEDITOR.replace('content');
							</script>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="content">Thông số kỹ thuật</label>
							<textarea name="properties" id="property">{!! $data->properties !!}</textarea>
							<script>
								CKEDITOR.replace('property');
							</script>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="code">Mã sản phẩm (SKU) <span class="required">*</span></label>
							<input type="text" name="code" value="{{ $data->code }}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="external_id">AFR product code</label>
							<input type="text" name="external_id" value="{{ $data->external_id }}" class="form-control">
						</div>
						<div class="form-group">
							<label for="group">Số lượng</label>
							<input {{ count($versions)==0? "readonly":"" }} type="number" name="quantity" min="0" value="{{ $data->quantity }}" class="form-control">
							<span class="help-block">Được tính tự động bằng tổng các versions. Nhập số lượng nếu sản phẩm không có version</span>
						</div>
						<div class="form-group">
							<label for="group">Real Stock Quantity</label>
							<input type="number" data-code="{{ $data->code }}" readonly="true" name="real_stock_quantity" min="0" value="{{ $data->real_stock_quantity??'' }}" class="form-control">
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label for="group">Giá bán hiện tại <span class="required">*</span></label>
									<div class="input-group">
										<span class="input-group-addon addon-info">đ</span>
										<input type="text" name="price" class="form-control" value="{{ number_format($data->price) }}" onkeyup="make_money_format(event)" required>
									</div>
									<label id="price-error" class="error" for="price" style="display: none;"></label>
								</div>
								<div class="form-group">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" name="display_price" class="custom-control-input"{!! $data->display_price ? ' checked' : null !!}>
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">Hiển thị giá bán</span>
									</label>
									<span class="help-block">Bỏ chọn nếu muốn hiển thị giá là <b>"Liên hệ"</b></span>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label for="group">Giá thị trường</label>
									<div class="input-group">
										<span class="input-group-addon addon-info">đ</span>
										<input type="text" name="original_price" class="form-control" value="{{ !empty($data->original_price) ? number_format($data->original_price) : null }}" onkeyup="make_money_format(event)">
									</div>
									<span class="help-block">Giá thị trường luôn cao hơn giá hiện tại. Không bắt buộc nhập</span>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<ul class="list-group alert-fetch" style="margin: 20px">
						</ul>
						<div class="form-group">
							<div class="float-right">
								
								<button type="button" class="btn btn-info" id="fetchStock">Fetch Stock <i class="fa fa-refresh fa-spin" style="display: none;"></i></button>
								<button type="button" class="btn btn-info" id="addVersionBtn" onclick="addVersion()">Thêm phiên bản</button>
							</div>
							<label>Sản phẩm có nhiều phiên bản</label>
							<hr>
							<table width="100%" cellpadding="3" class="mb-3" id="versionTable"{!! empty($versions[0]) ? ' style="display: none;"' : null !!}>
								<thead>
									<tr>
										<th width="20%">Tên mẫu <span class="required">*</span></th>
										<th width="15%">Mã sản phẩm (SKU) <span class="required">*</span></th>
										<th width="15%">AFR product code</th>
										<th width="15%">Màu sắc</th>
										<th width="15%">Giá bán</th>
										<th width="60">Real Stock Quantity</th>
										<th width="60">Số lượng</th>
										<th width="20"></th>
									</tr>
								</thead>
								@for($i = 0;$i < 100; $i++)
								<tbody {!! isset($versions[$i]) ? null : ' style="display: none;"' !!} {!! $i % 2 == 0 ? 'bgcolor="#EDEDED"' : 'bgcolor="#D9D9D9"' !!}>
									<tr>
										<td>
											<input type="text" name="version_name[{{ $i }}]" value="{{ $versions[$i]->name ?? null }}" class="form-control table-element" required{!! isset($versions[$i]) ? null : ' disabled' !!}>
										</td>
										<td>
											<input type="text" name="version_code[{{ $i }}]" value="{{ $versions[$i]->code ?? null }}" class="form-control table-element" required{!! isset($versions[$i]) ? null : ' disabled' !!}>
										</td>
										<td>
											<input type="text" name="version_external_id[{{ $i }}]" value="{{ $versions[$i]->external_id ?? null }}" class="form-control table-element" required{!! isset($versions[$i]) ? null : ' disabled' !!}>
										</td>
										<td>
											<div class="input-group">
												<input type="text" name="version_color[{{ $i }}]" class="form-control table-element" placeholder="000000" data-wheelcolorpicker="{{ $versions[$i]->color ?? null }}" value="{{ $versions[$i]->color ?? null }}" onchange="colorPreview(event)"{!! isset($versions[$i]) ? null : ' disabled' !!}>
												<span class="input-group-addon" style="background: #{{ $versions[$i]->color ?? null }}">&emsp;</span>
											</div>
										</td>
										<td>
											<input type="text" name="version_price[{{ $i }}]" value="{{ isset($versions[$i]->price) ? number_format($versions[$i]->price) : null }}" class="form-control table-element" onkeyup="make_money_format(event)"{!! isset($versions[$i]) ? null : ' disabled' !!}>
										</td>
										<td>
											<input type="number" readonly="true" name="version_real_stock_quantity[{{ $i }}]" value="{{ $versions[$i]->real_stock_quantity ?? null }}" data-code="{{ $versions[$i]->external_id ?? '' }}" class="form-control table-element" min="0"{!! isset($versions[$i]) ? null : ' disabled' !!}>
										</td>
										<td>
											<input type="number" name="version_quantity[{{ $i }}]" value="{{ $versions[$i]->quantity ?? null }}" class="form-control table-element" min="0"{!! isset($versions[$i]) ? null : ' disabled' !!}>
										</td>
										<td style="white-space:nowrap;text-align: center;">
											<input type="hidden" name="version_id[{{ $i }}]" value="{{ $versions[$i]->id ?? null }}" class="form-control table-element"{!! isset($versions[$i]) ? null : ' disabled' !!}>
											<button type="button" class="btn btn-link text-muted px-1" data-toggle="tooltip" data-placement="top" title="Thêm ảnh" onclick="addVersionImage({{ $i }})"><i class="fa fa-picture-o fa-lg"></i></button>
											<button type="button" class="btn btn-link text-muted" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="removeVersion()"><i class="fa fa-trash fa-lg"></i></button>
										</td>
									</tr>
									<tr>
										<td colspan="5" class="py-0">
											<div class="selected-images row row-mx-5">
												@if(!empty($versions[$i]->fk_images))
													@foreach($versions[$i]->fk_images as $imageControl)
													<div class="img-wrap col-6 col-md-3 mt-1 mb-2">
														<div class="custom-image-upload image-preview mx-0">
																<img class="img-preview" src="{{ media_url('products', $imageControl->path) }}">
															<label class="custom-label">
																<input type="hidden" name="version_images_old[]" value="{{ $imageControl->id }}">
															</label>
															<svg id="i-close" onclick="removeVersionImage();" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
																<path d="M2 30 L30 2 M30 30 L2 2"></path>
															</svg>
														</div>
														<div>
															<input type="text" class="d-block form-control" name="version_image_name{{ $imageControl->id }}" placeholder="Tiêu đề" value="{{ $imageControl->name }}">
															<input type="number" class="d-block form-control" name="version_image_index{{ $imageControl->id }}" placeholder="Thứ tự" value="{{ $imageControl->index }}">
														</div>
													</div>
													@endforeach
												@endif
											</div>
										</td>
										<td class="py-0"></td>
									</tr>
								</tbody>
								@endfor
							</table>
							<script>
								function colorPreview(event){
									$(event.target).parents(".input-group").find(".input-group-addon").css("background", "#" + $(event.target).val());
								}
							</script>
						</div>
					</div>
					<div class="card-footer">
						Bạn đã thêm <b id="VersionCount">{{ count($versions) }}</b> phiên bản.<!-- Tối đa <b>20</b> phiên bản.-->
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="float-right">
							<button type="button" class="btn btn-link" data-toggle="collapse" href="#seoOption">Tùy chỉnh nội dung SEO</button>
						</div>
						<label>Xem trước kết quả tìm kiếm</label>

						<div class="seo-container">
							<p class="seo-title">{{ $data->title ?? $data->name }}</p>
							<p class="seo-url">{{ url($data->slug) }}.html</p>
							<p class="seo-description">{{ $data->description ?? 'Mô tả sản phẩm hoặc trang web được rút gọn từ bài viết thông tin sản phẩm' }}</p>

							<div class="collapse" id="seoOption">
								<div class="card">
									<hr/>
									<div class="form-group">
										<label>Tiêu đề</label>
										<input type="text" name="title" class="form-control" value="{{ $data->title }}" onkeyup="title_make();">
									</div>

									<div class="form-group">
										<label>Mô tả</label>
										<textarea rows="3" name="description" class="form-control" onkeyup="description_make();">{!! $data->description !!}</textarea>
									</div>

									<div class="form-group">
										<label>Keywords</label>
										<textarea rows="2" name="keywords" class="form-control">{!! $data->keywords !!}</textarea>
									</div>

									<div class="form-group">
										<label>Đường dẫn <span class="required">*</span></label>
										<div class="input-group">
											<span class="input-group-addon addon-info">{{ url('') }}</span>
											<input type="text" name="slug" class="form-control" value="{{ $data->slug }}" placeholder="ten-san-pham-123456" required>
											<span class="input-group-btn">
												<button class="btn btn-info slug-refresh" type="button" onclick="slug_make();"><i class="fa fa-refresh"></i></button>
											</span>
										</div>
										<label id="slug-error" class="error" for="slug" style="display: none;"></label>
									</div>
									<script>
										$(".slug-refresh").click(function() {
											var event = $(this);
											$(this).find("i").addClass("fa-spin");
											setTimeout(function() {
												$(event).find("i").removeClass("fa-spin");
											}, 1000);
										});
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Ảnh đại diện sản phẩm</label>
							<div class="custom-image-upload custom-iu-md image-preview m-0">
								<img class="img-preview" src="{{ media_url('products', $data->thumbnail) }}">
								<label class="custom-label">
									<input type="file" name="thumbnail" onchange="upload_img_preview(event);" accept="image/*">
								</label>
								<svg id="i-close" onclick="remove_file(event);" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
									<path d="M2 30 L30 2 M30 30 L2 2"></path>
								</svg>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label>Ảnh mô tả sản phẩm</label>
							<div id="multiImagePanel">
								<div class="row">
                                    <input type="hidden" id="numimage" value="{{ count($data->fk_images) }}" />
									@foreach($data->fk_images as $key => $image)
									<div class="img-wrap col-md-3">
										<div class="image_{{ $key+1 }}" id="image">
											<label>{{--  for="image_{{ $key+1 }}" --}}
												<img id="image_preview_{{ $key+1 }}" src="{{ media_url('products', $image->path) }}" height="130px">
												<input type="text" id="image_name_{{ $key+1 }}" class="d-block form-control" name="image_name{{ $image->id }}" placeholder="Tiêu đề" value="{{ $image->name }}">
												<input type="number" id="image_index_{{ $key+1 }}" class="d-block form-control" name="image_index{{ $image->id }}" placeholder="Thứ tự" value="{{ $image->index }}">
											</label>
											<input type="file" class="photo" name="images[]" id="image_{{ $key+1 }}" onchange="makeImageArr(event, {{ $key+1 }})" accept="image/*">
											<input type="hidden" name="old_images[]" value="{{ $image->id }}" id="old_images_{{ $key+1 }}">
											<svg id="i-close" onclick="this.parentElement.parentElement.remove();" class="remove button_{{ $key+1 }}" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
												<path d="M2 30 L30 2 M30 30 L2 2"></path>
											</svg>
										</div>
									</div>
									@endforeach
								</div>
							</div>
						</div>

						<div class="form-group">
							<button class="btn btn-info" type="button" id="add_image"><i class="fa fa-plus"></i>&emsp;Thêm ảnh</button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg 4">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<div class="flex-row">
								<label for="show">Hiển thị công khai</label>
								<label class="custom-toggle toggle-info mb-0">
									<input type="checkbox" name="show" class="toggle-checkbox"{!! $data->hidden ? null : ' checked' !!}>
									<div class="toggle-inner mr-0">
										<span class="toggle-button"></span>
									</div>
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="flex-row">
								<label for="close">Tắt chức năng đặt hàng</label>
								<label class="custom-toggle toggle-danger mb-0">
									<input type="checkbox" name="close" class="toggle-checkbox"{!! $data->closed ? ' checked' : null !!}>
									<div class="toggle-inner mr-0">
										<span class="toggle-button"></span>
									</div>
								</label>
							</div>
							<span class="help-block">Chỉ hiển thị thông tin sản phẩm, không kinh doanh sản phẩm này</span>
						</div>
						
						<div class="form-group">
							<label for="group">Trạng thái sản phẩm</label>
							<select name="status" class="custom-select form-control" >
								@foreach ($status as $key => $value)
									<option value="{{ $key }}" {!! $data->status === $key ? ' selected' : null !!}>{{ $value }}</option>
								@endforeach 
							</select>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="group">Nhóm sản phẩm</label>
							<div class="card px-4 py-3" style="background: #F8F8F8">
								{!! get_checkbox_product_groups_control($data) !!}
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="group">Loại sản phẩm</label>
							{!! get_selectize_product_categories_control($data->category_id) !!}
							<label id="categories-selectized-error" class="error" for="categories-selectized" style="display: none"></label>
						</div>
						<div class="form-group">
							<label for="group">Thương hiệu <span class="required">*</span></label>
							{!! get_selectize_product_brands_control($data->brand_id) !!}
							<label id="brands-selectized-error" class="error" for="brands-selectized" style="display: none"></label>
						</div>
						<div class="form-group">
							<div class="flex-row">
								<label for="pre_order">Pre-order</label>
								<label class="custom-toggle toggle-info mb-0">
									<input type="checkbox" name="pre_order" class="toggle-checkbox"{!! $data->pre_order ? 'checked' : null !!}>
									<div class="toggle-inner mr-0">
										<span class="toggle-button"></span>
									</div>
								</label>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Thêm từ khóa liên quan (Tags)</label>
							<div class="control-group">
								<select id="tags" name="tags[]" multiple placeholder="Chọn trong danh sách hoặc Enter để thêm từ khóa">
									@foreach($tags as $tag)
										<option value="{{ $tag->keyword }}"{!! strpos($data->tags, $tag->keyword) !== false ? ' selected' : null !!}>{{ $tag->keyword }}</option>
									@endforeach
								</select>
							</div>
							<script>
								$('#tags').selectize({
									plugins: ['remove_button'],
									delimiter: ',',
									persist: false,
									createOnBlur: true,
									create: true
								});
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navProduct").addClass("active").find("ul").addClass("show").find(".product").addClass("active");

		$("#mainForm").validate();

		// Multi-Image Handle
		var product_id = "{{ $data->id }}";
	</script>
	<script src="{{ asset('js/admin/multi-image.handler.js') }}"></script>
	<script src="{{ asset('js/admin/seo-preview.handler.js') }}"></script>
	<script src="{{ asset('js/admin/product.handler.js') }}"></script>
	<script type="text/javascript">
		if (typeof CKEDITOR != "undefined") {
			for (var i in CKEDITOR.instances) {
				CKEDITOR.instances[i].on('key', function(e) {
					$(window).bind('beforeunload', function() {
						return 'Bạn có chắc chắn chuyển sang trang khác, Nếu thực hiện dữ liệu sẽ không được lưu';
					});
				});
			}
		}
		$('#fetchStock').on('click',function(){
			var productId = "{{$data->id}}";
			var obj = $(this);
			$.ajax({
			    type: "POST",
			    url: "{{url('admin/products/fecthStock')}}",
			    data: {
			    	productId: productId,
			    	"_token": "{{ csrf_token() }}",
			    },
			    dataType: "JSON",
			    beforeSend : function(){
			        obj.find('i').show();
			        obj.addClass('disabled');
			        obj.attr('disabled','true');
			    },
			    success: function(result){
			        obj.find('i').hide();
			        obj.removeClass('disabled');
			        obj.removeAttr('disabled');
			        // $('.alert-fetch').find('strong').text(result.title);
			        // $('.alert-fetch').find('span').text(result.msg);
			        var html = '';
			        if(result.alert == 'success'){
			        	$('.alert-fetch').addClass('alert-success').removeClass('alert-danger');
			        	if(result.data.length){
			        		var total_quantity = 0;
			        		 
			        		$.each( result.data, function( key, value ) {
			        		  	var pd = value.productCode;
			        		  	var quantity = value.productStockQuantity;
			        		  	$('input[data-code="'+pd+'"]').val(quantity);
			        		  	total_quantity += quantity;
			        		  	if(value.success){
			        		  		html += '<li class="list-group-item list-group-item-success">'+value.message+'</li>';
			        		  	}else{
			        		  		html += '<li class="list-group-item list-group-item-danger">'+value.message+'</li>';
			        		  	}
			        		});
			        		$('input[name="real_stock_quantity"]').val(total_quantity);
			        	}
			        	$('.alert-fetch').html(html);
			            return false;
			        }
			        else{
			        	html += '<li class="list-group-item list-group-item-danger">'+result.msg+'</li>';
			        	$('.alert-fetch').html(html);
			        	// $('.alert-fetch').removeClass('alert-success').addClass('alert-danger');
			            return false;
			        }
			        
			    },
			    error: function(jqXHR, textStatus, errorThrown){
			    	html += '<li class="list-group-item list-group-item-danger">Lỗi hệ thống!Vui lòng thử lại hoặc liên hệ với lập trình viên để khắc phục chúng</li>';
			    	$('.alert-fetch').html(html);
			    	// $('.alert-fetch').find('strong').text("Lỗi hệ thống!");
			    	// $('.alert-fetch').find('span').text("Vui lòng thử lại hoặc liên hệ với lập trình viên để khắc phục chúng.");
			    	// $('.alert-fetch').removeClass('alert-success').addClass('alert-danger');
			    }
			});
			
			
		});
	</script>
@stop
