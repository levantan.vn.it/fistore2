@extends('admin.layouts.master')

{{-- Title --}}
@section('title', $data->name)

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.article.category.edit', $data))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Lưu</button>
	<a href="{{ route('admin.article.category.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
@stop

{{-- Main --}}
@section('main')
	<div class="row">
		<div class="col-lg-6">
			<div class="card">
				<div class="card-body">
					<form action="{{ route('admin.article.category.update', $data->id) }}" method="POST" id="mainForm">
						@method('PUT')
						@csrf
						<div class="form-group">
							<label for="name">Tên chuyên mục <span class="required">*</span></label>
							<input type="text" name="name" value="{{ $data->name }}" class="form-control" onkeyup="slug_make(event);" required>
						</div>
						<div class="form-group">
							<label for="parent">Thuộc chuyên mục cha</label>
							<select name="parent" class="custom-select form-control">
								<option value=""></option>
								@foreach($categories as $category_1)
									<option value="{{ $category_1->id }}"{!! $data->parent_id == $category_1->id ? ' selected' : null !!}>{{ $category_1->name }}</option>
									@foreach($category_1->fk_childs as $category_2)
										@if($category_2->id != $data->id)
										<option value="{{ $category_2->id }}"{!! $data->parent_id == $category_2->id ? ' selected' : null !!}>— {{ $category_2->name }}</option>
										@endif
									@endforeach
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="title">Tiêu đề</label>
							<input type="text" name="title" value="{{ $data->name }}" class="form-control">
							<span class="help-block">Hiển thị trên tiêu đề trình duyệt và trong kết quả tìm kiếm của Google</span>
						</div>
						<div class="form-group">
							<label for="description">Mô tả</label>
							<textarea name="description" rows="4" class="form-control">{!! $data->description !!}</textarea>
							<span class="help-block">Hiển thị mô tả trong kết quả tìm kiếm của Google</span>
						</div>
						<div class="form-group">
							<label>Đường dẫn <span class="required">*</span></label>
							<div class="input-group">
								<span class="input-group-addon addon-info">{{ url('') }}</span>
								<input type="text" name="slug" value="{{ $data->slug }}" class="form-control" placeholder="ten-danh-muc" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,255}$" required>
								<span class="input-group-btn">
									<button class="btn btn-info spinner-refresh" type="button" onclick="slug_make(event);"><i class="fa fa-refresh"></i></button>
								</span>
							</div>
							<label id="slug-error" class="error" for="slug" style="display: none"></label>
						</div>
						<script>
							$(".spinner-refresh").click(function() {
								var event = $(this);
								$(this).find("i").addClass("fa-spin");
								setTimeout(function() {
									$(event).find("i").removeClass("fa-spin");
								}, 1000);
							});
						</script>
						<hr>
						<div class="form-group">
							<label class="custom-toggle toggle-info">
								<input type="checkbox" name="pinned" class="toggle-checkbox"{!! $data->pinned ? ' checked' : null !!}>
								<div class="toggle-inner">
									<span class="toggle-button"></span>
								</div>
								<span class="toggle-text">Ghim lên trang chủ</span>
							</label>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navArticle").addClass("active").find("ul").addClass("show").find(".category").addClass("active");
	</script>
	<script>
		var slug_make = function(event)
		{
			var value = $("form").find("input[name=name]").val();
			$("form").find("input[name=slug]").val(convertToSlug(value));
		}
		$("#mainForm").validate({
			rules: {
				name: {
					remote: {
						url : '{!! route("admin.article.category.checkNameExist") !!}',
						type: "POST",
						data: 
						{
							id: {{ $data->id }},
							name:  function () {
								return $("input[name=name]").val();
							},
							_token:  function () {
								return $('meta[name="csrf-token"]').attr('content');
							}
						}
					},
				},
				slug: {
					remote: {
						url : '{!! route("admin.article.category.checkSlugExist") !!}',
						type: "POST",
						data: 
						{
							id: {{ $data->id }},
							slug:  function () {
								return $("input[name=slug]").val();
							},
							_token:  function () {
								return $('meta[name="csrf-token"]').attr('content');
							}
						}
					},
				}
			}
		});
	</script>
@stop