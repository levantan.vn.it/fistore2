<nav class="navbar fixed-top">
	@yield('breadcrumbs')
	<div class="buttons">
		@yield('nav')
	</div>
</nav>