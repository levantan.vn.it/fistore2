@extends('admin.layouts.master')

{{-- Title --}}
@section('title', $data->name)

{{-- Import CSS, JS --}}
@section('header')
	{{-- Jquery Validation --}}
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>

	{{-- CKEditor --}}
	<script src="{{ asset('libs/ckeditor/4.10.0/ckeditor.js') }}"></script>

	{{-- Selectize --}}
	<script type="text/javascript" src="{{ asset('libs/selectize/0.12.4/js/standalone/selectize.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/selectize/0.12.4/css/selectize.css') }}">
	
    {{-- Wickedpicker --}}
	<script type="text/javascript" src="{{ asset('libs/wickedpicker/0.4.1/wickedpicker.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/wickedpicker/0.4.1/wickedpicker.min.css') }}">
    
	{{-- Wheel Color Picker --}}
	<link rel="stylesheet" href="{{ asset('libs/wheelcolorpicker/3.0.3/wheelcolorpicker.css') }}">
	<script src="{{ asset('libs/wheelcolorpicker/3.0.3/wheelcolorpicker.min.js') }}"></script>

	<style type="text/css">
		fieldset.scheduler-border {
	        border: 1px dotted #dfdfdf !important;
	        padding: 0 1.4em 1.4em 1.4em !important;
	        margin: 0 0 1.5em 0 !important;
	        -webkit-box-shadow:  0px 0px 0px 0px #fafafa;
	                box-shadow:  0px 0px 0px 0px #fafafa;
	    }

        legend.scheduler-border {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;
            width:auto;
            padding:0 10px;
            border-bottom:none;
        }
	</style>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.sale.edit', $data))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Cập nhật</button>
	<a href="{{ route('admin.sale.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Hủy bỏ</a>
@stop

{{-- Main --}}
@section('main')
	<form action="{{ route('admin.sale.update', $data->id) }}" method="POST" enctype="multipart/form-data" id="mainForm">
		@method("PUT")
		@csrf
		<div class="row">
			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="name">Tên chương trình khuyến mãi <span class="required">*</span></label>
							<input type="text" name="name" class="form-control" value="{{ $data->name }}" onkeyup="slug_make();" required>
						</div>
						<div class="form-group"  style="display:none;">>
							<label>Đường dẫn <span class="required">*</span></label>
							<div class="input-group">
								<span class="input-group-addon addon-info">{{ url('/') }}/sale/</span>
								<input type="text" name="slug" class="form-control" value="{{ $data->slug }}" placeholder="ten-san-pham-123456" required>
								<span class="input-group-btn">
									<button class="btn btn-info slug-refresh" type="button" onclick="slug_make();"><i class="fa fa-refresh"></i></button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label for="products">Giảm giá theo</label>
							<div class="control-group">
								<select class="custom-select form-control" name="price_type" style="height: 36px !important;font-size: 1em;">
									@if($data->price_type == 1)
									<option value="1" selected>Giảm giá theo %</option>
									<option value="2">Giảm giá theo số tiền</option>
									@else
									<option value="1" >Giảm giá theo %</option>
									<option value="2" selected>Giảm giá theo số tiền</option>
									@endif
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="name">Giá trị khuyến mãi <span class="required">*</span></label>
							<input type="text" name="price_down" onkeyup="make_money_format(event)" class="form-control" value="{{ !empty($data->price_down) ? number_format($data->price_down) : null }}">
						</div>
						<div class="form-group" style="display:none;">
							<label for="logo">Ảnh border <span class="required">*</span></label>
							<div class="custom-image-upload custom-iu-sm image-preview m-0">
								<img class="img-preview" src="{{ media_url('sales', $data->image_border) }}">
								<label class="custom-label">
									<input type="file" name="image_border" onchange="upload_img_preview(event);" accept="image/*">
								</label>
							</div>
						</div>
						<div class="form-group">
							<label for="name">Giới hạn số lượng mỗi lượt mua hàng</label>
							<input type="number" name="limit" class="form-control" value="{{ $data->limit }}" min="0">
							<span class="help-block">Số lượng giới hạn được phép mua mỗi lần.<br/>Để trống nếu không giới hạn.</span>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="row row-mx-5">
									<div class="col-8">
										<div class="form-group">
											<label for="started_at">Ngày bắt đầu <span class="required">*</span></label>
											<div class="custom-datetimepicker">
												<div class="form-inline">
													<input type="text" name="started_at" class="form-control datepicker" placeholder="dd/mm/yyyy" value="{{ date('d/m/Y', strtotime($data->started_at)) }}" style="min-width: 100%;" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" required>
												</div>
											</div>
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>&emsp;</label>
											<input type="text" name="started_at_time" id="startedAtTime" placeholder="00:00" value="{{ date('H:i', strtotime($data->started_at)) }}" class="form-control timepicker" required/>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="row row-mx-5">
									<div class="col-8">
										<div class="form-group">
											<label for="expired_at">Ngày hết hạn <span class="required">*</span></label>
											<div class="custom-datetimepicker">
												<div class="form-inline">
													<input type="text" name="expired_at" value="{{ date('d/m/Y', strtotime($data->expired_at)) }}" class="form-control datepicker" placeholder="dd/mm/yyyy" style="min-width: 100%;" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" required>
												</div>
											</div>
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>&emsp;</label>
											<input type="text" name="expired_at_time" id="expiredAtTime" value="" placeholder="00:00" class="form-control timepicker" required/>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="flex-row">
								<label for="show">Hiển thị công khai</label>
								<label class="custom-toggle toggle-info mb-0">
									@if($data->status == 1)
									<input type="checkbox" name="status" class="toggle-checkbox" checked>
									@else
									<input type="checkbox" name="status" class="toggle-checkbox">
									@endif
									<div class="toggle-inner mr-0">
										<span class="toggle-button"></span>
									</div>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="products">Chọn sản phẩm</label>
							<div class="control-group">
								<select id="products" name="products[]" multiple placeholder="Tên sản phẩm, Mã sản phẩm" required>
									@foreach($products as $product)
									<option value="{{ $product->id }}" {{ $data->fk_products->contains($product) ? 'selected' : null }}>{{ $product->name }}</option>
									@endforeach
								</select>
							</div>
							<script>
								$('#products').selectize({
									maxItems: 20,
									create: false,
									persist: false,
									valueField: 'id',
									labelField: 'name',
									searchField: ['name', 'code'],
									options: [
										@foreach($products as $product)
											{
												id: {{ $product->id }}, 
												name: '{{ $product->name }}', 
												code: '{{ $product->code }}', 
												thumbnail: '{{ $product->thumbnail }}',
												price: '{{ $product->price }}',
											},										
										@endforeach
									],
									render: {
								        option: function(item, escape) {
							            	var hitem = '<div class="selectize-item">';
							            		hitem +=	'<img src="{{config('filesystems.s3.url')}}/media/products/' + escape(item.thumbnail) + '">';
							            		hitem +=	'<div class="item-name">' + escape(item.name) + '</div>';
							            		hitem +=	'<div class="item-code">Mã sản phẩm: <strong>' + escape(item.code) + '</strong></div>';
							            		hitem +=	'<div class="item-price">Giá bán: <strong>' + number_format(escape(item.price)) + '</strong>đ</div>';
							                return hitem;
								        }
								    },
								});
								</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navSaleOf").addClass("active").find("ul").addClass("show").find(".sales").addClass("active");

		$("#mainForm").validate();
	</script>
	<script>
		// Datetime Picker
		$(function() {
			$( ".datepicker" ).datepicker({
				dateFormat : "dd/mm/yy"
			});
		});
		$('#startedAtTime').wickedpicker({
			now: "{{ date('H:i', strtotime($data->started_at)) }}",
			twentyFour: true,
			title: 'Thời gian',
			showSeconds: false,
		});
		$('#expiredAtTime').wickedpicker({
			now: "{{ date('H:i', strtotime($data->expired_at)) }}",
			twentyFour: true,
			title: 'Thời gian',
			showSeconds: false,
		});
	</script>
	<script src="{{ asset('js/admin/seo-preview.handler.js') }}"></script>
	<script src="{{ asset('js/admin/product.handler.js') }}"></script>
	<style>
    	.wickedpicker__controls__control-up:before {
		    font-family: FontAwesome;
		    font-style: normal;
		    font-weight: 400;
		    speak: none;
		    content: "\f062";
		}
		.wickedpicker__controls__control-down:after{
			font-family: FontAwesome;
		    font-style: normal;
		    font-weight: 400;
		    speak: none;
		    content: "\f063";
		}
		.selectize-item{
			display: block;
		}
		.selectize-item img{
			float: left;
			width: 70px;
			margin-right: 10px;
			height: 4.5rem;
			object-fit: contain;
			padding: 2px;
			background-color: #fafafa;
			border-radius: 3px;
			border: 1px solid #DCDCDC;
		}
		.selectize-item .item-name{
			font-weight: 500;
		}
		.selectize-item .item-price strong{
			font-weight: 500;
			color: #ED0000;
		}
	</style>
@stop