@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Chuyên mục bài viết')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.product.group'))

{{-- Navs --}}
@section('nav')
	<a href="{{ route('admin.product.group.create') }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Tạo mới</a>
@stop

{{-- Main --}}
@section('main')
	<table class="table table-primary table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th class="no-sort" width="50"></th>
				<th>Tên nhóm</th>
				<th>Đường dẫn</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)			
				<tr>
					<td>
						<a data-fancybox="gallery-{{ $item->id }}" href="{{ media_url('product-groups', $item->thumbnail) }}">
							<img src="{{ media_url('product-groups', $item->thumbnail) }}" width="60">
						</a>
					</td>
					<td>
						{{ $item->name }}&emsp;{!! $item->pinned ? '<i class="fa fa-thumb-tack" data-toggle="tooltip" data-placement="top" title="Đã ghim lên trang chủ"></i>' : null !!}
					</td>
					<td>{{ $item->slug }}</td>
					<td>
						<ul class="table-options">
							<li>
								<a href="{{ route('admin.product.group.edit', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
							</li>
							<li>
								<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.product.group.destroy', $item->id) }}')"><i class="fa fa-remove"></i></button>
							</li>
						</ul>
					</td>
				</tr>

				@foreach($item->fk_childs as $child)
					<tr>
						<td>
							<a data-fancybox="gallery-{{ $child->id }}" href="{{ media_url('product-groups', $child->thumbnail) }}">
								<img src="{{ media_url('product-groups', $child->thumbnail) }}" width="60">
							</a>
						</td>
						<td>
							— {{ $child->name }}&emsp;{!! $child->pinned ? '<i class="fa fa-thumb-tack" data-toggle="tooltip" data-placement="top" title="Đã ghim lên trang chủ"></i>' : null !!}
						</td>
						<td>{{ $child->slug }}</td>
						<td>
							<ul class="table-options">
								<li>
									<a href="{{ route('admin.product.group.edit', $child->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
								</li>
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.product.group.destroy', $child->id) }}')"><i class="fa fa-remove"></i></button>
								</li>
							</ul>
						</td>
					</tr>

					@foreach($child->fk_childs as $grandchild)
						<tr>
							<td>
								<a data-fancybox="gallery-{{ $grandchild->id }}" href="{{ media_url('product-groups', $grandchild->thumbnail) }}">
									<img src="{{ media_url('product-groups', $grandchild->thumbnail) }}" width="60">
								</a>
							</td>
							<td>
								——— {{ $grandchild->name }}&emsp;{!! $grandchild->pinned ? '<i class="fa fa-thumb-tack" data-toggle="tooltip" data-placement="top" title="Đã ghim lên trang chủ"></i>' : null !!}
							</td>
							<td>{{ $grandchild->slug }}</td>
							<td>
								<ul class="table-options">
									<li>
										<a href="{{ route('admin.product.group.edit', $grandchild->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
									</li>
									<li>
										<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.product.group.destroy', $grandchild->id) }}')"><i class="fa fa-remove"></i></button>
									</li>
								</ul>
							</td>
						</tr>
					@endforeach
				@endforeach
			@endforeach
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navProduct").addClass("active").find("ul").addClass("show").find(".group").addClass("active");
	</script>

	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop