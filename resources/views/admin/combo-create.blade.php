@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Tạo gói khuyến mãi mới')

{{-- Import CSS, JS --}}
@section('header')
	{{-- Jquery Validation --}}
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>

	{{-- CKEditor --}}
	<script src="{{ asset('libs/ckeditor/4.10.0/ckeditor.js') }}"></script>

	{{-- Selectize --}}
	<script type="text/javascript" src="{{ asset('libs/selectize/0.12.4/js/standalone/selectize.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/selectize/0.12.4/css/selectize.css') }}">

	{{-- Wheel Color Picker --}}
	<link rel="stylesheet" href="{{ asset('libs/wheelcolorpicker/3.0.3/wheelcolorpicker.css') }}">
	<script src="{{ asset('libs/wheelcolorpicker/3.0.3/wheelcolorpicker.min.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.combo.create'))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Đăng gói khuyến mãi</button>
	<a href="{{ route('admin.combo.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Hủy bỏ</a>
@stop

{{-- Main --}}
@section('main')
	<form action="{{ route('admin.combo.store') }}" method="POST" enctype="multipart/form-data" id="mainForm">
		@csrf
		<div class="row">
			<div class="col-lg-8">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="name">Tên gói khuyến mãi <span class="required">*</span></label>
							<input type="text" name="name" class="form-control" onkeyup="slug_make();" required>
						</div>
						<div class="form-group">
							<label for="content">Thông tin gói khuyến mãi</label>
							<textarea name="content" id="content"></textarea>
							<script>
								CKEDITOR.replace('content');
							</script>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="products">Chọn sản phẩm</label>
							<div class="control-group">
								<select id="products" name="products[]" multiple placeholder="Tên sản phẩm, Mã sản phẩm" required>
									@foreach($products as $product)
									<option value="{{ $product->id }}">{{ $product->name }}</option>
									@endforeach
								</select>
							</div>
							<script>
								$('#products').selectize({
									plugins: ['remove_button'],
									delimiter: ',',
									persist: false,
									create: false,
									maxItems: null,
								});
							</script>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="code">Mã sản phẩm (SKU) <span class="required">*</span></label>
							<input type="text" name="code" value="{{ $code }}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="group">Số lượng</label>
							<input type="number" name="quantity" min="0" class="form-control">
							<span class="help-block">Để trống nếu không giới hạn số lượng</span>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="group">Giá bán <span class="required">*</span></label>
									<div class="input-group">
										<span class="input-group-addon addon-info">đ</span>
										<input type="text" name="price" class="form-control" onkeyup="make_money_format(event)" required>
									</div>
									<label id="price-error" class="error" for="price" style="display: none;"></label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="group">Giá thị trường</label>
									<div class="input-group">
										<span class="input-group-addon addon-info">đ</span>
										<input type="text" name="original_price" class="form-control" onkeyup="make_money_format(event)">
									</div>
									<label id="original_price-error" class="error" for="original_price" style="display: none;"></label>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="float-right">
							<button type="button" class="btn btn-link" data-toggle="collapse" href="#seoOption">Tùy chỉnh nội dung SEO</button>
						</div>
						<label>Xem trước kết quả tìm kiếm</label>

						<div class="seo-container">
							<p class="seo-title">Tên sản phẩm</p>
							<p class="seo-url">{{ url('ten-san-pham-123456.htm') }}</p>
							<p class="seo-description">Mô tả sản phẩm hoặc trang web được rút gọn từ bài viết thông tin sản phẩm</p>

							<div class="collapse" id="seoOption">
								<div class="card">
									<hr/>
									<div class="form-group">
										<label>Tiêu đề</label>
										<input type="text" name="title" class="form-control" onkeyup="title_make();">
									</div>

									<div class="form-group">
										<label>Mô tả</label>
										<textarea rows="3" name="description" class="form-control" onkeyup="description_make();"></textarea>
									</div>

									<div class="form-group">
										<label>Đường dẫn <span class="required">*</span></label>
										<div class="input-group">
											<span class="input-group-addon addon-info">{{ url('') }}/</span>
											<input type="text" name="slug" class="form-control" placeholder="ten-san-pham-123456" required>
											<span class="input-group-btn">
												<button class="btn btn-info slug-refresh" type="button" onclick="slug_make();"><i class="fa fa-refresh"></i></button>
											</span>
										</div>
										<label id="slug-error" class="error" for="slug" style="display: none;"></label>
									</div>
									<script>
										$(".slug-refresh").click(function() {
											var event = $(this);
											$(this).find("i").addClass("fa-spin");
											setTimeout(function() {
												$(event).find("i").removeClass("fa-spin");
											}, 1000);
										});
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Ảnh đại diện sản phẩm</label>
							<div class="custom-image-upload custom-iu-md image-preview m-0">
								<img class="img-preview">
								<label class="custom-label">
									<input type="file" name="thumbnail" onchange="upload_img_preview(event);" accept="image/*" required>
								</label>
								<svg id="i-close" onclick="remove_file(event);" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
									<path d="M2 30 L30 2 M30 30 L2 2"></path>
								</svg>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label>Ảnh mô tả sản phẩm</label>
							<div id="multiImagePanel">
								<div class="row">
									<input type="hidden" id="numimage" value="1">
									<div class="img-wrap col-md-3">
										<div class="image_1" id="image">
											<label for="image_1">
												<img id="image_preview_1" height="130px">
												<input type="text" class="d-block form-control" id="image_name_1" placeholder="Tiêu đề" name="names[]">
												<input type="number" class="d-block form-control" id="image_index_1" placeholder="Thứ tự" name="indexes[]">
											</label>
											<input type="file" class="photo" name="images[]" id="image_1" onchange="makeImageArr(event, 1)" accept="image/*">
											<svg id="i-close" onclick="this.parentElement.parentElement.remove();" class="remove button_1" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
												<path d="M2 30 L30 2 M30 30 L2 2"></path>
											</svg>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<button class="btn btn-info" type="button" id="add_image"><i class="fa fa-plus"></i>&emsp;Thêm ảnh</button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg 4">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<div class="flex-row">
								<label for="show">Hiển thị công khai</label>
								<label class="custom-toggle toggle-info mb-0">
									<input type="checkbox" name="show" class="toggle-checkbox" checked>
									<div class="toggle-inner mr-0">
										<span class="toggle-button"></span>
									</div>
								</label>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Thêm từ khóa liên quan (Tags)</label>
							<div class="control-group">
								<select id="tags" name="tags[]" multiple placeholder="Chọn trong danh sách hoặc Enter để thêm từ khóa">
									@foreach($tags as $tag)
										<option value="{{ $tag->keyword }}">{{ $tag->keyword }}</option>
									@endforeach
								</select>
							</div>
							<script>
								$('#tags').selectize({
									plugins: ['remove_button'],
									delimiter: ',',
									persist: false,
									createOnBlur: true,
									create: true
								});
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navSaleOf").addClass("active").find("ul").addClass("show").find(".combo").addClass("active");

		$("#mainForm").validate();

		// Multi-Image Handle
		var product_id = "";
		var i = 2;
	</script>
	<script src="{{ asset('js/admin/multi-image.handler.js') }}"></script>
	<script src="{{ asset('js/admin/seo-preview.handler.js') }}"></script>
	<script src="{{ asset('js/admin/product.handler.js') }}"></script>
	<style>
		.selectize-item{
			display: block;
		}
		.selectize-item img{
			float: left;
			width: 70px;
			margin-right: 10px;
			height: 4.5rem;
			object-fit: contain;
			padding: 2px;
			background-color: #fafafa;
			border-radius: 3px;
			border: 1px solid #DCDCDC;
		}
		.selectize-item .item-name{
			font-weight: 500;
		}
		.selectize-item .item-price strong{
			font-weight: 500;
			color: #ED0000;
		}
	</style>
@stop