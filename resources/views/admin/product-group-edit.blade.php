@extends('admin.layouts.master')

{{-- Title --}}
@section('title', $data->name)

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.product.group.edit', $data))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Lưu</button>
	<a href="{{ route('admin.product.group.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
@stop

{{-- Main --}}
@section('main')
	<form action="{{ route('admin.product.group.update', $data->id) }}" method="POST" id="mainForm" enctype="multipart/form-data">
		@method('PUT')
		@csrf
		<div class="row">
			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="name">Tên nhóm <span class="required">*</span></label>
							<input type="text" name="name" value="{{ $data->name }}" class="form-control" onkeyup="slug_make(event);" required>
						</div>
						<div class="form-group">
							<label for="parent">Thuộc nhóm cha</label>
							<select name="parent" class="custom-select form-control">
								<option value=""></option>
								@foreach($groups as $group_1)
									<option value="{{ $group_1->id }}"{!! $data->parent_id == $group_1->id ? ' selected' : null !!}>{{ $group_1->name }}</option>
									@foreach($group_1->fk_childs as $group_2)
										@if($data->id != $group_2->id)
										<option value="{{ $group_2->id }}"{!! $data->parent_id == $group_2->id ? ' selected' : null !!}>— {{ $group_2->name }}</option>
										@endif
									@endforeach
								@endforeach
							</select>
						</div>
						<hr>
						<div class="form-group">
							<label>Ảnh đại diện</label>
							<div class="custom-image-upload custom-iu-md image-preview m-0">
								<img class="img-preview" src="{{ media_url('product-groups', $data->thumbnail) }}">
								<label class="custom-label">
									<input type="file" name="thumbnail" onchange="upload_img_preview(event);" accept="image/*">
								</label>
								<svg id="i-close" onclick="remove_file(event);" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
									<path d="M2 30 L30 2 M30 30 L2 2"></path>
								</svg>
							</div>
						</div>
						<div class="form-group">
							<label for="title">Tiêu đề</label>
							<input type="text" name="title" value="{{ $data->title }}" class="form-control">
							<span class="help-block">Hiển thị trên tiêu đề trình duyệt và trong kết quả tìm kiếm của Google</span>
						</div>
						<div class="form-group">
							<label for="description">Mô tả</label>
							<textarea name="description" rows="4" class="form-control">{!! $data->description !!}</textarea>
							<span class="help-block">Hiển thị mô tả trong kết quả tìm kiếm của Google</span>
						</div>
						<div class="form-group">
							<label>Đường dẫn <span class="required">*</span></label>
							<div class="input-group">
								<span class="input-group-addon addon-info">{{ url('san-pham') }}</span>
								<input type="text" name="slug" value="{{ $data->slug }}" class="form-control" placeholder="ten-nhom" required>
								<span class="input-group-btn">
									<button class="btn btn-info slug-refresh" type="button" onclick="slug_make();"><i class="fa fa-refresh"></i></button>
								</span>
							</div>
							<label id="slug-error" class="error" for="slug" style="display: none;"></label>
						</div>
						<script>
							$(".slug-refresh").click(function() {
								var event = $(this);
								$(this).find("i").addClass("fa-spin");
								setTimeout(function() {
									$(event).find("i").removeClass("fa-spin");
								}, 1000);
							});
						</script>
						<hr>
						<div class="form-group">
							<label class="custom-toggle toggle-info">
								<input type="checkbox" name="showed" class="toggle-checkbox"{!! $data->hidden ? null : ' checked' !!}>
								<div class="toggle-inner">
									<span class="toggle-button"></span>
								</div>
								<span class="toggle-text">Hiển thị công khai</span>
							</label>
						</div>
						<div class="form-group">
							<label class="custom-toggle toggle-info">
								<input type="checkbox" name="pinned" class="toggle-checkbox"{!! $data->pinned ? ' checked' : null !!}>
								<div class="toggle-inner">
									<span class="toggle-button"></span>
								</div>
								<span class="toggle-text">Ghim lên trang chủ</span>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navProduct").addClass("active").find("ul").addClass("show").find(".group").addClass("active");
	</script>
	<script>
		var slug_make = function(event)
		{
			var value = $("form").find("input[name=name]").val();
			$("form").find("input[name=slug]").val(convertToSlug(value));
		}
		$("#mainForm").validate({
			rules: {
				name: {
					remote: {
						url : '{!! route("admin.product.group.checkNameExist") !!}',
						type: "POST",
						data: 
						{
							id: {{ $data->id }},
							name:  function () {
								return $("input[name=name]").val();
							},
							_token:  function () {
								return $('meta[name="csrf-token"]').attr('content');
							}
						}
					},
				},
				slug: {
					remote: {
						url : '{!! route("admin.product.group.checkSlugExist") !!}',
						type: "POST",
						data: 
						{
							id: {{ $data->id }},
							slug:  function () {
								return $("input[name=slug]").val();
							},
							_token:  function () {
								return $('meta[name="csrf-token"]').attr('content');
							}
						}
					},
				}
			}
		});
	</script>
@stop