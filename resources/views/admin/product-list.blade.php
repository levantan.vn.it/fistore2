@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Sản phẩm')

{{-- Import CSS, JS --}}
@section('header')
	{{-- DataTable --}}
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">

	{{-- Fancybox --}}
	<link rel="stylesheet" href="{{ asset('libs/fancybox/3/jquery.fancybox.min.css') }}" />
	<script src="{{ asset('libs/fancybox/3/jquery.fancybox.min.js') }}"></script>
    
    <style type="text/css">
		fieldset.scheduler-border {
	        border: 1px groove #dfdfdf !important;
	        padding: 0 1.4em 1.4em 1.4em !important;
	        margin: 0 0 1.5em 0 !important;
	        -webkit-box-shadow:  0px 0px 0px 0px #fafafa;
	                box-shadow:  0px 0px 0px 0px #fafafa;
	    }

        legend.scheduler-border {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;
            width:auto;
            padding:0 10px;
            border-bottom:none;
        }
	</style>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.product'))

{{-- Navs --}}
@section('nav')
	@if(request('trash'))
	<a href="{{ current_url() }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
	@else
	<a href="{{ route('admin.product.create') }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Tạo mới</a>
	<a href="{{ current_url('?trash=1') }}" class="btn btn-default"><i class="fa fa-trash fa-fw"></i> Thùng rác</a>
	@endif
@stop

{{-- Main --}}
@section('main')
	<div class="card card-primary mb-4">
		<div class="card-header" data-toggle="collapse" href="#filterCollapse" aria-expanded="true">
			Tìm kiếm
		</div>
		<div class="card-body collapse show p-0" id="filterCollapse">
			<form action="" method="GET" class="m-4" id="filterForm" autocomplete="off">
				<input type="hidden" name="trash" value="{{ request()->trash }}">
				<div class="form-row form-group">
					<div class="col-lg-4">
						<div class="form-group">
							<label for="code">Mã sản phẩm</label>
							<input type="text" name="code" value="{{ request()->code }}" class="form-control">
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label for="name">Tên sản phẩm</label>
							<input type="text" name="name" value="{{ request()->name }}" class="form-control">
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label for="status">Tình trạng</label>
							<select name="status" class="custom-select form-control">
								<option value="">Tất cả</option>
								<option value="hidden_price"{!! request()->status == 'hidden_price' ? ' selected' : null !!}>Ẩn giá bán</option>
								<option value="closed"{!! request()->status == 'closed' ? ' selected' : null !!}>Tắt đặt hàng</option>
								<option value="hidden"{!! request()->status == 'hidden' ? ' selected' : null !!}>Ẩn khỏi trang chủ</option>
								<option value="drafted"{!! request()->status == 'drafted' ? ' selected' : null !!}>Bản nháp</option>
							</select>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label for="category">Nhóm sản phẩm</label>
							<select name="group" class="custom-select form-control">
								<option value="">Tất cả</option>
								@foreach(get_all_groups() as $item_group)
									<option value="{{ $item_group->id }}"{!! request()->group == $item_group->id ? ' selected' : null !!}>{{ $item_group->name }}</option>
									@foreach($item_group->fk_childs as $child_group)
									<option value="{{ $child_group->id }}"{!! request()->group == $child_group->id ? ' selected' : null !!}>-- {{ $child_group->name }}</option>
									@endforeach
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label for="category">Loại sản phẩm</label>
							<select name="category" class="custom-select form-control">
								<option value="">Tất cả</option>
								@foreach(get_all_categories() as $cat)
									<option value="{{ $cat->id }}"{!! request()->category == $cat->id ? ' selected' : null !!}>{{ $cat->name }}</option>
									@foreach($cat->fk_childs as $child_cat)
									<option value="{{ $child_cat->id }}"{!! request()->category == $child_cat->id ? ' selected' : null !!}>-- {{ $child_cat->name }}</option>
									@endforeach
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label for="brand">Thương hiệu</label>
							<select name="brand" class="custom-select form-control">
								<option value="">Tất cả</option>
								@foreach(get_all_brands() as $brand)
									<option value="{{ $brand->id }}"{!! request()->brand == $brand->id ? ' selected' : null !!}>{{ $brand->name }}</option>
									@foreach($brand->fk_childs as $child_brand)
									<option value="{{ $child_brand->id }}"{!! request()->brand == $child_brand->id ? ' selected' : null !!}>-- {{ $child_brand->name }}</option>
									@endforeach
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="form-group text-center">
					<button class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
					<a href="{{ url()->current() }}" class="btn btn-default"><i class="fa fa-refresh"></i> Xem tất cả</a>
				</div>
			</form>
		</div>
	</div>
	<div class="table-filter py-2">
		<div class="alert alert-success" role="alert">
			Có <strong>{{ $data->total() }}</strong> sản phẩm được tìm thấy
		</div>
		<div class="input-group">
			<select name="show" class="custom-select mr-1" form="filterForm" onchange="this.form.submit()">
				<option value="25"{!! request()->show == '25' ? ' selected' : null !!}>Hiển thị 25 kết quả</option>
				<option value="50"{!! request()->show == '50' ? ' selected' : null !!}>Hiển thị 50 kết quả</option>
				<option value="100"{!! request()->show == '100' ? ' selected' : null !!}>Hiển thị 100 kết quả</option>
				<option value="200"{!! request()->show == '200' ? ' selected' : null !!}>Hiển thị 200 kết quả</option>
				<option value="500"{!! request()->show == '500' ? ' selected' : null !!}>Hiển thị 500 kết quả</option>
				<option value="99999999"{!! request()->show == '99999999' ? ' selected' : null !!}>Hiển thị tất cả</option>
			</select>
            <select name="show1" class="custom-select mr-1" form="filterForm" onchange="changestatus(this.value);">
				<option>Thao tác</option>
				<option value="1">Ẩn</option>
				<option value="0">Hiện</option>
			</select>
		</div>
	</div>

	<table class="table table-primary table-hover table-striped table-responsive">
		<thead>
			<tr>
                <th class="checkbox-sort"><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th>
				<th class="no-sort" width="50"></th>
				<th>Sản phẩm</th>
				<th class="text-center">Thứ tự</th>
				<th class="text-center">Thứ tự<br>danh mục</th>
				<th>Giá bán</th>
				<th>Real Stock Quantity</th>
				<th class="text-center">Tồn kho</th>
				<th>Nhóm</th>
				<th>Loại sản phẩm</th>
				<th>Thương hiệu</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				<tr>
                    <td style="vertical-align:middle;"><input id="stt" name="stt[]" value="{{ $item->id }}" type="checkbox" /></td>
					<td>
						<a data-fancybox="gallery-{{ $item->id }}" href="{{ media_url('products', $item->thumbnail) }}">
							<img src="{{ media_url('products', $item->thumbnail) }}" width="60">
						</a>
					</td>
					<td class="white-space-unset">
						<div>
							<a href="{{ product_link($item->slug) }}" target="_blank">{{ $item->name }}</a>&nbsp;
							{!! $item->drafted ? '<i class="fa fa-paste fa-lg" data-toggle="tooltip" data-placement="top" title="Bản nháp"></i>' : null !!}
						</div>
						<div>Mã sản phẩm: <b>{{ $item->code }}</b></div>
						<div class="text-danger">{!! $item->closed ? 'Tắt chức năng đặt hàng' : null !!}</div>
						<div class="text-danger">{!! $item->hidden ? 'Ẩn khỏi trang chủ' : null !!}</div>
					</td>
					<td class="text-center">
						<select name="position" class="custom-select" onchange="updatePosition(event, {{ $item->id }}, '{{ route('admin.product.position') }}')">
							@for($i = 0; $i <= $data->total(); $i++)
							<option value="{{ $i }}"{!! $item->position == $i ? ' selected' : null !!}>{{ $i }}</option>
							@endfor
						</select>
					</td>
					<td class="text-center">
						<select name="position" class="custom-select" onchange="updatePosition(event, {{ $item->id }}, '{{ route('admin.product.positionInCat') }}')">
							@for($i = 0; $i <= $data->total(); $i++)
							<option value="{{ $i }}"{!! $item->position_in_cat == $i ? ' selected' : null !!}>{{ $i }}</option>
							@endfor
						</select>
					</td>
					<td>
						<div>Giá hiện tại: {{ number_format($item->price, 0, ',', '.') }}</div>
						<div>Giá gốc: {{ $item->original_price > 0 ? number_format($item->original_price, 0, ',', '.') : 'N/A' }}</div>
						<div class="text-danger">{!! $item->display_price ? null : 'Ẩn giá bán' !!}</div>
					</td>
					<td>{{$item->real_stock_quantity??null}}</td>
					<td class="text-center">{{ $item->quantity ?? 'Không giới hạn' }}</td>
					<td>
						@if(count($item->fk_groups) > 0)
							@foreach($item->fk_groups as $group)
								<div>{{ $group->name }}</div>
							@endforeach
						@endif
					</td>
					<td>
						{{ optional($item->fk_category)->name }}
					</td>
					<td>
						{{ optional($item->fk_brand)->name }}
					</td>
					<td>
						<ul class="table-options">
							@if(request('trash'))
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Khôi phục" onclick="restoreItem(event, {{ $item->id }}, '{{ route('admin.product.restore') }}')"><i class="fa fa-undo"></i></button>
								</li>
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.product.destroy', $item->id) }}')"><i class="fa fa-remove"></i></button>
								</li>
							@else
                    			<li>
									<a href="#item{{ $item->id }}" onclick="getinfo({{ $item->id }});" data-toggle="collapse" data-placement="top" title="Sửa nhanh"><i class="fa fa-flash"></i></a>
								</li>
								<li>
									<a href="{{ route('admin.product.edit', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
								</li>
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Chuyển vào thùng rác" onclick="trashItem(event, {{ $item->id }}, '{{ route('admin.product.trash') }}')"><i class="fa fa-trash"></i></button>
								</li>
							@endif
						</ul>
					</td>
				</tr>
                <tr id="item{{ $item->id }}" class="collapse">
					<td colspan="11">
						<div class="row">
							<div class="col-md-10 col-xs-12" style="margin: 0 auto;">
								<form id="product{{ $item->id }}">
									<fieldset class="scheduler-border">
									    <legend class="scheduler-border">
									        <h3 style="font-weight:300;font-size:20px;text-transform:none;">
									            Sửa nhanh sản phẩm
	                                        </h3>
									    </legend>
									    <div class="form-row form-group">
											<div class="col-lg-12">
												<div class="form-group">
													<label for="name">Tên sản phẩm</label>
													<input type="text" id="product_name" name="product_name" value="" class="form-control">
												</div>
											</div>
										</div>
										<div class="form-row form-group">
											<div class="col-lg-4">
												<div class="form-group">
													<label for="name">Nhóm</label>
													<select class="custom-select multiple form-control group{{ $item->id }}" id="product_group" name="product_group" >
														<option value="0"></option>
														@foreach($listgroup as $lg)
														<option value="{{ $lg['id'] }}">{{ $lg['name'] }}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="name">Loại sản phẩm</label>
													<select class="custom-select form-control kind{{ $item->id }}" id="product_kind" name="product_kind">
														@foreach($listcategory as $lt)
														<option value="{{ $lt['id'] }}">{{ $lt['name'] }}</option>
														@if(isset($lt['listchild']))
															@foreach($lt['listchild'] as $ltt)
														<option value="{{ $ltt['id'] }}">&nbsp;-{{ $ltt['name'] }}</option>
															@endforeach
														@endif
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="name">Thương hiệu</label>
													<select class="custom-select form-control brand{{ $item->id }}" id="product_brand" name="product_brand">
														@foreach($listbrand as $lb)
														<option value="{{ $lb['id'] }}">{{ $lb['name'] }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
										<div class="form-row form-group">
											<div class="col-lg-4">
												<div class="form-group">
													<label for="code">Giá thị trường</label>
													<input type="text" id="product_original_price" value="" class="form-control" name="product_original_price">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="code">Giá hiện tại</label>
													<input type="text" id="product_price" value="" class="form-control" name="product_price">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="code">Số lượng tồn kho</label>
													<input type="text" id="quantity" value="" class="form-control" name="quantity">
												</div>
											</div>
										</div>
										<div class="form-row form-group">
											<center>
												<button type="button" class="btn btn-primary" onclick="updatefast({{ $item->id }});">Cập nhật</button>
											</center>
										</div>
									</fieldset>
								</form>
							</div>
						</div>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	@include('particles.pagination')
@stop

{{-- Footer --}}
@section('footer')
	<script>
		@if($kind == 'category')
			$("#navProductCategory").addClass("active").find("ul").addClass("show").find(".product").addClass("active");
		@elseif($kind == 'brand')
			$("#navProductBrand").addClass("active").find("ul").addClass("show").find(".product").addClass("active");
		@else
			$("#navProduct").addClass("active").find("ul").addClass("show").find(".product").addClass("active");
		@endif

		function getinfo(productid){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url : base_url + 'admin/products/getinfo/'+productid,
				type : 'get',
				success : function (data) {
					if(data){
						$("#item"+productid+" #product_name").val(data.name);
						$("#item"+productid+" #product_price").val(data.price);
						$("#item"+productid+" #product_original_price").val(data.original_price);
						$("#item"+productid+" #quantity").val(data.quantity);
						$("#item"+productid+" #product_kind option[value='"+data.category_id+"']").attr("selected","selected");
						$("#item"+productid+" #product_brand option[value='"+data.brand_id+"']").attr("selected","selected");
						if(data.group_id){
							var group_id = data.group_id;
							if(group_id.length > 0 ){
								$("#item"+productid+" #product_group option[value='"+group_id[0].id+"']").attr("selected","selected");
							}else{
								$("#item"+productid+" #product_group option[value='0']").attr("selected","selected");
							}
						}else{
							$("#item"+productid+" #product_group option[value='0']").attr("selected","selected");
						}
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					swal('Lỗi!', xhr.responseText, 'error');
				}
			});
		}

		function updatefast(productid){
			swal({
				title: 'Bạn có chắc',
				text: "Cập nhật nội dung sản phẩm này ?",
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Cập nhật',
				cancelButtonText: 'Bỏ qua'
			}).then(function(isConfirm) {
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				var data = $("#product"+productid).serialize();

				$.ajax({
					url : base_url + 'admin/products/updateinfo/'+productid,
					data : data,
					type : 'POST',
					success : function (data) {
						if(data == 1){
							swal('Thành công !', 'success');
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						swal('Lỗi!', xhr.responseText, 'error');
					}
				});
			});
		}

		$("#example-select-all").click(function(){
			if($('#example-select-all:checkbox:checked').length > 0){
				$('input#stt').prop('checked',true)
			}else{
				$("input#stt").prop("checked",false);
			}
		});

		function changestatus(status){
			swal({
				title: 'Bạn có chắc',
				text: "Muốn ẩn / hiện nội dung các sản phẩm này ?",
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Cập nhật',
				cancelButtonText: 'Bỏ qua'
			}).then(function(isConfirm) {
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				
				$('input#stt:checked').each(function () {
				    var sThisVal = $(this).val();
				    
				    $.ajax({
						url : base_url + 'admin/products/updatestatus/'+sThisVal,
						data : {
							status : status
						},
						type : 'POST',
						success : function (data) {
							
						},
						error: function (xhr, ajaxOptions, thrownError) {
							swal('Lỗi!', xhr.responseText, 'error');
						}
					});
				});
				swal('Thành công !', 'success');
			});
		}
	</script>

	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop