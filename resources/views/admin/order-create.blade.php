@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Tạo đơn hàng mới')

{{-- Import CSS, JS --}}
@section('header')
	{{-- Fancybox --}}
	<link rel="stylesheet" href="{{ asset('libs/fancybox/3/jquery.fancybox.min.css') }}" />
	<script src="{{ asset('libs/fancybox/3/jquery.fancybox.min.js') }}"></script>

	{{-- Jquery Validation --}}
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>

	{{-- Selectize --}}
	<script type="text/javascript" src="{{ asset('libs/selectize/0.12.4/js/standalone/selectize.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/selectize/0.12.4/css/selectize.css') }}">

	{{-- DataTable --}}
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.order.create'))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Lưu đơn hàng</button>
	<a href="{{ route('admin.product.order.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
@stop

{{-- Main --}}
@section('main')
	<form action="{{ route('admin.product.order.store') }}" method="POST" id="mainForm">
		@csrf
		<div class="row">
			<div class="col-lg-5">
				<div class="card card-primary">
					<div class="card-header">Thông tin khách hàng</div>
					<div class="card-body">
						<div class="form-group">
							<p class="font-weight-bold">TÀI KHOẢN MUA HÀNG</p>
							<div class="control-group">
								<select id="user" name="user" multiple placeholder="Nhập Tên, Số điện thoại hoặc Email tài khoản"></select>
								<span class="help-block">Để trống nếu khách hàng không sử dụng tài khoản.</span>
							</div>
							<script>
								$('#user').selectize({
									plugins: ['remove_button'],
									maxItems: 1,
									create: false,
									persist: false,
									valueField: 'id',
									labelField: 'name',
									searchField: ['name', 'email', 'phone'],
									options: [
										@foreach($users as $user)
											{
												id: {{ $user->id }}, 
												name: '{{ $user->name }}', 
												email: '{{ $user->email }}', 
												phone: '{{ $user->phone }}'
											},										
										@endforeach
									],
									render: {
								        item: function(item, escape) {
								            return '<div class="item">' +
								                (item.name ? '<span class="name font-weight-bold"><i class="fa fa-user fa-fw mr-2"></i>' + escape(item.name) + '</span>' : '') +
								                (item.email ? '<br><span class="email"><i class="fa fa-envelope fa-fw mr-2"></i>' + escape(item.email) + '</span>' : '') +
								                (item.phone ? '<br><span class="phone"><i class="fa fa-phone fa-fw mr-2"></i>' + escape(item.phone) + '</span>' : '') +
								            '</div>';
								        },
								        option: function(item, escape) {
								            var label = item.name || item.email || item.phone;
								            var email = item.email;
								            var phone = item.phone;
								            return '<div class"item">' +
								                '<span class="name"><i class="fa fa-user fa-fw mr-2"></i>' + escape(label) + '</span><br>' +
								                (email ? '<span class="email text-muted"><i class="fa fa-envelope fa-fw mr-2"></i>' + escape(email) + '</span><br>' : '') +
								                (phone ? '<span class="phone text-muted"><i class="fa fa-phone fa-fw mr-2"></i>' + escape(phone) + '</span>' : '') +
								            '</div>';
								        }
								    },
								});
							</script>
						</div>
						<hr>
						<p class="font-weight-bold">THÔNG TIN GIAO HÀNG</p>
						<div class="form-group">
							<label for="name">Tên người nhận <span class="required">*</span></label>
							<input type="text" name="name" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="name">Điện thoại liên hệ <span class="required">*</span></label>
							<input type="text" name="phone" class="form-control" required>
						</div>
						<div class="row">
							<div class="col-lg-4">
								<div class="form-group">
									<label for="province">Tỉnh thành <span class="required">*</span></label>
									<select name="province" class="custom-select form-control" onchange="get_districts(event)" required>
										<option value=""></option>
										@foreach($provinces as $province)
											<option value="{{ $province->id }}">{{ $province->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label for="district">Quận huyện <span class="required">*</span></label>
									<select name="district" class="custom-select form-control" onchange="get_wards(event)" required>
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label for="district">Phường xã <span class="required">*</span></label>
									<select name="ward" class="custom-select form-control" required>
										<option value=""></option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="address">Địa chỉ <span class="required">*</span></label>
							<input type="text" name="address" placeholder="Số nhà, Tên đường" class="form-control" required>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-7">
				<div class="card card-primary">
					<div class="card-header">Sản phẩm</div>
					<div class="card-body" id="cart">
						<table class="table table-hover">
							<tbody id="cartContent"></tbody>
						</table>
					</div>
					<div class="card-footer">
						<button type="button" class="btn btn-info" data-toggle="modal" data-target="#selectProducts">Thêm sản phẩm</button>
						<button type="button" class="btn btn-danger">Xóa tất cả</button>
					</div>
				</div>
			</div>
		</div>
	</form>
@stop

{{-- Footer --}}
@section('footer')
	<div class="modal fade" id="selectProducts" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Thêm sản phẩm vào đơn hàng</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="" method="POST" id="selectProductForm">
						<label id="quantity-error" class="error" for="quantity" style="display: none;"></label>
						<table class="table table-hover" id="dataTable">
							<thead class="d-none">
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($products as $item)
								<tr data-id="{{ $item->id }}">
									<td>
										<a data-fancybox="product-{{ $item->id }}" href="{{ media_url('products', $item->thumbnail) }}">
											<img src="{{ media_url('products', $item->thumbnail) }}" width="80">
										</a>
									</td>
									<td>
										<div class="font-weight-bold">#{{ $item->code }}</div>
										<div>{{ $item->name }}</div>
									</td>
									<td>
										<div class="c-red">{{ number_format($item->price,0,',','.') }}đ</div>
										<strike class="text-muted">{{ $item->original_price ? number_format($item->original_price,0,',','.') . 'đ' : null }}</strike>
									</td>
									<td>
										@if(is_null($item->quantity))
											Còn hàng
										@elseif($item->quantity > 0)
											Còn <b>{{ $item->quantity }}</b> sản phẩm
										@else
											<div class="text-danger">Hết hàng</div>
										@endif
									</td>
									<td>
										@if(is_null($item->quantity) || $item->quantity > 0)
											<div class="form-group mb-0">
												<div class="custom-number">
													<div class="input-group">
														<span class="input-group-btn">
															<button type="button" class="btn quantity-minus" onclick="quantity_sub(event);">-</button>
														</span>
														<input type="text" id="quantity" name="quantity[{{ $item->id }}]" value="0" min="0" max="{{ $item->quantity ?? 9999999 }}">
														<span class="input-group-btn">
															<button type="button" class="btn quantity-plus" onclick="quantity_add(event);">+</button>
														</span>
													</div>
												</div>
											</div>
										@endif
									</td>
									<td>
										@if(is_null($item->quantity) || $item->quantity > 0)
										<ul class="table-options">
											<li>
												<button type="button" onclick="selectItem({{ $item->id }})"><i class="fa fa-plus"></i></button>
											</li>
										</ul>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Hoàn tất</button>
				</div>
			</div>
		</div>
	</div>

	<script>
		$("#navOrder").addClass("active").find("ul").addClass("show").find(".order").addClass("active");

		$("#mainForm").validate();
		$("#selectProductForm").validate();

		function selectItem(id) {
			var tr = $(event.target).parents("tr");
			tr.find(".table-options li button").attr("onclick", "removeItem("+id+")").html("<i class='fa fa-times'></i>");
			var product = tr.html();
			$("#dataTable").DataTable().row(tr).remove().draw();
			$("#cartContent").append("<tr>"+ product +"</tr>");
		}
		function removeItem(id) {
			var tr = $(event.target).parents("tr");
			tr.find(".table-options li button").attr("onclick", "selectItem("+id+")").html("<i class='fa fa-plus'></i>");

			var thumbnail = tr.find("td").eq(0).html();
			var name = tr.find("td").eq(1).html();
			var price = tr.find("td").eq(2).html();
			var quantity = tr.find("td").eq(3).html();
			var number = tr.find("td").eq(4).html();
			tr.find("td").eq(5).find(".table-options li button").attr("onclick", "selectItem("+id+")").html("<i class='fa fa-plus'></i>");
			var button = tr.find("td").eq(5).html();

			$("#cartContent").find(tr).fadeOut();
			$("#dataTable").DataTable().row.add([thumbnail, name, price, quantity, number, button]).draw();
		}
	</script>
	<script src="{{ asset('js/areas.handler.js') }}"></script>
	<style>
		.dt-buttons,#dataTable_paginate{
			display: none;
		}
		#dataTable_filter{
			text-align: left;
		}
		#dataTable_filter label{
			display: block;
		}
		#dataTable_filter input[type="search"]{
			margin-left: 0;
			display: block;
			width: 100%;
			padding: .5rem .7rem;
			font-size: 1rem;
		}
	</style>
@stop