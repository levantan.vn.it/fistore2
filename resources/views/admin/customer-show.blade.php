@extends('admin.layouts.master')

{{-- Title --}}
@section('title', $data->name)

{{-- Import CSS, JS --}}
@section('header')
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.customer.show', $data))

{{-- Navs --}}
@section('nav')
	<a href="{{ route('admin.user.customer.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
@stop

{{-- Main --}}
@section('main')
	<div class="row">
		<div class="col-lg-4">
			<div class="card card-primary">
				<div class="card-header">Thông tin cá nhân</div>
				<div class="card-body">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td>Họ tên</td>
								<td>{{ $data->name }}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{{ $data->email }}</td>
							</tr>
							<tr>
								<td>Điện thoại liên hệ</td>
								<td>{{ $data->phone }}</td>
							</tr>
							<tr>
								<td>Giới tính</td>
								<td>{{ isset($data->gender) ? ($data->gender ? 'Nam' : 'Nữ') : 'Không xác định' }}</td>
							</tr>
							<tr>
								<td>Ngày sinh</td>
								<td>{{ isset($data->date_of_birth) ? date('d/m/Y', strtotime($data->date_of_birth)) : 'Không xác định' }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card card-primary">
				<div class="card-header">Thông tin tài khoản</div>
				<div class="card-body">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td>Loại tài khoản</td>
								<td>
									{{ isset($data->provider) ? title_case($data->provider) : 'Tài khoản thường' }}
								</td>
							</tr>
							<tr>
								<td>ID tài khoản chủ quản</td>
								<td>
									{{ $data->social_id }}
								</td>
							</tr>
							<tr>
								<td>Ngày khởi tạo</td>
								<td>{{ date('d/m/Y', strtotime($data->created_at)) }}</td>
							</tr>
							<tr>
								<td>Xác thực</td>
								<td>{{ $data->verified ? 'Đã xác thực' : 'Chưa xác thực' }}</td>
							</tr>
							<tr>
								<td>Tình trạng</td>
								<td>{{ $data->disabled ? 'Vô hiệu hóa' : 'Hợp lệ' }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card card-primary">
				<div class="card-header">Thống kê mua hàng</div>
				<div class="card-body">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td>Số lượng đơn hàng</td>
								<td>{{ $statistics['orderCount'] }}</td>
							</tr>
							<tr>
								<td>Đơn hàng đang thực hiện</td>
								<td>{{ $statistics['buyingOrderCount'] }}</td>
							</tr>
							<tr>
								<td>Đơn hàng hoàn tất</td>
								<td>{{ $statistics['completedOrderCount'] }}</td>
							</tr>
							<tr>
								<td>Đơn hàng đã hủy</td>
								<td>{{ $statistics['cancelledOrderCount'] }}</td>
							</tr>
							<tr>
								<td>Tổng trị giá đã thanh toán</td>
								<td>{{ number_format($statistics['sumBoughtOrderCost'],0,',','.') }}</td>
							</tr>
							<tr class="font-weight-bold">
								<td>Điểm tích lũy</td>
								<td>{{ $data->extra_point }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<hr>

	<h5 class="mb-3">ĐỊA CHỈ GIAO HÀNG</h5>
	@if(count($data->fk_address_book) > 0)
	<div class="row">
		@foreach($data->fk_address_book as $item)
		<div class="col-lg-4">
			<div class="card">
				<div class="card-body">
					<table class="table table-bordered">
						<tr>
							<td>Tên người nhận</td>
							<td>{{ $item->name }}</td>
						</tr>
						<tr>
							<td>Số điện thoại</td>
							<td>{{ $item->phone }}</td>
						</tr>
						<tr>
							<td>Địa chỉ</td>
							<td class="white-space-unset">{{ $item->address }}
								@if(!empty($item->fk_ward->name)),<br/>{{ $item->fk_ward->name }}@endif
								@if(!empty($item->fk_district->name)), {{ $item->fk_district->name??'' }}@endif
								@if(!empty($item->fk_province->name)), {{ $item->fk_province->name??'' }}@endif</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		@endforeach
	</div>
	@else
	<div class="alert alert-info">Chưa có địa chỉ giao hàng nào được tạo.</div>
	@endif

	<hr>

	<h5 class="mb-3">LỊCH SỬ MUA HÀNG</h5>
	@if(count($orderHistory) > 0)
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Mã đơn hàng</th>
				<th>Ngày mua</th>
				<th>Sản phẩm</th>
				<th class="text-right">Tổng tiền</th>
				<th class="text-right">Trạng thái đơn hàng</th>
			</tr>
		</thead>
		<tbody>

			@foreach($orderHistory as $item)
			<tr>
				<td>{{ $item->code }}</td>
				<td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
				<td>
					@foreach($item->fk_details as $product)
					@if(is_null($product->product_id))
					<div class="text-muted"><strike>{{ $product->name }} <span class="text-muted">({{ $product->quantity }})</span></strike></div>
					@else
					<div><a href="{{ url($product->fk_product->slug??'') }}.html" target="_blank">{{ $product->name }}</a> <span class="text-muted">({{ $product->quantity }})</span></div>
					@endif							
					@endforeach
				</td>
				<td align="right" width="150" class="product-price">{{ number_format($item->total,0,',','.') }} <u>đ</u></td>
				<td align="right" width="180">
					@if($item->cancelled)
					Đã hủy
					@elseif($item->contacted)
					Liên hệ
					@elseif($item->delayed)
					Trì hoãn
					@else
						@switch($item->status)
							@case(0)
								Chờ duyệt
								@break
							@case(1)
								Xác nhận
								@break
							@case(2)
								Đóng gói
								@break
							@case(3)
								Hoàn thành
								@break
						@endswitch
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@else
	<div class="alert alert-info">Chưa có đơn hàng nào được đặt.</div>
	@endif
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navCustomer").addClass("active");
	</script>
@stop