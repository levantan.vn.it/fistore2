@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Cấu hình')

{{-- Import CSS, JS --}}
@section('header', null)

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.configuration'))

{{-- Navs --}}
@section('nav')
	<a href="{{ route('admin.configuration.edit') }}" class="btn btn-info"><i class="fa fa-cog fa-fw"></i> Thay đổi cấu hình</a>
@stop

{{-- Main --}}
@section('main')
	<div class="row">
		<div class="col-lg-6">
			<div class="card card-primary">
				<div class="card-body">
					<table class="table table-hover table-responsive fz-up-1">
						<tbody>
							<tr>
								<td class="py-4" width="25%">Tên website</td>
								<td class="white-space-unset"><b>{{ $data->name }}</b></td>
							</tr>
							<tr>
								<td class="py-4">Tiêu đề</td>
								<td class="white-space-unset"><b>{{ $data->title }}</b></td>
							</tr>
							<tr>
								<td class="py-4">Mô tả</td>
								<td class="white-space-unset"><b>{!! $data->description !!}</b></td>
							</tr>
							<tr>
								<td class="py-4">Địa chỉ tên miền</td>
								<td class="white-space-unset"><b>{{ $data->domain }}</b></td>
							</tr>
							<tr>
								<td class="py-4">Từ khóa tìm kiếm</td>
								<td class="white-space-unset"><b>{!! $data->keywords !!}</b></td>
							</tr>
							<tr>
								<td class="py-4">Ảnh logo</td>
								<td><img src="{{ config('filesystems.s3.url').'/media/' . $data->logo }}" height="70"></td>
							</tr>
							<tr>
								<td class="py-4">Favicon</td>
								<td><img src="{{ config('filesystems.s3.url').'/media/' . $data->favicon }}" height="70"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-lg-6">
			<div class="card card-primary">
				<div class="card-body">
					<table class="table table-hover table-responsive fz-up-1">
						<tbody>
							<tr>
								<td class="py-4" width="25%">Tên Doanh nghiệp/Cá nhân</td>
								<td class="white-space-unset"><b>{{ $data->author }}</b></td>
							</tr>
							<tr>
								<td class="py-4">Mô tả Doanh nghiệp/Cá nhân</td>
								<td class="white-space-unset"><b>{{ $data->introduce }}</b></td>
							</tr>
							<tr>
								<td class="py-4">Email liên hệ</td>
								<td><b>{{ $data->contact_email }}</b></td>
							</tr>
							<tr>
								<td class="py-4">Email hỗ trợ</td>
								<td><b>{{ $data->support_email }}</b></td>
							</tr>
							<tr>
								<td class="py-4">Hotline 1</td>
								<td><b>{{ $data->hotline_1 }}</b></td>
							</tr>
							<tr>
								<td class="py-4">Hotline 2</td>
								<td><b>{{ $data->hotline_2 }}</b></td>
							</tr>
							<tr>
								<td class="py-4">Fax</td>
								<td><b>{{ $data->fax }}</b></td>
							</tr>
							<tr>
								<td class="py-4">Địa chỉ</td>
								<td class="white-space-unset"><b>{{ $data->address }}, {{ $data->fk_ward->name }}, {{ $data->fk_district->name }}, {{ $data->fk_province->name }}</b></td>
							</tr>
							@if(!is_null($data->map))
							<tr>
								<td colspan="2" class="py-2">
									<iframe src="{!! explode('"', $data->map)[1] !!}" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
								</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navConfig").addClass("active");
	</script>
@stop