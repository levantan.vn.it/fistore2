@extends('admin.layouts.master')

{{-- Title --}}
@section('title', $data->code)

{{-- Import CSS, JS --}}
@section('header')
	{{-- Jquery Validation --}}
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>

	{{-- Selectize --}}
	<script type="text/javascript" src="{{ asset('libs/selectize/0.12.4/js/standalone/selectize.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/selectize/0.12.4/css/selectize.css') }}">

	{{-- Wickedpicker --}}
	<script type="text/javascript" src="{{ asset('libs/wickedpicker/0.4.1/wickedpicker.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/wickedpicker/0.4.1/wickedpicker.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.coupon.edit', $data))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Lưu</button>
	<a href="{{ route('admin.coupon.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
@stop

{{-- Main --}}
@section('main')
	<div class="row">
		<div class="col-lg-6">
			<div class="card">
				<div class="card-body">
					<form action="{{ route('admin.coupon.update', $data->id) }}" method="POST" id="mainForm">
						@csrf
						@method('PUT')
						<div class="form-group">
							<label for="code">Mã khuyến mãi <span class="required">*</span></label>
							<div class="input-group">
								<input type="text" name="code" value="{{ $data->code }}" class="form-control" placeholder="Mã khuyến mãi" required>
								<span class="input-group-btn">
									<button class="btn btn-info slug-refresh px-5" type="button" onclick="random_code(event);" data-toggle="tooltip" data-placement="top" title="Tạo mã"><i class="fa fa-refresh"></i></button>
								</span>
							</div>
							<label id="code-error" class="error" for="code" style="display: none;"></label>
						</div>
						<script>
							$(".slug-refresh").click(function() {
								var event = $(this);
								$(this).find("i").addClass("fa-spin");
								setTimeout(function() {
									$(event).find("i").removeClass("fa-spin");
								}, 1000);
							});
							var random_code = function(event) {
								var value = Math.random().toString(36).substring(2,8).toUpperCase();
								$(event.target).parents(".form-group").find("input[name=code]").val(value);
							}
						</script>

						<div class="form-group">
							<label for="limited">Giới hạn số lượng sử dụng</label>
							<input type="number" name="limited" min="0" value="{{ $data->limited }}" placeholder="Để trống nếu không giới hạn" class="form-control">
						</div>

						<div class="row">
							<div class="col-8">
								<div class="form-group">
									<label for="discount">Mức giảm giá <span class="required">*</span></label>
									<input type="text" name="discount" value="{{ number_format($data->discount) }}" class="form-control" onkeyup="make_money_format(event);" required>
								</div>
							</div>
							<div class="col-4">
								<div class="form-group">
									<label for="unit">Đơn vị <span class="required">*</span></label>
									<select name="unit" class="custom-select form-control">
										@foreach($units as $key => $name)
											<option value="{{ $key }}"{!! $data->unit == $key ? ' selected' : null !!}>{{ $name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						
						<div class="row" id="target">
							<div class="col-lg-5">
								<div class="form-group">
									<label for="target_type">Áp dụng cho <span class="required">*</span></label>
									<select name="target_type" class="custom-select form-control" required disabled>
										@foreach($target_types as $key => $name)
											<option value="{{ $key }}"{!! $data->target_type == $key ? ' selected' : null !!}>{{ $name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-lg-7">
								@if($data->target_type == 1)
								<div class="form-group target" id="target-1">
									<label for="target_ids">Chọn sản phẩm <span class="required">*</span></label>
									<div class="control-group">
										<select id="products" name="target_ids[]" multiple placeholder="VD: Kem làm trắng da" required>
											@foreach($products as $item)
												<option value="{{ $item->id }}"{!! $data->fk_targets->pluck('target_id')->contains($item->id) ? ' selected' : null !!}>[{{ $item->code }}] {{ $item->name }}</option>
											@endforeach
										</select>
									</div>
									<label id="products-selectized-error" class="error" for="products-selectized" style="display: none;"></label>
									<script>
										$('#products').selectize({
											plugins: ['remove_button'],
											delimiter: ',',
											persist: false,
											createOnBlur: false,
											create: false
										});
									</script>
								</div>
								@elseif($data->target_type == 2)
								<div class="form-group target" id="target-2">
									<label for="target_ids">Chọn Nhóm <span class="required">*</span></label>
									<div class="control-group">
										<select id="groups" name="target_ids[]" multiple placeholder="VD: Khuyến mãi hot" required>
											@foreach($groups as $item)
												<option value="{{ $item->id }}"{!! $data->fk_targets->pluck('target_id')->contains($item->id) ? ' selected' : null !!}>{{ $item->name }}</option>
											@endforeach
										</select>
									</div>
									<label id="groups-selectized-error" class="error" for="groups-selectized" style="display: none;"></label>
									<script>
										$('#groups').selectize({
											plugins: ['remove_button'],
											delimiter: ',',
											persist: false,
											createOnBlur: false,
											create: false
										});
									</script>
								</div>
								@elseif($data->target_type == 3)
								<div class="form-group target" id="target-3">
									<label for="target_ids">Chọn Danh mục <span class="required">*</span></label>
									<div class="control-group">
										<select id="categories" name="target_ids[]" multiple placeholder="VD: Trang điểm" required>
											@foreach($categories as $item)
												<option value="{{ $item->id }}"{!! $data->fk_targets->pluck('target_id')->contains($item->id) ? ' selected' : null !!}>{{ $item->name }}</option>
											@endforeach
										</select>
									</div>
									<label id="categories-selectized-error" class="error" for="categories-selectized" style="display: none;"></label>
									<script>
										$('#categories').selectize({
											plugins: ['remove_button'],
											delimiter: ',',
											persist: false,
											createOnBlur: false,
											create: false
										});
									</script>
								</div>
								@elseif($data->target_type == 4)
								<div class="form-group target" id="target-4">
									<label for="target_ids">Chọn Thương hiệu <span class="required">*</span></label>
									<div class="control-group">
										<select id="brands" name="target_ids[]" multiple placeholder="VD: 3CE" required>
											@foreach($brands as $item)
												<option value="{{ $item->id }}"{!! $data->fk_targets->pluck('target_id')->contains($item->id) ? ' selected' : null !!}>{{ $item->name }}</option>
											@endforeach
										</select>
									</div>
									<label id="brands-selectized-error" class="error" for="brands-selectized" style="display: none;"></label>
									<script>
										$('#brands').selectize({
											plugins: ['remove_button'],
											delimiter: ',',
											persist: false,
											createOnBlur: false,
											create: false
										});
									</script>
								</div>
								@endif
							</div>
						</div>
						
						<div class="form-group">
							<label for="min_value">Trị giá đơn hàng tối thiểu</label>
							<input type="text" name="min_value" class="form-control" value="{{ number_format($data->min_value,0) }}" onkeyup="make_money_format(event);">
						</div>

						<div class="row">
							<div class="col-lg-6">
								<div class="row row-mx-5">
									<div class="col-8">
										<div class="form-group">
											<label for="started_at">Ngày bắt đầu <span class="required">*</span></label>
											<div class="custom-datetimepicker">
												<div class="form-inline">
													<input type="text" name="started_at" value="{{ date('d/m/Y', strtotime($data->started_at)) }}" class="form-control datepicker" placeholder="dd/mm/yyyy" style="min-width: 100%;" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" required>
												</div>
											</div>
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>&emsp;</label>
											<input type="text" name="started_at_time" id="startedAtTime" value="{{ date('H:i', strtotime($data->started_at)) }}" placeholder="00:00" class="form-control timepicker" required/>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="row row-mx-5">
									<div class="col-8">
										<div class="form-group">
											<label for="expired_at">Ngày hết hạn <span class="required">*</span></label>
											<div class="custom-datetimepicker">
												<div class="form-inline">
													<input type="text" name="expired_at" value="{{ date('d/m/Y', strtotime($data->expired_at)) }}" class="form-control datepicker" placeholder="dd/mm/yyyy" style="min-width: 100%;" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" required>
												</div>
											</div>
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>&emsp;</label>
											<input type="text" name="expired_at_time" id="expiredAtTime" value="" placeholder="00:00" class="form-control timepicker" required/>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group mt-2">
							<label class="custom-toggle toggle-danger">
								<input type="checkbox" name="disabled" class="toggle-checkbox"{!! $data->disabled ? ' checked' : null !!}>
								<div class="toggle-inner">
									<span class="toggle-button"></span>
								</div>
								<span class="toggle-text">Tạm ngưng khuyến mãi</span>
							</label>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navOrder").addClass("active").find("ul").addClass("show").find(".coupon").addClass("active");
	</script>
	<script>
		$("#mainForm").validate({
			rules: {
				code: {
					remote: {
						url: "{!! route('admin.coupon.checkCodeExist') !!}",
						type: "POST",
						data: {
							id: function () {
								return {{ $data->id }};
							},
							code: function () {
								return $('input[name="code"]').val();
							},
							_token:  function () {
								return $('meta[name="csrf-token"]').attr('content');
							}
						}
					}
				},
			}
		});
	</script>
	<script>
		// Datetime Picker
		$(function() {
			$( ".datepicker" ).datepicker({
				dateFormat : "dd/mm/yy"
			});
		});
		// Time Picker
		$('#startedAtTime').wickedpicker({
			now: "{{ date('H:i', strtotime($data->started_at)) }}",
			twentyFour: true,
			title: 'Thời gian',
			showSeconds: false,
		});
		$('#expiredAtTime').wickedpicker({
			now: "{{ date('H:i', strtotime($data->expired_at)) }}",
			twentyFour: true,
			title: 'Thời gian',
			showSeconds: false,
		});
	</script>
@stop