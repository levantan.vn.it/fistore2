@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Sales')

{{-- Import CSS, JS --}}
@section('header')
	{{-- DataTable --}}
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">

	{{-- Fancybox --}}
	<link rel="stylesheet" href="{{ asset('libs/fancybox/3/jquery.fancybox.min.css') }}" />
	<script src="{{ asset('libs/fancybox/3/jquery.fancybox.min.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.sale'))

{{-- Navs --}}
@section('nav')
	<a href="{{ route('admin.sale.create') }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Tạo mới</a>
@stop

{{-- Main --}}
@section('main')
	<table class="table table-primary table-hover table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th class="no-sort" class="text-center">STT</th>
				<th class="no-sort" width="250">Tên chương trình khuyến mãi</th>
				<th class="no-sort" width="200">Mức giảm giá</th>
				<th class="no-sort" width="180">Thời điểm</th>
				<th class="no-sort">Sản phẩm</th>
				<th class="text-center">Trạng thái</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@if($data->count() > 0)
				@php 
					$dem=0; 
					$price_type = '';
					$price_currency  = '';
				@endphp
			@foreach($data as $item)
				@php $dem++; @endphp
				<tr>
					<td class="text-center">
						{{ $dem }}
					</td>
					<td width="250">
						{{ $item->name }}
					</td>
					<td>
						@if($item->price_type == 1)
							@php 
								$price_type = 'Giảm giá theo %';
								$price_currency  = '%';
							@endphp
						@else
							@php 
								$price_type = 'Giảm giá theo số tiền';
								$price_currency  = 'vnđ';
							@endphp
						@endif
						<strong>Loại: </strong>{{ $price_type }}<br/>
						<strong>Giá trị giảm: </strong> {{ $item->price_down }} {{ $price_currency }}
					</td>
					<td>
						<div><strong>Bắt đầu:</strong> {{ date('d/m/Y H:i', strtotime($item->started_at)) }}</div>
						<div><strong>Kết thúc:</strong> {{ date('d/m/Y H:i', strtotime($item->expired_at)) }}</div>
					</td>
					<td>
						@foreach($item->fk_products as $product)
						<div>{{ $product->name }}</div>
						@endforeach
					</td>
					<td>
						@if($item->status == 0)
							<mark class="mark mark-warning mark-block">Tạm ngưng</mark>
						@elseif(date('U', strtotime($item->started_at)) > date('U'))
							<mark class="mark mark-info mark-block">Chưa bắt đầu</mark>
						@elseif(date('U', strtotime($item->expired_at)) < date('U'))
							<mark class="mark mark-danger mark-block">Hết hạn</mark>
						@else
							<mark class="mark mark-success mark-block">Hoạt động</mark>
						@endif
					</td>
					<td>
						<ul class="table-options justify-content-start">
							<li>
								<a href="{{ route('admin.sale.edit', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
							</li>
							<li>
								<a href="{{ route('admin.sale.delete', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Xoá"><i class="fa fa-trash"></i></a>
							</li>
						</ul>
					</td>
				</tr>
			@endforeach
			@endif
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navSaleOf").addClass("active").find("ul").addClass("show").find(".sales").addClass("active");
	</script>

	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop