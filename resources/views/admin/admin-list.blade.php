@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Quản trị')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.admin'))

{{-- Navs --}}
@section('nav')
	<a href="{{ route('admin.user.admin.create') }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Tạo mới</a>
@stop

{{-- Main --}}
@section('main')
	<table class="table table-primary table-hover table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th class="no-sort" width="50"></th>
				<th width="300">Người dùng</th>
				<th width="300">Vai trò</th>
				<th class="no-sort text-center" width="150">Tình trạng</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				<tr>
					<td>
						<img src="{{ media_url('users', 'user.png') }}" width="60">
					</td>
					<td>{{ $item->name }}<br/>{{ $item->email }}</td>
					<td>{{ optional($item->fk_role)->name ?? 'N/A' }}</td>
					<td>
						@if(!$item->disabled)
						<mark class="mark mark-success mark-block">Hợp lệ</mark>
						@else
						<mark class="mark mark-danger mark-block">Vô hiệu hóa</mark>
						@endif
					</td>
					<td>
						@if(is_null($item->role_id) || is_null($item->fk_role->level) || optional(Auth::guard('admin')->user()->fk_role)->level < optional($item->fk_role)->level)
						<ul class="table-options">
							<li>
								<a href="{{ route('admin.user.admin.edit', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
							</li>
							@if(!$item->disabled)
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Vô hiệu hóa" onclick="disableItem(event, {{ $item->id }}, '{{ route('admin.user.admin.disable') }}')"><i class="fa fa-lock"></i></button>
								</li>
							@else
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Mở khóa" onclick="activateItem(event, {{ $item->id }}, '{{ route('admin.user.admin.activate') }}')"><i class="fa fa-unlock"></i></button>
								</li>
							@endif
							<li>
								<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.user.admin.destroy', $item->id) }}')"><i class="fa fa-close"></i></button>
							</li>
						</ul>
						@endif
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navAccount").addClass("active").find("ul").addClass("show").find(".user").addClass("active");
	</script>
	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop