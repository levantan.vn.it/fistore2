@extends('admin.layouts.master')

{{-- Title --}}
@section('title', $data->name)

{{-- Import CSS, JS --}}
@section('header')
	{{-- Jquery Validation --}}
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>

	{{-- CKEditor --}}
	<script src="{{ asset('libs/ckeditor/4.10.0/ckeditor.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.page.edit', $data))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Cập nhật</button>
	<button class="btn btn-info" name="draft" value="1" form="mainForm"><i class="fa fa-save fa-fw"></i> Lưu bản nháp</button>
	<a href="{{ route('admin.page.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Hủy bỏ</a>
@stop

{{-- Main --}}
@section('main')
	<form action="{{ route('admin.page.update', $data->id) }}" method="POST" enctype="multipart/form-data" id="mainForm">
		@method('PUT')
		@csrf
		<div class="row">
			<div class="col-lg-8">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="name">Tiêu đề <span class="required">*</span></label>
							<input type="text" name="name" value="{{ $data->name }}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="content">Nội dung <span class="required">*</span></label>
							<textarea name="content" id="content">{!! $data->content !!}</textarea>
							<script>
								CKEDITOR.replace('content');
							</script>
						</div>
						<div class="form-group">
							<label>Đường dẫn <span class="required">*</span></label>
							<div class="input-group">
								<span class="input-group-addon addon-info">{{ url('') }}</span>
								<input type="text" name="slug" value="{{ $data->slug }}" class="form-control" placeholder="tieu-de-bai-viet" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,255}$" required>
								<span class="input-group-btn">
									<button class="btn btn-info spinner-refresh" type="button" onclick="slug_make(event);"><i class="fa fa-refresh"></i></button>
								</span>
							</div>
							<label id="slug-error" class="error" for="slug" style="display: none"></label>
						</div>
						<script>
							$(".spinner-refresh").click(function() {
								var event = $(this);
								$(this).find("i").addClass("fa-spin");
								setTimeout(function() {
									$(event).find("i").removeClass("fa-spin");
								}, 1000);
							});
						</script>
					</div>
				</div>
			</div>
			<div class="col-lg 4">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<div class="flex-row">
								<span>Ngày đăng</span>
								<strong>{{ date('d/m/Y H:s', strtotime($data->created_at)) }}</strong>
							</div>
						</div>
						<div class="form-group">
							<div class="flex-row">
								<span>Chỉnh sửa lần cuối</span>
								<strong>{{ date('d/m/Y H:s', strtotime($data->updated_at)) }}</strong>
							</div>
						</div>
						<div class="form-group">
							<div class="flex-row">
								<span>Tác giả</span>
								<strong>{{ $data->fk_author->name }}</strong>
							</div>
						</div>
						<div class="form-group">
							<div class="flex-row">
								<label for="show">Hiển thị công khai</label>
								<label class="custom-toggle toggle-info">
									<input type="checkbox" name="show" class="toggle-checkbox"{{ $data->hidden === 0 ? ' checked' : null }}>
									<div class="toggle-inner mr-0">
										<span class="toggle-button"></span>
									</div>
								</label>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Ảnh hiển thị bài viết</label>
							<div class="custom-image-upload custom-iu-md image-preview">
								<img class="img-preview" src="{{ media_url('pages', $data->thumbnail) }}">
								<label class="custom-label">
									<input type="file" name="thumbnail" onchange="upload_img_preview(event);" accept="image/*">
								</label>
								<svg id="i-close" onclick="remove_file(event);" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
									<path d="M2 30 L30 2 M30 30 L2 2"></path>
								</svg>
							</div>
						</div>
					</div>
				</div>


				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="group">Phân loại</label>
							<select name="group" class="custom-select form-control">
								<option value="">Chưa phân loại</option>
								@foreach($page_groups as $item)
									<option value="{{ $item->id }}"{{ $data->group_id === $item->id ? ' selected' : null }}>{{ $item->name }}</option>
									@foreach($item->fk_childs as $child)
										<option value="{{ $child->id }}"{{ $data->group_id === $child->id ? ' selected' : null }}>— {{ $child->name }}</option>
										@foreach($child->fk_childs as $grandchild)
											<option value="{{ $grandchild->id }}"{{ $data->group_id === $grandchild->id ? ' selected' : null }}>——— {{ $grandchild->name }}</option>
										@endforeach
									@endforeach
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navPage").addClass("active").find("ul").addClass("show").find(".page").addClass("active");

		$("#mainForm").validate();

		var slug_make = function(event)
		{
			var value = $("form").find("input[name=name]").val();
			$("form").find("input[name=slug]").val(convertToSlug(value));
		}
	</script>
	<script type="text/javascript">
		if (typeof CKEDITOR != "undefined") {
			for (var i in CKEDITOR.instances) {
				CKEDITOR.instances[i].on('key', function(e) {
					$(window).bind('beforeunload', function() {
						return 'Bạn có chắc chắn chuyển sang trang khác, Nếu thực hiện dữ liệu sẽ không được lưu';
					});
				});
			}
		}
	</script>
@stop