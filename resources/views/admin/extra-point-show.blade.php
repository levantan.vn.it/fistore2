@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Quản lý điểm')

{{-- Import CSS, JS --}}
@section('header', null)

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.extra_point'))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Lưu thay đổi</a>
@stop

{{-- Main --}}
@section('main')
	<div class="row">
		<div class="col-lg-6">
			<div class="card card-primary">
				<div class="card-body">
					<form action="{{ route('admin.extra_point.update') }}" method="POST" id="mainForm">
						@csrf
						<div class="form-group">
							<div class="form-row">
								<div class="col-lg-6">
									<label for="">
										<strong>Đơn vị quy đổi giá trị đơn hàng sang điểm</strong>
									</label>
								</div>
								<div class="col-lg-6">
									<input type="text" name="money_to_extra_point" value="{{ number_format($money_to_extra_point) }}" class="form-control" onkeyup="make_money_format(event)">
									<span class="help-block">Số tiền quy đổi thành 1 điểm</span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="form-row">
								<div class="col-lg-6">
									<label for="">
										<strong>Đơn vị quy đổi điểm sang tiền mặt</strong><br>
									</label>
								</div>
								<div class="col-lg-6">
									<input type="text" name="extra_point_to_money" value="{{ number_format($extra_point_to_money) }}" class="form-control" onkeyup="make_money_format(event)">
									<span class="help-block">Số điểm quy đổi thành 1.000đ</span>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navOrder").addClass("active").find("ul").addClass("show").find(".extra").addClass("active");
	</script>
@stop