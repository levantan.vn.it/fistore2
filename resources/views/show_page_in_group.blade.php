@extends('layouts.master')

@section('title', $list->name)
@section('description', $list->description ?? null)
@section('keywords', null)

@section('header')
@stop

@section('main')
	{!! Breadcrumbs::render('page.group', $list) !!}
	
	<div class="block-articles">
		<div class="block-body">
			@foreach($data as $item)
			<div class="article-item-2">
				<img src="{{ media_url('pages', $item->thumbnail) }}" alt="{{ $item->title ?? $item->name }}" class="item-thumbnail">
				<div class="item-info">
					<a href="{{ page_link($item->slug) }}" class="item-title">{{ $item->name }}</a>
					<time class="item-time">Ngày {{ date('d/m/Y H:i', strtotime($item->created_at)) }}</time>
					<div class="item-description">{{{ str_length_limit($item->description ?? $item->content, 150) }}}</div>
					<a href="{{ page_link($item->slug) }}" class="item-action"><i class="fa fa-long-arrow-right"></i> Xem chi tiết</a>
				</div>
			</div>
			@endforeach
		</div>

		<div class="block-footer">
			@include('particles.pagination')
		</div>
	</div>

@stop

@section('footer')
@stop