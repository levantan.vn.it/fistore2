@extends('layouts.master')

@section('title', $data->title ?? $data->name)
@section('description', $data->description)
@section('keywords', $data->tags)

@section('header')
	{{-- Owl Carousel --}}
	<link rel="stylesheet" href="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.css') }}" />
	<script src="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.js') }}"></script>
@stop

@section('main')
	{!! Breadcrumbs::render('page.detail', $data) !!}

	<div class="block-article-content">
		<div class="article-title">{{ $data->name }}</div>
		<div class="article-content">{!! $data->content !!}</div>
	</div>
@stop

@section('footer')
@stop