@extends('layouts.master')

@section('title', $data->title ?? $data->name)
@section('description', $data->description ?? str_length_limit($data->content, 200))
@section('keywords', $data->tags)

@section('header')
	{{-- XZoom --}}
	<link rel="stylesheet" href="{{ asset('libs/xzoom/1.0.8/xzoom.css') }}">
	<script src="{{ asset('libs/xzoom/1.0.8/xzoom.min.js') }}"></script>

	{{-- Owl Carousel --}}
	<script src="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.css') }}">

	{{-- Bar Rating Master --}}
	<script src="{{ asset('libs/bar-rating-master/jquery.barrating.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/bar-rating-master/themes/bootstrap-stars.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/bar-rating-master/themes/fontawesome-stars.css') }}">
@stop

@section('main')
	{!! Breadcrumbs::render('combo.detail', $data) !!}

	<div class="block-single">
		<div class="row row-mx-10 mb-4">
			<div class="col-md-4">
				<div class="block-single-images">
					@if(count($data->fk_images) > 0)
					<div class="xzoom-container">
						<img class="xzoom" id="main_image" width="100%" src="{{ media_url('combos', $data->fk_images[0]->path) }}" xoriginal="{{ media_url('combos', $data->fk_images[0]->path) }}">

						<div class="xzoom-thumbs scrollbar-hidden">
							@foreach($data->fk_images as $key => $image)
							<a href="{{ media_url('combos', $image->path) }}">
								<img class="xzoom-gallery" height="50" src="{{ media_url('combos', $image->path) }}" {!! $key == 0 ? 'xpreview="'.media_url('combos', $image->path).'"' : null !!}>
							</a>
							@endforeach
						</div>
					</div>
					@else
					<div class="xzoom-container">
						<img class="xzoom" id="main_image" width="100%" src="{{ media_url('combos', $data->thumbnail) }}" xoriginal="{{ media_url('combos', $data->thumbnail) }}">
					</div>
					@endif
					<script>
					// Xzoom Slider
					$(document).ready(function() {
						$(".xzoom, .xzoom-gallery").xzoom({tint: '#333', Xoffset: 15});
					});
					</script>
				</div>
			</div>
			<div class="col-md-5 mb-4">
				<div class="block-single-info">
					<div class="block-single-name">{{ $data->name }}</div>
					<div class="block-single-social"></div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="block-single-policy">
					<div class="block-single-policy-title">MIỄN PHÍ VẬN CHUYỂN</div>
					<div class="block-single-policy-body">
						<div class="block-single-policy-item">
							<div>
								<img src="{{ asset('images/ico/ico-ship-01.png') }}">
							</div>
							<span>Nhận 500% giá trị sản phẩm nếu phát hiện hàng giả.</span>
						</div>
						<div class="block-single-policy-item">
							<div>
								<img src="{{ asset('images/ico/ico-ship-02.png') }}">
							</div>
							<span>Giao hàng miễn phí toàn HCM từ 90K (huyện 200k).</span>
						</div>
						<div class="block-single-policy-item">
							<div>
								<img src="{{ asset('images/ico/ico-ship-03.png') }}">
							</div>
							<span>Đổi trả trong 14 ngày.</span>
						</div>
						<div class="block-single-policy-item">
							<div>
								<img src="{{ asset('images/ico/ico-ship-04.png') }}">
							</div>
							<span>Giao hàng dưới 120 phút.</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		@if($data->content)
		<div class="block-single-content mb-4">
			<div class="block-single-header">Mô tả sản phẩm</div>
			<div class="block-single-content-body">{!! $data->content !!}</div>
		</div>
		@endif

		{{-- @if(count($related) > 0)
		<div class="block-single-related mb-4">
			<div class="block-single-header">Gợi ý sản phẩm cho bạn</div>
			<div class="block-single-related-body">
				<div class="owl-carousel product-slider">
					@foreach($related as $item)
					<div class="item">
						@include('particles.product_item')
					</div>
					@endforeach
				</div>
			</div>
		</div>
		@endif --}}

	</div>

@stop

@section('footer')
@stop