@extends('layouts.master')

@section('title', 'Quản lý đơn hàng')
@section('description', null)
@section('keywords', null)

@section('header')
@stop

@section('main')
	{!! Breadcrumbs::render('user.order') !!}

	<div class="row">
		<div class="col-md-3">
			@include('particles.aside_user')
		</div>
		<div class="col-md-9">
			@if(Agent::isPhone())
				<div class="block-user-orders">
					@foreach($data as $item)
					<a href="{{ route('user.order.detail', $item->code) }}" class="card mb-3" style="color: #676767">
						<div class="card-body">
							<div>
								<b class="fz-up-1">{{ str_limit($item->fk_details[0]->name, 30) }}</b>
								@if($item->quantity > 1)
									và {{ $item->quantity - 1 }} SP khác
								@endif
							</div>
							<div>Mã đơn hàng: {{ $item->code }}</div>
							<div>Ngày đặt hàng: {{ date('d/m/Y', strtotime($item->created_at)) }}</div>
							<div>Số lượng SP: <b>{{ $item->quantity }}</b></div>
							<div>Tổng: <span class="c-red font-weight-bold">{{ number_format($item->total,0,',','.') }}</span></div>
							<div>Trạng thái:
								@if($item->cancelled)
								Đã hủy
								@elseif($item->delayed)
								Trì hoãn
								@elseif($item->contacted)
								Liên hệ
								@else
									{{ $status[$item->status] }}
									@if($item->status == 0)
										(<button class="btn btn-cancel" type="button" name="id" value="{{ $item->id }}" onclick="cancelOrder(event,{{ $item->id }},'{{ route('user.order.cancel') }}')">Hủy đơn hàng</button>)
									@endif
								@endif
							</div>
						</div>
					</a>
					@endforeach
				</div>
			@else			
				<div class="table-responsive">
					<table class="table-user table table-striped mt-4 mb-5">
						<thead>
							<tr>
								<th>Mã đơn hàng</th>
								<th>Ngày mua</th>
								<th>Sản phẩm</th>
								<th class="text-right">Tổng tiền</th>
								<th class="text-right">Trạng thái đơn hàng</th>
							</tr>
						</thead>
						<tbody>

							@foreach($data as $item)
							<tr>
								<td>{{ $item->code }}</td>
								<td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
								<td style="min-width: 250px">
									@foreach($item->fk_details as $product)
										@if(is_null($product->product_id))
											<div class="text-muted"><strike>{{ $product->name }} <span class="text-muted">({{ $product->quantity }})</span></strike></div>
										@else
											<div><a href="{{ url($product->fk_product->slug) }}.html" target="_blank">{{ $product->name }}</a> <span class="text-muted">({{ $product->quantity }})</span></div>
										@endif
									@endforeach
								</td>
								<td align="right" width="150" class="product-price">{{ number_format($item->total,0,',','.') }} <u>đ</u></td>
								<td align="right" width="180">
									@if($item->cancelled)
									<div class="product-cancelled">Đã hủy</div>
									@elseif($item->delayed)
									<div class="product-cancelled">Trì hoãn</div>
									@elseif($item->contacted)
									<div class="product-cancelled">Liên hệ</div>
									@else
										<div class="product-status-{{ $item->status }}">{{ $status[$item->status] }}</div>
										@if($item->status == 0)
										(<button class="btn btn-cancel" type="button" name="id" value="{{ $item->id }}" onclick="cancelOrder(event,{{ $item->id }},'{{ route('user.order.cancel') }}')">Hủy đơn hàng</button>)
										@endif
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			@endif
		</div>
	</div>

@stop

@section('footer')
	<script>
		$("aside.block-user .order").addClass("active");

		function cancelOrder(event, id, url){
			swal({
				title: 'Bạn có chắc',
				text: "Muốn hủy đơn hàng này?",
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: 'Thực hiện',
				cancelButtonText: 'Bỏ qua'
			}).then(function(isConfirm) {
				if (isConfirm.value) {
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
					$.ajax({
						url : url,
						data : {
							id : id
						},
						type : 'POST',
						success : function (data) {
							swal("Hoàn tất", "Đơn hàng đã được hủy!", "success");
							$(event.target).parents('td').html('<div class="product-cancelled">Đã hủy</div>');
						}
					});
				}
			});
		}
	</script>
@stop