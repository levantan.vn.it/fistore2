@extends('layouts.clear')

@section('title', 'Đăng nhập')
@section('description', null)
@section('keywords', null)

@section('header')
@stop

@section('main')
	{{-- Checkout Progress --}}
	<div class="checkout-progress">
		<li class="d-none d-md-flex"><span class="step">1</span>Giỏ hàng</li>
		<li class="active"><span class="step">2</span>Địa chỉ giao hàng</li>
		<li class="d-none d-md-flex"><span class="step">3</span>Thanh toán & Đặt hàng</li>
	</div>

	<div class="row justify-content-center">
		<div class="col-md-5 mb-5 mx-auto">
			<div class="block-checkout">
				<div class="block-header">
					<div class="block-title">ĐĂNG NHẬP</div>
					<div class="block-description">Vui lòng nhập thông tin tài khoản</div>
				</div>
				<div class="block-body">
					<form action="{{ route('checkout.account.login.post') }}" method="POST" id="loginForm">
						@csrf
						<div class="form-group">
							<label for="email">Email <span class="required">*</span></label>
							<input type="email" name="email" value="{{ old('email') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required>
							@if ($errors->has('email'))
							    <span class="invalid-feedback">
							        <strong>{{ $errors->first('email') }}</strong>
							    </span>
							@endif
						</div>

						<div class="form-group">
							<label for="password">Mật khẩu <span class="required">*</span></label>
							<input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" required>
							@if ($errors->has('password'))
							    <span class="invalid-feedback">
							        <strong>{{ $errors->first('password') }}</strong>
							    </span>
							@endif
						</div>
						
						<div class="form-group">
							<label class="custom-control custom-checkbox">
								<input type="checkbox" name="remember" class="custom-control-input" {{ old('remember') ? 'checked' : '' }}>
								<span class="custom-control-indicator"></span>
								<span class="custom-control-description">Nhớ phiên đăng nhập</span>
							</label>
						</div>

						<div class="form-group mt-5">
							<button class="btn btn-primary btn-block">ĐĂNG NHẬP</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
	<script>
		$("#loginForm").validate();
	</script>
@stop