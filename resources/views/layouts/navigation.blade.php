<div id="nav-target"></div>
<nav id="navigation">
	<div class="container">
		<ul class="nav-wrap">
			<li class="nav-item">
				<a class="nav-link" href="{{ url('/') }}"><i class="fa fa-home fa-lg"></i></a>
			</li>
			{{-- <li class="nav-item dropdown dropdown-hover">
				<a href="{{ url('khuyen-mai') }}" class="nav-link" data-toggle="dropdown">Hot Shopa</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="{{ route('hotdeal') }}">Hot Deal</a>
					<a class="dropdown-item" href="{{ route('freetrial') }}">Trải nghiệm 0 đồng</a>
					<a class="dropdown-item" href="{{ route('combo') }}">Combo sản phẩm</a>
					<div class="cat-thumbnail">
						<img src="{{ asset('images/sale-cat.jpg') }}" alt="Khuyến mãi" width="350">
					</div>
				</div>
			</li> --}}

			<li class="nav-item dropdown dropdown-hover">
				@if(!empty($hotshopa))
				<a href="{{ group_link($hotshopa->slug) }}" class="nav-link" data-toggle="dropdown">{{ $hotshopa->name }}</a>
				<div class="dropdown-menu">
					<div class="cat-list">
						@foreach($hotshopa->fk_childs as $key => $item)
							@if(!empty($item->fk_childs) && count($item->fk_childs))
							<ul class="nav-wrap">
								<li class="nav-item dropdown-hover-sub">
									<a class="dropdown-item" href="{{ group_link($item->slug) }}">{{ $item->name }}</a>
									<div class="dropdown-submenu">
										<div>
											@foreach($item->fk_childs as $grandchild)
												<a class="dropdown-item" href="{{ group_link($grandchild->slug) }}">{{ $grandchild->name }}</a>
											@endforeach
										</div>
									</div>
								</li>
							</ul>
							@else
							<a class="dropdown-item" href="{{ group_link($item->slug) }}">{{ $item->name }}</a>
							@endif
						@endforeach
					</div>
					<div class="cat-thumbnail">
						@if(!empty($hotshopa->thumbnail))
						<img src="{{ media_url('product-groups', $hotshopa->thumbnail) }}" alt="{{ $hotshopa->name }}" width="350">
						@else
						<img src="{{ asset('images/sale-cat.jpg') }}" alt="Khuyến mãi" width="350">
						@endif
					</div>
				</div>
				@endif
			</li>
			@if(!empty($category_1))
			<li class="nav-item dropdown dropdown-hover">
				<a href="{{ category_link($category_1->slug) }}" class="nav-link" data-toggle="dropdown">{{ $category_1->name }}</a>
				<div class="dropdown-menu">
					<div class="cat-list">
						@foreach($category_1->fk_childs as $key => $item)
							@if(!empty($item->fk_childs) && count($item->fk_childs))
							<ul class="nav-wrap">
								<li class="nav-item dropdown-hover-sub">
									<a class="dropdown-item" href="{{ category_link($item->slug) }}">{{ $item->name }}</a>
									<div class="dropdown-submenu">
										<div>
											@foreach($item->fk_childs as $grandchild)
												<a class="dropdown-item" href="{{ category_link($grandchild->slug) }}">{{ $grandchild->name }}</a>
											@endforeach
										</div>
									</div>
								</li>
							</ul>
							@else
							<a class="dropdown-item" href="{{ category_link($item->slug) }}">{{ $item->name }}</a>
							@endif
							@if($key > 14)
								@break
							@endif
						@endforeach
					</div>
					<div class="cat-thumbnail">
						<img src="{{ media_url('product-categories', $category_1->thumbnail) }}" alt="{{ $category_1->name }}" width="350">
					</div>
				</div>
			</li>
			@endif
			@if(!empty($category_2))
			<li class="nav-item dropdown dropdown-hover">
				<a href="{{ category_link($category_2->slug) }}" class="nav-link" data-toggle="dropdown">{{ $category_2->name }}</a>
				<div class="dropdown-menu">
					<div class="cat-list">
						@foreach($category_2->fk_childs as $key => $item)
							@if(!empty($item->fk_childs) && count($item->fk_childs))
							<ul class="nav-wrap">
								<li class="nav-item dropdown-hover-sub">
									<a class="dropdown-item" href="{{ category_link($item->slug) }}">{{ $item->name }}</a>
									<div class="dropdown-submenu">
										<div>
											@foreach($item->fk_childs as $grandchild)
												<a class="dropdown-item" href="{{ category_link($grandchild->slug) }}">{{ $grandchild->name }}</a>
											@endforeach
										</div>
									</div>
								</li>
							</ul>
							@else
							<a class="dropdown-item" href="{{ category_link($item->slug) }}">{{ $item->name }}</a>
							@endif
							@if($key > 14)
								@break
							@endif
						@endforeach
					</div>
					<div class="cat-thumbnail">
						<img src="{{ media_url('product-categories', $category_2->thumbnail) }}" alt="{{ $category_2->name }}" width="350">
					</div>
				</div>
			</li>
			@endif
			@if(!empty($category_7))
			<li class="nav-item dropdown dropdown-hover">
				<a href="{{ category_link($category_7->slug) }}" class="nav-link" data-toggle="dropdown">{{ $category_7->name }}</a>
				<div class="dropdown-menu">
					<div class="cat-list">
						@foreach($category_7->fk_childs as $key => $item)
							@if(!empty($item->fk_childs) && count($item->fk_childs))
							<ul class="nav-wrap">
								<li class="nav-item dropdown-hover-sub">
									<a class="dropdown-item" href="{{ category_link($item->slug) }}">{{ $item->name }}</a>
									<div class="dropdown-submenu">
										<div>
											@foreach($item->fk_childs as $grandchild)
												<a class="dropdown-item" href="{{ category_link($grandchild->slug) }}">{{ $grandchild->name }}</a>
											@endforeach
										</div>
									</div>
								</li>
							</ul>
							@else
							<a class="dropdown-item" href="{{ category_link($item->slug) }}">{{ $item->name }}</a>
							@endif
							@if($key > 14)
								@break
							@endif
						@endforeach
					</div>
					<div class="cat-thumbnail">
						<img src="{{ media_url('product-categories', $category_7->thumbnail) }}" alt="{{ $category_7->name }}" width="350">
					</div>
				</div>
			</li>
			@endif
			<li class="nav-item dropdown dropdown-hover">
				<a href="{{ route('brand.list') }}" class="nav-link" data-toggle="dropdown">Thương hiệu</a>
				<div class="dropdown-menu">
					<div class="cat-list">
						@foreach(get_all_brands() as $key => $item)
							<a class="dropdown-item" href="{{ brand_link($item->slug) }}">{{ $item->name }}</a>
							@if($key > 26)
								@break
							@endif
						@endforeach
					</div>
					<div class="cat-thumbnail">
						@if(!empty($category_6))
						<img src="{{ media_url('product-categories', $category_6->thumbnail) }}" alt="{{ $category_6->name }}" width="350">
						@else
						<img src="{{ asset('images/branding.jpg') }}" alt="Thương hiệu" width="350">
						@endif
						
					</div>
				</div>
			</li>
			@if(!empty($category_4))
			<li class="nav-item dropdown dropdown-hover">
				<a href="{{ category_link($category_4->slug) }}" class="nav-link" data-toggle="dropdown">{{ $category_4->name }}</a>
				<div class="dropdown-menu">
					<div class="cat-list">
						@foreach($category_4->fk_childs as $key => $item)
							@if(!empty($item->fk_childs) && count($item->fk_childs))
							<ul class="nav-wrap">
								<li class="nav-item dropdown-hover-sub">
									<a class="dropdown-item" href="{{ category_link($item->slug) }}">{{ $item->name }}</a>
									<div class="dropdown-submenu">
										<div>
											@foreach($item->fk_childs as $grandchild)
												<a class="dropdown-item" href="{{ category_link($grandchild->slug) }}">{{ $grandchild->name }}</a>
											@endforeach
										</div>
									</div>
								</li>
							</ul>
							@else
							<a class="dropdown-item" href="{{ category_link($item->slug) }}">{{ $item->name }}</a>
							@endif
							@if($key > 14)
								@break
							@endif
						@endforeach
					</div>
					<div class="cat-thumbnail">
						<img src="{{ media_url('product-categories', $category_4->thumbnail) }}" alt="{{ $category_4->name }}" width="350">
					</div>
				</div>
			</li>
			@endif
		</ul>
	</div>
</nav>
