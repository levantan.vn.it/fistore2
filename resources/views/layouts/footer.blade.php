<footer id="footer">
	<div class="footer-top">
		<div class="row row-mx-5 mb-3">
			<div class="col-md-3">
				<div class="footer-logo">
					<a href="{{ url('/') }}">
						<img src="{{ config('filesystems.s3.url').'/media/' . $configs->logo }}" alt="{{ $configs->title }}">
					</a>
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-3 align-self-center font-weight-bold text-right">
				<span class="mr-3">Đăng ký nhận email</span>
			</div>
			<div class="col-md-4 align-self-center">
				<form action="{{ route('newsletter') }}" method="POST" id="newsletterForm">
					@csrf
					<div class="form-group mb-0">
						<div class="input-group">
							<input type="email" name="email" class="form-control" placeholder="Nhập địa chỉ Email" required>
							<span class="input-group-btn">
								<button class="btn btn-primary"><i class="fa fa-envelope-open fa-lg"></i></button>
							</span>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="row row-mx-5">
			<div class="col-lg-3 text-center">
				<div class="facebook-page mb-3">
					<div class="fb-page" data-href="https://www.facebook.com/fistore.vn/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/fistore.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/fistore.vn/">Fistore Vietnam</a></blockquote></div>
				</div>
			</div>
			<div class="col-lg-2 col-md-4">
				<ul class="list">
					<li class="list-title">Về {{ $configs->name }}</li>
					<li class="list-item"><a href="{{ url('gioi-thieu') }}" class="list-link">Giới thiệu</a></li>
					<li class="list-item"><a href="{{ url('bai-viet-su-kien') }}" class="list-link">Bài viết sự kiện</a></li>
				</ul>
			</div>
			<div class="col-lg-3 col-md-4">
				<ul class="list">
					<li class="list-title">Chính sách</li>
					<li class="list-item"><a href="{{ url('chinh-sach-bao-mat-thong-tin-khach-hang') }}" class="list-link">Chính sách bảo mật</a></li>
					<li class="list-item"><a href="{{ url('chinh-sach-doi-tra-va-hoan-tien') }}" class="list-link">Chính sách đổi trả và hoàn tiền</a></li>
					<li class="list-item"><a href="{{ url('chinh-sach-van-chuyen-giao-nhan') }}" class="list-link">Chính sách vận chuyển giao nhận</a></li>
					<li class="list-item"><a href="{{ url('qui-dinh-va-hinh-thuc-thanh-toan') }}" class="list-link">Quy định và hình thức thanh toán</a></li>
					<li class="list-item"><a href="{{ url('hoi-dap') }}" class="list-link">Hỏi đáp</a></li>
				</ul>
			</div>
			<div class="col-lg-4 col-md-4">
				<ul class="list">
					<li class="list-title">Liên hệ</li>
					<li class="list-item">Địa chỉ: {{ $configs->address }}, {{ get_ward($configs->ward_id)->name }}, {{ get_district($configs->district_id)->name }}, {{ get_province($configs->province_id)->name }}</li>
					<li class="list-item">Tel: <a href="tel:{{ $configs->hotline_1 }}">{{ $configs->hotline_1 }}</a></li>
					<li class="list-item">Mail: <a href="mailto:{{ $configs->contact_email }}">{{ $configs->contact_email }}</a></li>
					<li class="list-item mt-2">
						<a href="https://www.facebook.com/fistore.vn">
							<img src="{{ asset('images/facebook-ico.png') }}" height="35" style="border-rqcius: 10px">
						</a>
						<a href="https://www.instagram.com/fistore.vn/">
							<img src="{{ asset('images/instagram-ico.png') }}" height="35" style="border-radius: 10px">
						</a>
						<a href="#">
							<img src="{{ asset('images/zalo-ico.png') }}" height="35" style="border-rqcius: 10px">
						</a>
						<a href="https://shopee.vn/shopavietnam">
							<img src="{{ asset('images/shopee-ico.png') }}" height="35" style="border-rqcius: 10px">
						</a>
						<a href="https://www.youtube.com/channel/UCNKQiOMdCOxX_IbIvs-AiCQ">
							<img src="{{ asset('images/youtube-ico.png') }}" height="35" style="border-rqcius: 10px">
						</a>
					</li>
                    <li class="list-item">
						<a href="http://www.online.gov.vn/CustomWebsiteDisplay.aspx?DocId=44789" target="_blank">
							<img src="{{ asset('images/bocongthuong.png') }}" alt="Đã thông báo bộ công thương" height="50">
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="mr-1 footer-bottom1">
			<div class="d-none d-lg-inline mr-1">Đơn vị vận chuyển</div>
			<img src="{{ asset('images/transportations/giaohangtietkiem.jpg') }}">
			Giấy phép ĐKKD số: 0314724148 cấp ngày 08/11/2017 tại Sở Kế Hoạch và Đầu tư TP. Hồ Chí Minh
		</div>
		<div class="text-md-right footer-bottom2">
			<div class="d-none d-lg-inline mr-1">Phương thức thanh toán</div>
			<img src="{{ asset('images/transactions/tienmat.png') }}">
			<img src="{{ asset('images/transactions/sacombank.jpg') }}">
		</div>
	</div>
</footer>