<ul class="pagination">
    @if($paginator->currentPage() != 1)
    <li class="page-item">
        <a class="page-link" href="{{$paginator->url(1)}}">
            <i class="fa fa-angle-double-left"></i>
        </a>
    </li>
    @endif

    @for($i = 1; $i <= $paginator->lastPage(); $i++)
        @if($i <= ($paginator->currentPage() + 2) && $i >= ($paginator->currentPage() - 2))
            @if($paginator->lastPage() > 1)
                <li class="page-item{{($paginator->currentPage() == $i) ? ' active' : null}}">
                    <a class="page-link" href="{{$paginator->url($i)}}">{{$i}}</a>
                </li>
            @endif
        @endif
    @endfor
    
    @if($paginator->currentPage() < $paginator->lastPage())
    <li class="page-item">
        <a class="page-link" href="{{$paginator->url($paginator->lastPage())}}">
            <i class="fa fa-angle-double-right"></i>
        </a>
    </li>
    @endif
</ul>

