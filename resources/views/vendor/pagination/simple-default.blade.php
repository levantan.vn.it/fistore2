@if ($paginator->hasPages())
    <nav>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled " aria-disabled="true"><span class="custom-font">@lang('pagination.previous')</span></li>
            @else
                <li><a class="custom-font" href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a></li>
            @endif

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li><a class="custom-font" href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')</a></li>
            @else
                <li class="disabled" aria-disabled="true"><span class="custom-font">@lang('pagination.next')</span></li>
            @endif
        </ul>
    </nav>
@endif
