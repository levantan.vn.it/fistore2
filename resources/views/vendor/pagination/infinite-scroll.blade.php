@if ($paginator->hasPages())
    <div class="infinite-scroll-page-load-status">
        <div class="infinite-scroll-request text-center">
            <div class="spinner-border m-3" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    <nav style="display: none">
        <ul class="pagination">
            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="infinite-scroll-pagination__next custom-font" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                </li>
            @endif
        </ul>
    </nav>
@endif
