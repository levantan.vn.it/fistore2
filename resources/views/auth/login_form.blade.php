<div class="modal modal-primary fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title">ĐĂNG NHẬP</div>
			</div>
			<div class="modal-body py-5">
				<form method="POST" action="{{ route('login') }}" id="loginForm" autocomplete="off">
					@csrf

					<div class="form-group row">
						<label for="email" class="col-sm-3 text-md-right">Địa chỉ Email</label>

						<div class="col-md-8">
							<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

							@if ($errors->has('email'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="password" class="col-md-3 text-md-right">Mật khẩu</label>

						<div class="col-md-8">
							<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

							@if ($errors->has('password'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label class="col-md-3 text-md-right">&emsp;</label>
						<div class="col-md-8">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="remember" checked> Nhớ phiên đăng nhập
								</label>
							</div>
						</div>
					</div>

					<div class="form-group row mb-0">
						<label class="col-md-3 text-md-right">&emsp;</label>
						<div class="col-md-8">
							<button type="submit" class="btn btn-primary">
								ĐĂNG NHẬP
							</button>
							&emsp;
							<a class="btn btn-link" href="{{ route('password.request') }}">
								Quên mật khẩu?
							</a>
						</div>
					</div>
				</form>
				<hr class="my-4">
				<div class="form-group row">
					<label for="email" class="col-sm-3 text-md-right"></label>
					<div class="col-md-8">
						<div class="fb-login-button">
							<a href="{{ url('login/facebook/redirect') }}"><i class="fa fa-facebook-square fa-fw"></i> Đăng nhập bằng Facebook</a>
						</div>
						<div class="gg-login-button">
							<a href="{{ url('login/google/redirect') }}"><i class="fa fa-google-plus-square fa-fw"></i> Đăng nhập bằng Google</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$("#loginForm").validate({
		rules: {
			email: {
				remote: {
					type: 'post',
					url: '{{ route('checkout.account.login.checkEmailValid') }}',
					data: 
					{
						email:  function () {
							return $("#loginForm").find('input[name=email]').val();
						},
						_token:  function () {
							return $('meta[name="csrf-token"]').attr('content');
						}
					},
					dataType: 'json'
				}

			},
			password: {
				remote: {
					type: 'post',
					url: '{{ route('checkout.account.login.checkPasswordValid') }}',
					data: {
						email:  function () {
							return $("#loginForm").find('input[name=email]').val();
						},
						password:  function () {
							return $("#loginForm").find('input[name=password]').val();
						},
						_token:  function () {
							return $('meta[name="csrf-token"]').attr('content');
						}
					},
					dataType: 'json'
				}
			},
		}
	});
</script>