@extends('layouts.clear')

@section('title', 'Chuyển hướng')

@section('header')  
@stop

@section('main')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="block-auth">
                <div class="card my-5">
                    <div class="card-header">CHUYỂN HƯỚNG</div>
                    <div class="card-body text-center py-5">
                       <div class="mb-1">Đổi mật khẩu thành công!</div>
                       <div class="mb-3">Tự động chuyển hướng sau <span id="countdown">5</span>s</div>
                       <div>
                           <a href="{{ route('login') }}" class="btn btn-primary">Chuyển hướng ngay</a>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
    <script>
        setInterval(function(){
            var time = $("#countdown").html();
            if(time == 0)
                window.location.href = base_url + "/login";
            else
                $("#countdown").html(time - 1);
        }, 1000);
    </script>
@stop