@extends('layouts.master')

@section('title', 'Quản lý điểm')
@section('description', null)
@section('keywords', null)

@section('header')
@stop

@section('main')
	{!! Breadcrumbs::render('user.extra') !!}

	<div class="row">
		<div class="col-md-3">
			@include('particles.aside_user')
		</div>
		<div class="col-md-9">
			<div class="block-user mt-4 mb-5">
				<div class="block-header">
					<div class="block-title">Quản lý điểm</div>
				</div>
				<div class="block-body text-center">
					@if($data->extra_point > 0)
					Bạn có <strong class="fz-up-5 c-red">{{ number_format($data->extra_point,0,',','.') }}</strong> điểm tích lũy trong ví.
					@else
					Bạn chưa có điểm tích lũy nào trong ví.
					@endif
					<br/>Thực hiện mua hàng để tích lũy nhiều điểm hơn.
				</div>
			</div>

			<div class="block-user mt-4 mb-5">
				<div class="block-header">
					<div class="block-title">Lịch sử điểm tích lũy</div>
				</div>
				<div class="block-body p-0">
					<table width="100%" cellspacing="10" cellpadding="10" class="table table-striped mb-0">
						<thead>
							<tr>
								<th>Điểm</th>
								<th>Thời gian</th>
								<th>Nội dung</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="3">Chưa có hoạt động nào để hiển thị.</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@stop

@section('footer')
	<script>
		$("aside.block-user .extra").addClass("active");
	</script>
@stop